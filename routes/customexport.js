module.exports={
   mail             			: require('../lib/sendmail'),
   sendnotification 			: require('../lib/sendnotification'),
   helper           			: require('../helper/commonhelper'),
   authenticate     			: require('../middleware/authenticate'),
   customerPortalauthenticate   : require('../middleware/customer_portal_authenticate'),
   validateapi      			: require('../middleware/validateapi'),
   setting          			: require('../config/setting')
}
