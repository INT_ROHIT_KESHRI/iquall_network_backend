module.exports = {
    asyncForEach: require('async-foreach').forEach,
    asyncLoop: require('node-async-loop'),
    multer: require('multer'),
    mkdirp: require('mkdirp'),
    jwt: require('jsonwebtoken'),
    crypto: require('crypto'),
    fs: require('fs'),
    path: require('path'),
    async: require('async'),
    datetime: require('node-datetime'),
    session: require('express-session'),
    restClient: require('node-rest-client').Client,
    Influx: require('influx'),
    convert: require('convert-units'),
    arraySort: require('array-sort'),
    humanize: require('humanize'),
    humanizeDuration: require('humanize-duration'),
    wsse: require('wsse'),
    request: require('request'),
    moment: require('moment'),
    WSSEToken: require('wsse-token'),
    winston: require('winston'),
    WinstonContext: require('winston-context'),
    morgan: require('morgan'),
    rotatingFileStream: require('rotating-file-stream'),
    expressRequestId: require('express-request-id'),
    esTemplate: require('es6-template-strings'),
    esTemplateCompile: require('es6-template-strings/compile'),
    esTemplateResolveToString: require('es6-template-strings/resolve-to-string'),
    underscore: require('underscore')
}