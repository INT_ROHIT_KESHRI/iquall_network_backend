var database = require('../../../../../config/database');
var diamantedeskDatabase = require('../../../../../config/diamantedeskDatabase');
var qb = require('node-querybuilder').QueryBuilder(database, 'mysql', 'single');
var qbDiamante = require('node-querybuilder').QueryBuilder(diamantedeskDatabase, 'mysql', 'single');
var globalAPI = require('../../../../../middleware/globalapi');
var SETTING = require('../../../../../config/setting');
var node_module = require('../../../../export');

var model = {
    /* create user model */
    createUser: function(data, userParam, errorcallback, successcallback) {
        var responsequery = "Select * from oro_user WHERE email = '" + data.email + "' OR username = '" + data.username + "' ";
        return qbDiamante.query(responsequery, function(err, response) {
            if (err) {
                errorcallback("Uh oh! Couldn't get results: " + err.msg);
            } else {
                if (response.length > 0) {
                    errorcallback("User already exist.Please try with diffrent user name");
                } else {
                    console.log(userParam);
                    qbDiamante.insert('oro_user', userParam, function(err, res) {
                        if (err) errorcallback(err);
                        successcallback(res.insertId);
                    });
                }
            }
        });
    },
    /* create user access role */
    createUserAccessRole: function(userParam, errorcallback, successcallback) {
        qbDiamante.insert('oro_user_access_role', userParam, function(err, res) {
            if (err) errorcallback(err);
            successcallback(true);
        });
    },
    createUserApi: function(userParam, errorcallback, successcallback) {
        qbDiamante.insert('oro_user_api', userParam, function(err, res) {
            if (err) errorcallback(err);
            successcallback(true);
        });
    },
    updateTicketApi: function(userParam, errorcallback, successcallback) {
        qbDiamante.insert('oro_user_api', userParam, function(err, res) {
            if (err) errorcallback(err);
            successcallback(true);
        });
    },
    deleteDiamanteDeskUserApi: function(cond, errorcallback, successcallback) {
        qbDiamante.delete('oro_user', cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't delete results: " + err);
            } else {
                successcallback(true);
            }
        });
    },
    updateTicketCondiction: function(data, cond, errorcallback, successcallback) {
        qbDiamante.update('diamante_ticket', data, cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't update results: " + err);
            } else {
                successcallback(true);
            }
        });
    },
    /* GET TICKET LIST MODEL */
    getTicketFullInfoByCondiction: function(param, errorcallback, successcallback) {
        var responsequery = "" + param.select + param.join + param.condiction + param.order;
        return qbDiamante.query(responsequery, function(err, response) {
            if (err) {
                errorcallback("Uh oh! Couldn't get results: " + err.msg);
            } else {
                successcallback(response);
            }
        });
    },
    getAdminUserDetail: function(param, errorcallback, successcallback) {
        var userSql = " " + param.select + " " + param.join + "  " + param.condiction;
        return qb.query(userSql, function(err, response) {
            successcallback(response);
        })
    },
    getCustomerUserDetail: function(param, errorcallback, successcallback) {
        var userSql = " " + param.select + " " + param.join + "  " + param.condiction;
        return qb.query(userSql, function(err, response) {
            successcallback(response);
        })
    },
    getTicketCommentFullInfoByCondiction: function(param, errorcallback, successcallback) {
        var getCommentSql = " " + param.select + " " + param.condiction + "  " + param.order;
        return qbDiamante.query(getCommentSql, function(err, response) {
            successcallback(response);
        })
    },
}
module.exports = model;
