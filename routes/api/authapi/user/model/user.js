var database          = require('../../../../../config/database');
var qb = require('node-querybuilder').QueryBuilder(database, 'mysql', 'single');
var model = {
  /* Role Management Section */
  createRole :function(data,errorcallback,successcallback){
    qb.select('*')
      .where(data)
      .get('role', function(err,response) {
          if (err){
            errorcallback("Uh oh! Couldn't get results: " + err.msg);
          }else{
            if(response.length>0){
              errorcallback("Role already exist.Please try with diffrent role name");
            }else{
              qb.insert('role', data, function(err, res) {
                  if (err) errorcallback(err);
                  successcallback(res.insertId);
              });
            }
          }
      });
  },
  updateRole :function(cond,data,errorcallback,successcallback){
    qb.select('*')
      .where({name:data.name,'role_id !=':cond.role_id})
      .get('role', function(err,response) {
          if (err){
            errorcallback("Error in updating role.Please try again");
          }else{
            if(response.length>0){
              errorcallback("Role already exist.Please try with diffrent role name");
            }else{
              qb.update('role', data, cond ,function(err, res) {
                if (err) errorcallback("Uh oh! Couldn't update results: " + err.msg);
                successcallback(true);
              });
            }
          }
      });
  },

  deleteRole :function(cond,errorcallback,successcallback){
    qb.delete('role', cond, function(err, res) {
      if (err) {
        errorcallback("Uh oh! Couldn't delete results: " + err);
      }else{
        successcallback(true);
      }

    });
  },

  getRoles   :function(param,errorcallback,successcallback){
    //var response = "Select "+param.select+" from role as r "+param.join+" where "+param.condiction;
    return qb.query("Select "+param.select+" from role as r "+param.join+" where "+param.condiction,function(err,response){
       successcallback(response);
     })
  },

  /* Permission Management Section */
  createPermission :function(data,errorcallback,successcallback){
    qb.select('*')
      .where({name:data.name})
      .get('permission', function(err,response) {
          if (err){
            errorcallback("Uh oh! Couldn't get results: " + err.msg);
          }else{
            if(response.length>0){
              errorcallback("Permission already exist.Please try with diffrent permission name");
            }else{
              qb.insert('permission', data, function(err, res) {
                      if (err) errorcallback(err);
                      successcallback(res.insertId);
              });
            }
          }
      });
  },

  updatePermission :function(cond,data,errorcallback,successcallback){
    qb.select('*')
      .where({name:data.name})
      .get('permission', function(err,response) {
          if (err){
            errorcallback("Uh oh! Couldn't get results: " + err.msg);
          }else{
            if(response.length>0){
              errorcallback("Permission already exist.Please try with diffrent permission name");
            }else{
              qb.update('permission', data, cond ,function(err, res) {
                if (err) {
                  errorcallback("Uh oh! Couldn't update results: " + err.msg);
                }else{
                  successcallback(true);
                }

              });
            }
          }
      });
  },

  deletePermission :function(cond,errorcallback,successcallback){
    qb.delete('permission', cond, function(err, res) {
      if (err) {
        errorcallback("Uh oh! Couldn't delete results: " + err);
      }else{
        successcallback(true);
      }

    });
  },

  getPermissions   :function(errorcallback,successcallback){
    qb.select('*')
      .where({'permission_id !=':0})
      .get('permission', function(err,response) {
        if(err){
          errorcallback(err);
        }else{
          successcallback(response);
        }
      })
  },

  /* Role-Permission Management Section */
  createRolePermission :function(data,errorcallback,successcallback){
    qb.insert('role_permissions', data, function(err, res) {
            if (err) errorcallback(err);
            successcallback(res.insertId);
    });
  },

  updateRolePermission :function(cond,data,errorcallback,successcallback){
    qb.update('role_permissions', data, cond ,function(err, res) {
      if (err) {
        errorcallback("Uh oh! Couldn't update results: " + err.msg);
      }else{
        successcallback(true);
      }
    });
  },

  deleteRolePermission :function(cond,errorcallback,successcallback){
    qb.delete('role_permissions', cond, function(err, res) {
      if (err) {
        errorcallback("Uh oh! Couldn't delete results: " + err);
      }else{
        successcallback(true);
      }
    });
  },

  /* User section */
  getUserByCondiction :function(condiction,errorcallback,successcallback){
    return qb.select('*')
      .where(condiction)
      .get('user', function(err,response) {
          if (err){
            errorcallback("Uh oh! Couldn't get results: " + err.msg);
          }else{
            successcallback(response);
          }
      });
  },
  createUser :function(data,errorcallback,successcallback){
    qb.select('*')
      .where({email:data.email})
      .get('user', function(err,response) {
          if (err){
            errorcallback("Uh oh! Couldn't get results: " + err.msg);
          }else{
            if(response.length>0){
              errorcallback("User already exist with this email.Please try with diffrent email");
            }else{
              qb.insert('user', data, function(err, res) {
                if (err) {
                  errorcallback(err);
                }else{
                  successcallback(res.insertId);
                }
              });
            }
          }
      });
  },
  updateUserByCondiction :function(data,cond,errorcallback,successcallback){
    qb.select('*')
      .where({user_id:cond.user_id})
      .get('user', function(err,response) {
          if (err){
            errorcallback("Uh oh! Couldn't get results: " + err.msg);
          }else{
            if(response.length==0){
              errorcallback("User doesnot exist.");
            }else{
              qb.update('user', data, cond ,function(err, res) {
                if (err) {
                  errorcallback("Uh oh! Couldn't update results: " + err);
                }else{
                  successcallback(true);
                }
              });
            }
          }
      });
  },
  getUserFullInfoByCondiction :function(param,errorcallback,successcallback){
    var responsequery="Select "+param.select+" from user as u "+param.join+" where "+param.condiction;
    return qb.query("Select "+param.select+" from user as u "+param.join+" where "+param.condiction,function(err,response){
       successcallback(response);
     })
  },
  deleteUser :function(cond,errorcallback,successcallback){
    qb.delete('user', cond, function(err, res) {
      if (err) {
        errorcallback("Uh oh! Couldn't delete results: " + err);
      }else{
        successcallback(true);
      }
    });
  },
  /* Token section */
  getAccessTokenByCondiction :function(condiction,errorcallback,successcallback){
   return qb.select('*')
     .where(condiction)
     .get('token_secret', function(err,response) {
         if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
         successcallback(response);
     });
  },
  removeAccessToken :function(cond,errorcallback,successcallback){
   qb.delete('token_secret', cond, function(err, res) {
     if (err) {
         errorcallback("Uh oh! Couldn't delete results: " + err);
     }else{
         successcallback(true);
     }
   });
  },
  createAccessToken :function(data,errorcallback,successcallback){
   return qb.insert('token_secret', data, function(err, res) {
           qb.release();
           if (err) errorcallback(err);
           successcallback(res.insertId);
       });
  },
  updateAccessTokenByCondiction :function(data,condiction,errorcallback,successcallback){
   return qb.update('token_secret', data, condiction ,function(err, res) {
     if (err) errorcallback("Uh oh! Couldn't update results: " + err.msg);
     successcallback(true);
   });
 }
}
module.exports=model;
