var config       = require('../config/setting.js');
var Authmodel    = require('../routes/api/authapi/customer/model/customer');
var datetime     = require('node-datetime');
var ERROR = {
    UNAUTH : {
        success : false,
        resCode : 401,
        resMessage : "Unauthorized Access"
    },
    TOKENEXP : {
        success : false,
        resCode : 401,
        resMessage :"Accesstoken Expire"
    },
    UNAUTHENTICATE  :{
        success : false,
        resCode : 401,
        resMessage :  "Failed to authenticate token."
    }
};
function isAuthenticated(req, res, next) {
    // CHECK THE USER STORED IN SESSION FOR A CUSTOM VARIABLE
    // you can do this however you want with whatever variables you set up
  //  var token  = req.headers['token'];
    var token = (!req.headers['token'])?req.body['token']:req.headers['token'];
    var ret               = new Date();
    var currentime        = ret.getTime();
    /* Set the expire time of logout to 24 hours */
    var exp_time          = ret.setTime(ret.getTime() + 15000);
    var expiredate        = datetime.create(new Date(exp_time));
    var expire            = expiredate.format("Y-m-d H:M:S");
    var apikey = (!req.headers['apikey'])?req.body['apikey']:req.headers['apikey'];
    var cond   = {
        token : token
    }
    if(apikey==config.definedapikey){
      if (token) {
        Authmodel.getCustomerPortalAccessTokenByCondiction(cond,function(err){},function(token){
          if(token.length>0){
            var expire_time       = new Date(token[0].expire_time).getTime();            
            if(currentime<expire_time){
              var data = {
                expire_time : expire
              }
              Authmodel.updateCustomerPortalAccessTokenByCondiction(data,cond,function(err){               
              },function(success){
                next();
              })
            }else{
              req.session.destroy(function(err) {
                res.status(200).send(ERROR.TOKENEXP);
              })

            }
          }else{
            res.status(200).send(ERROR.UNAUTHENTICATE);
          }
        })
      }else{
        res.status(200).json(ERROR.UNAUTH);
      }
    }else{
      res.status(200).send(ERROR.UNAUTH);
    }
}
module.exports=isAuthenticated;
