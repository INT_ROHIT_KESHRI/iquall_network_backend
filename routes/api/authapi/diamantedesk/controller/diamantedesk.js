var express = require('express');
var router = express.Router();
var app = express();
var DiamantedeskModel = require('../model/diamantedesk');
var customodule = require('../../../../customexport');
var node_module = require('../../../../export');
var ERROR = require('../../../../../config/statuscode');
var PERMISSION = require('../../../../../config/permission');
var SETTING = require('../../../../../config/setting');
var globalAPI = require('../../../../../middleware/globalapi');
var os = require('os');
var wsse = node_module.wsse;
var request = node_module.request;
var moment = node_module.moment;
var underscore = node_module.underscore;
var WSSEToken = node_module.WSSEToken;
//**************** GET TICKET LIST **************//
router.get('/getTicketLstTest', function(req, res) {
    var limit = req.query.limit;
    var page = req.query.page;
    var sortedStr = 'id';
    if (limit != undefined && page != undefined) {
        var token2 = new wsse.UsernameToken({
            username: SETTING.diamenteDeskUsername, // (required)
            password: SETTING.diamenteDeskPassword // (required)
        });
        var uri = SETTING.diamantedeskurl + "tickets?order=desc&sort=" + sortedStr + "&limit=" + limit + '&page=' + page;
        request({
            'method': "GET",
            'uri': uri,
            'headers': {
                'Authorization': 'WSSE profile="UsernameToken"',
                'X-WSSE': token2.getWSSEHeader({
                    nonceBase64: true
                }),
                'Accept': 'application/json',
                'Content-type': 'application/json'
            }
        }, (err, response, body) => {
            if(err == null)
            {
                if(typeof body != 'undefined' && body != "" && body != null)
                {
                    if (body.length > 0) {
                        res.status(200).send(body);
                    }
                    else
                    {
                        var response = {
                            "resCode": 200,
                            "responseMessage": "No ticket found"
                        }
                        res.status(200).send(response);
                    }
                }
                else
                {
                    res.status(200).send([]);
                }
            }
            else
            {
                res.status(200).send(ERROR.DIAMENTE.DIAMENTESERVEREERROR);
            }
        });
    } else {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    }
});
//**************** GET OPEN TICKET LIST **************//
router.post('/getOpenTicketLst',customodule.authenticate, function(req, res) {
    var clientId = '';
    if(req.userType == 'USER')
    {
        clientId = req.client_id;
    }
    else
    {
        clientId = req.body.client_id;
    }
    if (!clientId) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        var limit = req.body.limit;
        var page = req.body.page;
        var sortedStr = 'id';
        var clientId = clientId;
        var condiction = " WHERE dt.id != 0 AND dt.customer_id = " + clientId + " AND dt.status = 'open'  ";
        var select = " SELECT dt.*,db.name from diamante_ticket as dt ";
        var join = "LEFT JOIN diamante_branch as db ON dt.branch_id=db.id ";
        var order = " order by dt.id desc  ";
        var param = {
            condiction: condiction,
            join: join,
            select: select,
            order: order
        }
        DiamantedeskModel.getTicketFullInfoByCondiction(param, function(err) {
            customodule.helper.winstonContext(req,res).info("Getting error on fetching ticket details in diamente");
        }, function(ticketRes) {
            res.status(200).send(ticketRes);
        })
    }
});

//**************** GET TICKET DETAIL **************//
router.post('/getTicketDetails', function(req, res) {
    var tototCommnetRes = {};
    var commentListArr = [];
    var ticketId = req.body.ticketId;
    var commentImageObj = {};
    if (ticketId != undefined) {
        var token2 = new wsse.UsernameToken({
            username: SETTING.diamenteDeskUsername, // (required)
            password: SETTING.diamenteDeskPassword // (required)
        });
        request({
            'method': "GET",
            'uri': SETTING.diamantedeskurl + "tickets/" + ticketId,
            'headers': {
                'Authorization': 'WSSE profile="UsernameToken"',
                'X-WSSE': token2.getWSSEHeader({
                    nonceBase64: true
                }),
                'Accept': 'application/json',
                'Content-type': 'application/json'
            }
        }, (err, response, ticketRes) => {
            if(err == null)
            {
                var ticketObj = JSON.parse(ticketRes);
                tototCommnetRes.ticketRes = ticketRes;
                tototCommnetRes.commentImageObj = commentImageObj;
                res.status(200).send(tototCommnetRes);
            }
            else{
                res.status(200).send(ERROR.DIAMENTE.DIAMENTESERVEREERROR);
            }
        });
    } else {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    }
});
//**************** GET BRANCH LIST **************//
router.get('/getBranchLst', function(req, res) {
    console.log('ok');
    var token2 = new wsse.UsernameToken({
        username: SETTING.diamenteDeskUsername, // (required)
        password: SETTING.diamenteDeskPassword // (required)
    });
    request({
        'method': "GET",
        'uri': SETTING.diamantedeskurl + "branches",
        'headers': {
            'Authorization': 'WSSE profile="UsernameToken"',
            'X-WSSE': token2.getWSSEHeader({
                nonceBase64: true
            }),
            'Accept': 'application/json',
            'Content-type': 'application/json'
        }
    }, (err, response, body) => {
        if(err == null)
        {
            if(typeof body != 'undefined' && body != '' && body != null)
            {
                if (body.length > 0) {
                    res.status(200).send(response);
                }
                else
                {
                    var response = {
                        "resCode": 200,
                        "responseMessage": "No branch found"
                    }
                    res.status(200).send(response);
                }
            }
            else
            {
                res.status(200).send([]);
            }
        }
        else
        {
            res.status(200).send(ERROR.DIAMENTE.DIAMENTESERVEREERROR);
        }
    });
});
//* API FOR ADD BRANCH ***********************//
router.post('/addBranch', function(req, res) {
    var branchParam = req.body;
    var token2 = new wsse.UsernameToken({
        username: SETTING.diamenteDeskUsername, // (required)
        password: SETTING.diamenteDeskPassword // (required)
    });
    request({
        'method': "POST",
        'uri': SETTING.diamantedeskurl + "branches",
        'headers': {
            'Authorization': 'WSSE profile="UsernameToken"',
            'X-WSSE': token2.getWSSEHeader({
                nonceBase64: true
            }),
            'Accept': 'application/json',
            'Content-type': 'application/json'
        },
        'body': branchParam,
        'json': true
    }, (err, response, body) => {
        if(err == null)
        {
            if(typeof body != 'undefined' && body != '' && body != null)
            {
                if (body.length > 0) {
                    res.status(200).send(body);
                }
                else
                {
                    var response = {
                        "resCode": 200,
                        "responseMessage": "No branch Added."
                    }
                    res.status(200).send(response);
                }
            }
            else
            {
                res.status(200).send([]);
            }
        }
        else
        {
            res.status(200).send(ERROR.DIAMENTE.DIAMENTESERVEREERROR);
        }
    });
});
//* API FOR ADD TICKET ***********************//
router.post('/addTicket/:clientId', function(req, res) {
    var ticketParam = req.body;
    var clientId = req.params.clientId;
    var token2 = new wsse.UsernameToken({
        username: SETTING.diamenteDeskUsername, // (required)
        password: SETTING.diamenteDeskPassword // (required)
    });
    request({
        'method': "POST",
        'uri': SETTING.diamantedeskurl + "tickets",
        'headers': {
            'Authorization': 'WSSE profile="UsernameToken"',
            'X-WSSE': token2.getWSSEHeader({
                nonceBase64: true
            }),
            'Accept': 'application/json',
            'Content-type': 'application/json'
        },
        'body': ticketParam,
        'json': true
    }, (err, response, body) => {
        if(err == null)
        {
            if(typeof body != 'undefined' && body != '' && body != null)
            {
                var data = {
                    customer_id: clientId
                }
                var cond = {
                    id: body.id
                }
                DiamantedeskModel.updateTicketCondiction(data, cond, function(error) {
                    var response = {
                        resCode: 201,
                        response: error
                    }
                    res.status(200).send(response);
                }, function(success) {
                    res.status(200).send(body);
                });
            }
            else
            {
                res.status(200).send([]);
            }
        }
        else
        {
            res.status(200).send(ERROR.DIAMENTE.DIAMENTESERVEREERROR);
        }
    });
});
//* API FOR UPDATE TICKET ***********************//
router.post('/updateTicket/:ticketId', function(req, res) {
    var ticketId = req.params.ticketId;
    var ticketParam = req.body;
    if (ticketId != undefined && ticketParam != undefined) {
        var token2 = new wsse.UsernameToken({
            username: SETTING.diamenteDeskUsername, // (required)
            password: SETTING.diamenteDeskPassword // (required)
        });
        request({
            'method': "PUT",
            'uri': SETTING.diamantedeskurl + "tickets/" + ticketId,
            'headers': {
                'Authorization': 'WSSE profile="UsernameToken"',
                'X-WSSE': token2.getWSSEHeader({
                    nonceBase64: true
                }),
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            'body': ticketParam,
            'json': true
        }, (err, response, body) => {
            if(err == null)
            {
                res.status(200).send(body);
            }
            else{
                res.status(200).send(ERROR.DIAMENTE.DIAMENTESERVEREERROR);
            }
        });
    } else {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    }
});
//* API FOR DELETE TICKET ***********************//
router.post('/deleteTicket/:ticketId', function(req, res) {
    var ticketId = req.params.ticketId;
    var resObj = {};
    if (ticketId != undefined) {
        var token2 = new wsse.UsernameToken({
            username: SETTING.diamenteDeskUsername, // (required)
            password: SETTING.diamenteDeskPassword // (required)
        });
        request({
            'method': "DELETE",
            'uri': SETTING.diamantedeskurl + "tickets/" + ticketId,
            'headers': {
                'Authorization': 'WSSE profile="UsernameToken"',
                'X-WSSE': token2.getWSSEHeader({
                    nonceBase64: true
                }),
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            'json': true
        }, (err, response, resBody) => {
            if(err == null)
            {
                if (resBody == undefined) {
                    resObj.resCode = 200;
                    resObj.message = "Ticket has been deleted successfully."
                    res.status(200).send(resObj);
                } else {
                    resBody.resCode = 201;
                    res.status(200).send(resBody);
                }
            }
            else{
                res.status(200).send(ERROR.DIAMENTE.DIAMENTESERVEREERROR);
            }
        });
    } else {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    }
});
//**************** GET BRANCH LIST **************//
router.get('/getCommentLst', function(req, res) {
    var limit = req.query.limit;
    var page = req.query.page;
    if (limit != undefined && page != undefined) {
        var token2 = new wsse.UsernameToken({
            username: SETTING.diamenteDeskUsername, // (required)
            password: SETTING.diamenteDeskPassword // (required)
        });
        var uri = SETTING.diamantedeskurl + "comments?limit=" + limit + '&page=' + page;
        request({
            'method': "GET",
            'uri': uri,
            'headers': {
                'Authorization': 'WSSE profile="UsernameToken"',
                'X-WSSE': token2.getWSSEHeader({
                    nonceBase64: true
                }),
                'Accept': 'application/json',
                'Content-type': 'application/json'
            }
        }, (err, response, body) => {
            if(err == null)
            {
                if(typeof body != 'undefined' && body != null && body != '')
                {
                    if (body.length > 0) {
                        res.status(200).send(body);
                    }
                    else
                    {
                        var response = {
                            "resCode": 200,
                            "responseMessage": "No comment found"
                        }
                        res.status(200).send(response);
                    }
                }
                else
                {
                    res.status(200).send([]);
                }
            }
            else{
                res.status(200).send(ERROR.DIAMENTE.DIAMENTESERVEREERROR);
            }
        });
    } else {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    }
});
//* API FOR ADD COMMENT ***********************//
router.post('/addComment', function(req, res) {
    var commentParam = req.body;
    var token2 = new wsse.UsernameToken({
        username: SETTING.diamenteDeskUsername, // (required)
        password: SETTING.diamenteDeskPassword // (required)
    });
    request({
        'method': "POST",
        'uri': SETTING.diamantedeskurl + "comments",
        'headers': {
            'Authorization': 'WSSE profile="UsernameToken"',
            'X-WSSE': token2.getWSSEHeader({
                nonceBase64: true
            }),
            'Accept': 'application/json',
            'Content-type': 'application/json'
        },
        'body': commentParam,
        'json': true
    }, (err, response, body) => {
        if(err == null)
        {
            if(typeof body != 'undefined' && body != '' && body != null)
            {
                res.status(200).send(body);
            }
            else
            {
                res.status(200).send([]);
            }
        }
        else
        {
            res.status(200).send(ERROR.DIAMENTE.DIAMENTESERVEREERROR);
        }
    });

});
//* API FOR ADD NEW USER ***********************//
router.post('/addUserTest', function(req, res) {
    var resObj = {};
    var ticketParam = req.body;
    var token2 = new wsse.UsernameToken({
        username: SETTING.diamenteDeskUsername, // (required)
        password: SETTING.diamenteDeskPassword // (required)
    });
    request({
        'method': "POST",
        'uri': SETTING.diamantedeskurl + "users",
        'headers': {
            'Authorization': 'WSSE profile="UsernameToken"',
            'X-WSSE': token2.getWSSEHeader({
                nonceBase64: true
            }),
            'Accept': 'application/json',
            'Content-type': 'application/json'
        },
        'body': ticketParam,
        'json': true
    }, (err, response, resBody) => {
        if(err == null)
        {
            if (resBody.error == undefined) {
                resObj.resCode = 200;
                resObj.message = "New user has been created successfully.";
                resObj.resBody = resBody;
                res.status(200).send(resObj);
            } else {
                resBody.resCode = 201;
                res.status(200).send(resBody);
            }
        }
        else
        {
            res.status(200).send(ERROR.DIAMENTE.DIAMENTESERVEREERROR);
        }
    });
});
//* API FOR ADD NEW USER ***********************//
router.post('/addUser', function(req, res) {
    var userParam = req.body;
    var email = req.body.email;
    var username = req.body.username;
    var first_name = req.body.first_name;
    var cond = {};
    cond = {
        "email": email,
        "username": username
    }
    if (!email || !username || !first_name) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        userParam.enabled = 1;
        userParam.salt = SETTING.diamenteDeskAddUserSalt;
        userParam.password = SETTING.diamenteDeskAddUserPassword;
        DiamantedeskModel.createUser(cond, userParam, function(error) {
            var response = {
                resCode: 201,
                response: error
            }
            res.status(200).send(response);
        }, function(responseUserParam) {
            var roleParam = {
                "user_id": responseUserParam,
                "role_id": 3
            }
            DiamantedeskModel.createUserAccessRole(roleParam, function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(responseRoleParam) {
                if (responseRoleParam) {
                    var userApiParam = {
                        "user_id": responseUserParam,
                        "organization_id": 1,
                        "api_key": Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
                    }
                    DiamantedeskModel.createUserApi(userApiParam, function(error) {
                        var response = {
                            resCode: 201,
                            response: error
                        }
                        res.status(200).send(response);
                    }, function(responseUserApiParam) {
                        if (responseUserApiParam) {
                            var responseObj = {
                                resCode: 200,
                                responseMssg: "New user created successfully",
                                response: response,
                                userId: responseUserParam,
                            }
                            res.status(200).send(responseObj);
                        } else {
                            var response = {
                                resCode: 201,
                                response: error
                            }
                            res.status(200).send(response);
                        }
                    });
                } else {
                    var response = {
                        resCode: 201,
                        response: error
                    }
                    res.status(200).send(response);
                }
            });
        })
    }
});
//* API FOR EDIT USER ***********************//
router.post('/editUser/:userId', function(req, res) {
    var userId = req.params.userId;
    if (userId != undefined) {
        var resObj = {};
        var ticketParam = req.body;
        var token2 = new wsse.UsernameToken({
            username: SETTING.diamenteDeskUsername, // (required)
            password: SETTING.diamenteDeskPassword // (required)
        });
        var editUrl = SETTING.diamantedeskurl + "users/" + userId;
        request({
            'method': "PUT",
            'uri': SETTING.diamantedeskurl + "users/" + userId,
            'headers': {
                'Authorization': 'WSSE profile="UsernameToken"',
                'X-WSSE': token2.getWSSEHeader({
                    nonceBase64: true
                }),
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            'body': ticketParam,
            'json': true
        }, (err, response, resBody) => {
            if(err == null){
                if (resBody.error == undefined) {
                    resObj.resCode = 200;
                    resObj.message = "User has been updated successfully.";
                    res.status(200).send(resObj);
                } else {
                    resBody.resCode = 201;
                    res.status(200).send(resBody);
                }
            }
            else
            {
                res.status(200).send(ERROR.DIAMENTE.DIAMENTESERVEREERROR);
            }
        });
    } else {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    }
});

//* API FOR LIST USER *********************//
//**************** GET TICKET LIST **************//
router.get('/listUser', function(req, res) {
    var limit = req.query.limit;
    var page = req.query.page;
    var sortedStr = 'id';
    if (limit != undefined && page != undefined) {
        var token2 = new wsse.UsernameToken({
            username: SETTING.diamenteDeskUsername, // (required)
            password: SETTING.diamenteDeskPassword // (required)
        });
        var uri = SETTING.diamantedeskurl + "users?order=desc&sort=" + sortedStr + "&limit=" + limit + '&page=' + page;
        request({
            'method': "GET",
            'uri': uri,
            'headers': {
                'Authorization': 'WSSE profile="UsernameToken"',
                'X-WSSE': token2.getWSSEHeader({
                    nonceBase64: true
                }),
                'Accept': 'application/json',
                'Content-type': 'application/json'
            }
        }, (err, response, body) => {
            if(err == null){
                if (body.length > 0) {
                    res.status(200).send(body);
                }
                else
                {
                    var response = {
                        "resCode": 200,
                        "responseMessage": "No user found"
                    }
                    res.status(200).send(response);
                }
            }
            else{
                res.status(200).send(ERROR.DIAMENTE.DIAMENTESERVEREERROR);
            }
        });
    } else {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    }
});

//* API FOR DELETE TICKET ***********************//
router.post('/deleteUserTest/:userId', function(req, res) {
    var userId = req.params.userId;
    var resObj = {};
    if (userId != undefined) {
        var token2 = new wsse.UsernameToken({
            username: SETTING.diamenteDeskUsername, // (required)
            password: SETTING.diamenteDeskPassword // (required)
        });
        request({
            'method': "DELETE",
            'uri': SETTING.diamantedeskurl + "users/" + userId,
            'headers': {
                'Authorization': 'WSSE profile="UsernameToken"',
                'X-WSSE': token2.getWSSEHeader({
                    nonceBase64: true
                }),
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            'json': true
        }, (err, response, resBody) => {
            if(err ==null){
                if (resBody == undefined) {
                    resObj.resCode = 200;
                    resObj.message = "User has been deleted successfully."
                    res.status(200).send(resObj);
                } else {
                    resBody.resCode = 201;
                    res.status(200).send(resBody);
                }
            }
            else
            {
                res.status(200).send(ERROR.DIAMENTE.DIAMENTESERVEREERROR);
            }
        });
    } else {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    }
});
//* API FOR DELETE TICKET ***********************//
router.post('/deleteUser/:userId', function(req, res) {
    var userId = req.params.userId;
    var resObj = {};
    if (userId != undefined) {
        var cond = {
            id: userId
        }
        DiamantedeskModel.deleteDiamanteDeskUserApi(cond, function(error) {
            customodule.helper.winstonContext(req,res).info("Getting error on deleting desk user");
        }, function(response) {
            var response = {
                resCode: 200,
                response: "User has been deleted successfully"
            }
            res.status(200).send(response);
        })
    } else {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    }
});
//API GET TICKET LIST
router.post('/getTicketLst', customodule.authenticate,function(req, res, next) {
    var clientId = '';
    if(req.userType == 'USER')
    {
        clientId = req.client_id;
    }
    else
    {
        clientId = req.body.clientId;
    }
    if (!clientId) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        var limit = req.body.limit;
        var page = req.body.page;
        var sortedStr = 'id';
        var condiction = " WHERE dt.id != 0 AND dt.customer_id = " + clientId + " ";
        var select = " SELECT dt.*,db.name from diamante_ticket as dt ";
        var join = "LEFT JOIN diamante_branch as db ON dt.branch_id=db.id ";
        var order = " order by dt.id desc  ";
        var param = {
            condiction: condiction,
            join: join,
            select: select,
            order: order
        }
        DiamantedeskModel.getTicketFullInfoByCondiction(param, function(err) {
            customodule.helper.winstonContext(req,res).info("Getting error on fetching ticket details");
        }, function(ticketRes) {
            res.status(200).send(ticketRes);
        })
    }
});

//API GET TICKET LIST
router.post('/getTicketCommentDetail', function(req, res, next) {
    var commentDetailObj = {};
    var commentListArr = [];
    if (!req.body.ticketId) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        var ticketId = req.body.ticketId;
        var condiction = " WHERE dc.id != 0 AND dc.ticket_id = " + ticketId + " ";
        var select = " SELECT dc.content,dc.author_id,dc.created_at from diamante_comment as dc ";
        var order = " order by dc.id desc  ";
        var param = {
            condiction: condiction,
            select: select,
            order: order
        }
        DiamantedeskModel.getTicketCommentFullInfoByCondiction(param, function(err) {
            customodule.helper.winstonContext(req,res).info("Getting error on fetching ticket comment details");
        }, function(commentRes) {
            var totCommnetResLen = commentRes.length;
            node_module.asyncForEach(commentRes, function(item, index, arr) {
                commentDetailObj[index] = [];
                var authorId = arr[index].author_id;
                //split to get the id
                var userArr = authorId.split('_');
                var dUserId = userArr[1];
                if (dUserId != undefined && dUserId != '' && dUserId != 0 && dUserId != null && dUserId != 'null') {
                    //now check the user from table
                    var condiction = " WHERE u.user_id != 0 AND u.d_user_id = " + dUserId + " ";
                    var select = " SELECT profile_image,user from user as u ";
                    var join = '';
                    var param = {
                        condiction: condiction,
                        select: select,
                        join: join
                    }
                    DiamantedeskModel.getAdminUserDetail(param, function(err) {
                        customodule.helper.winstonContext(req,res).info("Getting error on fetching admin user details");
                    }, function(adminUserRes) {
                        if (adminUserRes.length > 0) {
                            if (adminUserRes == undefined) {

                            } else {
                                if (adminUserRes[0].profile_image != null) {
                                    commentDetailObj[index].push({
                                        "comment": arr[index].content,
                                        "created_at" : arr[index].created_at,
                                        "created_by" : adminUserRes[0].user,
                                        "image": SETTING.profileImagePath + adminUserRes[0].profile_image
                                    });
                                } else {
                                    commentDetailObj[index].push({
                                        "comment": arr[index].content,
                                        "created_at" : arr[index].created_at,
                                        "created_by" : adminUserRes[0].user,
                                        "image": ''
                                    });
                                }
                            }
                        }
                        var condiction = " WHERE u.c_user_id != 0 AND u.d_user_id = " + dUserId + " ";
                        var select = " SELECT profile_image,user from customer_user as u ";
                        var join = '';
                        var param = {
                            condiction: condiction,
                            select: select,
                            join: join
                        }
                        DiamantedeskModel.getCustomerUserDetail(param, function(err) {
                            customodule.helper.winstonContext(req,res).info("Getting error on fetching customer user details");
                        }, function(customerUserRes) {
                            if (customerUserRes.length > 0) {
                                if (customerUserRes == undefined) {} else {
                                    if (customerUserRes[0].profile_image != null) {
                                        commentDetailObj[index].push({
                                            "comment": arr[index].content,
                                            "created_at" : arr[index].created_at,
                                            "created_by" : customerUserRes[0].user,
                                            "image": SETTING.profileImagePath + customerUserRes[0].profile_image
                                        });
                                    } else {
                                        commentDetailObj[index].push({
                                            "comment": arr[index].content,
                                            "created_at" : arr[index].created_at,
                                            "created_by" : customerUserRes[0].user,
                                            "image": ''
                                        });
                                    }
                                }
                            }
                            commentListArr.push({
                                "id": index
                            });
                            var commentListArrLen = commentListArr.length;
                            if (commentListArrLen == totCommnetResLen) {
                                var commentDetailObj2 = underscore.sortBy(commentDetailObj, 'created_at');
                                res.status(200).send(commentDetailObj2);
                            }
                        })
                    })
                } else {
                    commentDetailObj[index].push({
                        "comment": arr[index].content,
                        "created_at" : arr[index].created_at,
                        "created_by" : '',
                        "image": ''
                    });
                    commentListArr.push({
                        "id": index
                    });
                    var commentListArrLen = commentListArr.length;
                    if (commentListArrLen == totCommnetResLen) {
                        var commentDetailObj2 = underscore.sortBy(commentDetailObj, 'created_at');
                        res.status(200).send(commentDetailObj2);
                    }
                }
            });
        })
    }
});
module.exports = router;
