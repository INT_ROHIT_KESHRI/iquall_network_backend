var dasModule = angular.module('dasModule',[])
dasModule
.config(function($stateProvider, $urlRouterProvider){
    $stateProvider
    .state('app', {
        templateUrl: 'templates/abstract/layout/layout.html',
        controller : 'PageViewCtrl'
    })
})
