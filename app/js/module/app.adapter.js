/**
 * This is the service that communicates with the Server. It is kind of
 * a state machine (not enforced though) in the sense that methods
 * must be called sequentially.
 *
 * This is implemented as a service, therefore, as per AngularJS, this is a
 * singleton.
 * This script will not come under strict made: as we are accessing 'caller', 'callee', and 'arguments' properties
 */
app
.factory('$apiAdapter', function($http, $q, $timeout) {

    function postData(reqApiObj) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: reqApiObj.api,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: reqApiObj.data,
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            }
        }).then(function (res) {
            deferred.resolve(res.data);
        }, function (error) {
          deferred.reject(error);
        });

        return deferred.promise;
    }

    return {
          postData      : postData
   	};
});
