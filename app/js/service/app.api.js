app
.factory('$iquallApi', function ($q, $apiAdapter) {
    //============= Verify User =============================//
    function appLogin(email, pass) {
        var deferred = $q.defer();
        var request = {
            api : '/api/unauthUser/userLogin',
            data : {
              userEmail:email,
              userPassword:pass
            }
        };
        $apiAdapter.postData(request).then(function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    return {
        appLogin : appLogin
    }
});
