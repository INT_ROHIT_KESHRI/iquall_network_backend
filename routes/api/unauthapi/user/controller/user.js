var express = require('express');
var router = express.Router();
var app = express();
var customodule = require('../../../../customexport');
var node_module = require('../../../../export');
var ERROR = require('../../../../../config/statuscode');
var Usermodel = require('../model/user');
var SETTING = require('../../../../../config/setting');
var sess;
/* Login Api for all users and admin */
router.post('/userLogin', customodule.validateapi, function(req, res, next) {
    var email = req.body.userEmail;
    var pwd = customodule.setting.passprefix + req.body.userPassword;
    var password = node_module.crypto.createHash('md5').update(pwd).digest("hex");
    if (!req.body.userEmail || !req.body.userPassword) {
        res.status(200).send(ERROR.USER.REQUIREDFIELD);
    } else {
        var condiction = "u.email='" + email + "' AND u.password='" + password + "'";
        var select = "u.user_id as userId,u.d_user_id,ifnull(u.user,'') as userName, ifnull(u.email,'') as userEmail, ifnull(u.phone,'') as userPhone,ifnull(u.mobilePhone,'') as userMobile,ifnull(u.role,'') as userRoleId ,ifnull(r.name,'') as userRole,(SELECT GROUP_CONCAT(p.name) from role_permissions as rp LEFT JOIN permission as p ON rp.permission_id=p.permission_id where rp.role_id=u.role) as permissionName, (SELECT GROUP_CONCAT(name) from permission where 1) as fullpermission,concat('" + SETTING.profileImagePath + "',CASE WHEN u.profile_image='' THEN '' ELSE u.profile_image END) as profile_image ";
        var join = "LEFT JOIN role as r ON r.role_id=u.role";
        var param = {
            condiction: condiction,
            join: join,
            select: select
        }
        Usermodel.getUserFullInfoByCondiction(param, function(err) {
            customodule.helper.winstonContext(req,res).info("Getting error on fetching user details");
        }, function(userdata) {
            if (userdata != undefined) {
                if (userdata.length > 0) {
                    /* Create Expiry time of access_token for a logged in user */
                    var ret = new Date();
                    var exp_time = ret.setTime(ret.getTime() + 60 * 24 * 60000);
                    //var exp_time = ret.setTime(ret.getTime() +  60000);
                    var expiredate = node_module.datetime.create(new Date(exp_time));
                    var expire = expiredate.format("Y-m-d H:M:S");
                    var access_token = node_module.jwt.sign({ time: new Date().getTime() }, customodule.setting.jwtsecret);
                    /* End */
                    var token_data = {
                        user_id: userdata[0].userId,
                        token: access_token,
                        expire_time: expire
                    }

                    Usermodel.createAccessToken(token_data, function(err) {}, function(success) {
                            sess = req.session;
                            sess.admin = (userdata[0].userRoleId == 1 || userdata[0].userRoleId == '1') ? true : false;
                            sess.permissions = userdata[0].permissionName.split(',');
                            var response = {
                                resCode: 200,
                                token: access_token,
                                response: userdata[0]
                            }
                            res.status(200).send(response);
                        })
                        var request = require('request');
                        var myJSONObject = {
                            "email":email,
                            "name":"",
                            "login":userdata[0].userName,
                            "theme":"",
                            "orgId":1,
                            "isGrafanaAdmin":false
                        }
                        request({
                        url: customodule.setting.grafanaurl,
                        headers : {"X-WEBAUTH-USER":email},
                        method: "POST",
                        json: true,   // <--Very important!!!
                        body: myJSONObject
                    }, function (error, response, body){
                    });
                } else {
                    res.status(200).send(ERROR.USER.INVALIDLOGINCREDENTIAL);
                }
            } else {
                res.status(200).send(ERROR.USER.INVALIDLOGINCREDENTIAL);
            }
        });
    }
})
router.post('/forgotPassword', customodule.validateapi, function(req, res, next) {
    var condiction = {
        email: req.body.email,
        status: '1'
    }
    if (!req.body.email) {
        res.status(200).send(ERROR.USER.EMAILREQUIRED);
    } else {
        Usermodel.getUserByCondiction(condiction, function(err) {
            customodule.helper.winstonContext(req,res).info("Getting error on fetching user details");
        }, function(users) {
            if (users.length > 0) {
                var verification_token = node_module.jwt.sign({ time: new Date().getTime() }, customodule.setting.jwtsecret);
                var data = {
                    verification_token: verification_token
                }
                Usermodel.updateUserByCondiction(data, condiction, function(err) {
                    customodule.helper.winstonContext(req,res).info("Getting error on updating user");
                }, function(success) {
                    customodule.mail(req.body.email, verification_token, 'forgotPassword', '');
                    var response = {
                        resCode: 200,
                        response: "Please check your mail for generating new password."
                    }
                    res.status(200).send(response);
                })
            } else {
                res.status(200).send(ERROR.USER.INVALIDEMAIL);
            }
        });
    }

})

router.get('/test', function(req, res, next) {
    var response = {
        resCode: 200,
        response: "This is a response massage"
    }
    res.status(200).json(response);
})

router.get('/resetPassword', function(req, res, next) {
    var token_data = req.query.token;
    var usercond = {
        verification_token: token_data,
        status: '1'
    }

    Usermodel.getUserByCondiction(usercond, function(err) {
        customodule.helper.winstonContext(req,res).info("Getting error on fetching user details");
    }, function(users) {
        if (users.length > 0) {
            res.setHeader('Content-Type', 'text/html');
            res.render(node_module.path.join(__dirname + '/../../../../../views/forgotPassword.ejs'), {
                verification_token: token_data
            });
        } else {
            res.setHeader('Content-Type', 'text/html');
            res.render(node_module.path.join(__dirname + '/../../../../../views/failure.html'));
        }
    })
});

router.post('/reset', function(req, res, next) {
    var token = req.body.token;
    var usercond = {
        verification_token: token
    }
    Usermodel.getUserByCondiction(usercond, function(err) {
        customodule.helper.winstonContext(req,res).info("Getting error on fetching user details");
    }, function(users) {
        if (users.length > 0 && token != '') {
            var password = node_module.crypto.createHash('md5').update(customodule.setting.passprefix + req.body.new_password).digest("hex");
            var cond = {
                verification_token: token,
                status: '1'
            }
            var data = {
                verification_token: '',
                password: password
            }
            Usermodel.updateUserByCondiction(data, cond, function(err) { node_module.winston.info("Error to update data ",err); }, function(success) {
                res.sendFile(node_module.path.join(__dirname + '/../../../../../views/success.html'));
            })
        } else {
            res.sendFile(node_module.path.join(__dirname + '/../../../../../views/failure.html'));
        }
    })
})
module.exports = router;
