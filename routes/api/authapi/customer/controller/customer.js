var express = require('express');
var router = express.Router();
var app = express();
var customodule = require('../../../../customexport');
var node_module = require('../../../../export');
var ERROR = require('../../../../../config/statuscode');
var PERMISSION = require('../../../../../config/permission');
var SETTING = require('../../../../../config/setting');
var Customermodel = require('../model/customer');
var globalAPI = require('../../../../../middleware/globalapi');
var os = require('os');
var wsse = node_module.wsse;
var request = node_module.request;
var moment = node_module.moment;
var WSSEToken = node_module.WSSEToken;

/* Role Management Api Start */
/* This api is to create customer role and only admin can access this api
   Created  : 13-04-2017
   Modified : 05-04-2017
*/
router.post('/createCustomerRole', customodule.authenticate, function(req, res, next) {
    var name = req.body.name;
    var permission_ids = req.body.permissionIds;
    var permissions = req.session.permissions;   
    var customer_manage_index = 1;
    if (customer_manage_index != -1) {
        if (!name || !permission_ids) {
            res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
        } else {
            var data = {
                name: name
            }
            var dataAppend = {
                name: name
            };
            Customermodel.createRole(data, function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(insertId) {
                if (insertId) {
                    var permissions = permission_ids.split(',');
                    var i;
                    for (i = 0; i < permissions.length; i++) {
                        var data = {
                            c_permission_id: permissions[i],
                            c_role_id: insertId
                        }
                        Customermodel.createRolePermission(data, function(error) {}, function(successcallback) {});
                        if (i == permissions.length - 1) {
                            Customermodel.getCustomerPermissions(data.c_permission_id, function(error) {}, function(successcallback2) {
                                var returnData = {
                                    "c_role_name": dataAppend.name,
                                    "c_permission_id": data.c_permission_id,
                                    "c_role_id": data.c_role_id,
                                    "c_permission_names": successcallback2[0].name
                                };
                                var response = {
                                    resCode: 200,
                                    response: "Customer role created successfully",
                                    roledata: returnData
                                }
                                res.status(200).send(response);
                            });

                        }
                    }
                } else {
                    res.status(200).send(ERROR.ROLE.ROLEEXIST);
                }
            })
        }
    } else {
        res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
})

/* This api is to edit role and only admin can access this api
    Created  : 13-04-2017
    Modified : 05-04-2017
 */
router.post('/editCustomerRole', customodule.authenticate, function(req, res, next) {
    var name = req.body.name;
    var role_id = req.body.roleId;
    var permission_ids = req.body.permissionIds;
    if (!name || !role_id || !permission_ids) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        var permissions = req.session.permissions;
        var customer_manage_index = permissions.indexOf(PERMISSION.customer_manage);
        if (customer_manage_index != -1) {
            var permissions = permission_ids.split(',');
            var data = {
                name: name
            }
            var cond = {
                c_role_id: role_id
            }
            Customermodel.updateRole(cond, data, function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success) {
                Customermodel.deleteRolePermission(cond, function(err) {
                    customodule.helper.winstonContext(req, res).info("Error");
                }, function(success) {
                    var i;
                    for (i = 0; i < permissions.length; i++) {
                        var data = {
                            c_permission_id: permissions[i],
                            c_role_id: role_id
                        }
                        Customermodel.createRolePermission(data, function(error) {}, function(successcallback) {});
                        if (i == permissions.length - 1) {
                            var response = {
                                resCode: 200,
                                response: "Customer role updated successfully"
                            }
                            res.status(200).send(response);
                        }
                    }
                })
            })
        } else {
            res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
        }
    }

})

/* This api is to delete role and only admin can access this api
   Created  : 29-03-2017
   Modified : 04-04-2017
*/
router.get('/deleteCustomerRole', customodule.authenticate, function(req, res, next) {
    var role_id = req.query.roleId;
    if (!role_id) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        var permissions = req.session.permissions;        
        var customer_manage_index = 1;
        if (customer_manage_index != -1) {
            var cond = {
                c_role_id: role_id
            }
            Customermodel.getClientByCondiction({
                customer_role_id: role_id
            }, function(err) {
                res.status(200).send(ERROR.ROLE.ROLENOTDELETED);
            }, function(users) {
                if (users.length > 0) {
                    res.status(200).send(ERROR.ROLE.ROLEASSIGNED);
                } else {
                    Customermodel.deleteRolePermission(cond, function(error) {
                        var response = {
                            resCode: 201,
                            response: error
                        }
                        res.status(200).send(response);
                    }, function(success) {
                        if (success) {
                            Customermodel.deleteCustomerRole(cond, function(error) {
                                customodule.helper.winstonContext(req, res).info("Error on deleting customer role");
                            }, function(response) {
                                var response = {
                                    resCode: 200,
                                    response: "Role deleted successfully"
                                }
                                res.status(200).send(response);
                            })
                        }
                    })
                }
            })
        } else {
            res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
        }
    }
})

/* This api is to get customer roles and only admin can access this api
   Created  : 14-04-2017
   Modified : 14-04-2017
*/
router.get('/getCustomerRoles', customodule.authenticate, function(req, res, next) {
    var permissions = req.session.permissions;    
    var manage_role_index = 1;
    var role_list_index = 1;
    if (manage_role_index != -1 || role_list_index != -1) {
        if (req.query.roleId) {
            var condiction = "r.c_role_id =" + req.query.roleId;
        } else {
            var condiction = "r.c_role_id !=''";
        }
        var select = "r.c_role_id as c_role_id,ifnull(r.name,'') as c_role_name, (SELECT GROUP_CONCAT(c_permission_id) from customer_role_permissions as rp where rp.c_role_id=r.c_role_id) as c_permission_id , (SELECT GROUP_CONCAT(p.name) from  customer_role_permissions as rp LEFT JOIN customer_permission as p ON rp.c_permission_id=p.c_permission_id where rp.c_role_id=r.c_role_id) as c_permission_names ";
        var join = "";
        var param = {
            condiction: condiction,
            join: join,
            select: select
        }
        Customermodel.getRoles(param, function(error) {
            var response = {
                resCode: 201,
                response: error
            }
            res.status(200).send(response);
        }, function(success) {
            if (success) {
                var response = {
                    resCode: 200,
                    response: success
                }
                res.status(200).send(response);
            }
        })
    } else {
        res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
})

/* This api is to get customer permissions and only admin can access this api
   Created  : 14-04-2017
*/
router.get('/getCustomerPermissions', customodule.authenticate, function(req, res, next) {
        var permissions = req.session.permissions;      
        var manage_role_index = 1;
        var role_list_index = 1;
        if (manage_role_index != -1 || role_list_index != -1) {
            Customermodel.getPermissions(function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success) {
                if (success) {
                    var response = {
                        resCode: 200,
                        response: success
                    }
                    res.status(200).send(response);
                }
            })
        } else {
            res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
        }
    })
    /* Role-Permission Management Api End */

/* Customer Management Section */

/* This api is to create client and only admin can access this api
   Created  : 18-04-2017
*/
router.post('/createClient', customodule.authenticate, function(req, res, next) {
        var clientName = req.body.clientName;
        var site = req.body.clientSite;
        var subscription = req.body.clientSubscription;
        var status = req.body.clientStatus;
        var permissions = req.session.permissions;
        //var manage_user_index = permissions.indexOf(PERMISSION.user_manage);
        var manage_user_index = 1;
        if (manage_user_index != -1) {
            if (!clientName || !site || !subscription || !status) {
                res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
            } else {
                var data = {
                    name: clientName,
                    site: site,
                    subscription: subscription,
                    status: status
                }
                Customermodel.createCustomer(data, function(error) {
                    var response = {
                        resCode: 201,
                        response: error
                    }
                    res.status(200).send(response);
                }, function(success) {
                    var condiction = "u.client_id =" + success;
                    var select = "u.client_id as client_id,u.name as client_name,u.site as site,u.subscription as subscription,u.status as status";
                    var join = "";
                    var param = {
                        condiction: condiction,
                        join: join,
                        select: select
                    }
                    Customermodel.getCustomerFullInfoByCondiction(param, function(err) {
                        customodule.helper.winstonContext(req, res).info("Error to fetch customer info");
                    }, function(userdata) {
                        var response = {
                            resCode: 200,
                            response: "Client created successfully",
                            userdata: userdata[0]
                        }
                        res.status(200).send(response);
                    })
                })
            }
        } else {
            res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
        }
    })
    /* This api is to get all clients and only admin can access this api
       Created  : 31-03-2017
    */
router.get('/getClients', customodule.authenticate, function(req, res, next) {
        var permissions = req.session.permissions;     
        var manage_user_index = 1;
        var user_list_index = 1;
        if (manage_user_index != -1 || user_list_index != -1) {
            if (req.query.userId) {
                var condiction = "u.client_id =" + req.query.clientId;
            } else {
                var condiction = "u.client_id !=1";
            }
            var select = "u.client_id as client_id,ifnull(u.name,'') as client_name, ifnull(u.site,'') as site, ifnull(u.subscription,'') as subscription, u.status as status";            
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getUserFullInfoByCondiction(param, function(err) {
                customodule.helper.winstonContext(req, res).info("Error to fetch user info");
            }, function(userdata) {
                var response = {
                    resCode: 200,
                    response: userdata
                }
                res.status(200).send(response)
            })
        } else {
            res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
        }
    })
    /* This api is to Update client information and only admin can access this api
       Created  : 17-04-2017
    */

router.post('/editClient', customodule.authenticate, function(req, res, next) {
        var clientName = req.body.clientName;
        var site = req.body.clientSite;
        var subscription = req.body.clientSubscription;
        var client_id = req.body.clientId;
        var status = req.body.clientStatus;        
        var manage_user_index = 1;
        if (manage_user_index != -1) {
            if (!client_id) {
                res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
            } else {
                var data = {
                    name: clientName,
                    site: site,
                    subscription: subscription,
                    status: status
                }
                var cond = {
                    client_id: client_id
                }
                Customermodel.updateClientByCondiction(data, cond, function(error) {
                    var response = {
                        resCode: 201,
                        response: error
                    }
                    res.status(200).send(response);
                }, function(success) {
                    var condiction = "u.client_id =" + cond.client_id;
                    var select = "u.client_id as client_id,u.name as client_name,u.site as site,u.subscription as subscription,u.status as status";                    
                    var join = "";
                    var param = {
                        condiction: condiction,
                        join: join,
                        select: select
                    }
                    Customermodel.getCustomerFullInfoByCondiction(param, function(err) {
                        customodule.helper.winstonContext(req, res).info("Error to fetch customer full info");
                    }, function(userdata) {
                        var response = {
                            resCode: 200,
                            response: "client information updated successfully",
                            userdata: userdata[0]
                        }
                        res.status(200).send(response);
                    });
                })
            }
        } else {
            res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
        }
    })
    /* Plan Section */
    /* This api is to delete plan and only admin can access this api
       Created  : 17-04-2017
    */
var storage = node_module.multer.diskStorage({
    destination: function(req, file, cb) {      
        var dest = 'public/images/plan';
        node_module.mkdirp(dest, function(err) {
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + '.' + 'jpg');
    }
});

var upload = node_module.multer({
    storage: storage
});
router.post('/createPlan', customodule.authenticate, upload.any(), function(req, res) {
    var planName = req.body.planName;
    var vserviceTemplateId = req.body.vserviceTemplateId;
    var display = req.body.displayName;
    var description = req.body.description;
    var detail = req.body.detail;
    var price = req.body.price;
    var image = req.files;
    var color1 = req.body.color1;
    var color2 = req.body.color2;
    var tag = req.body.tag;
    if (req.body.featured == "true" || req.body.featured == "TRUE" || req.body.featured == true) {
        var featured = 1;
    } else {
        var featured = 0;
    }
    if (req.body.allow_subscription == "true" || req.body.allow_subscription == "TRUE" || req.body.allow_subscription == true) {
        var allow_subscription = 1;
    } else {
        var allow_subscription = 0;
    }
    var permissions = req.session.permissions;
    //var manage_user_index = permissions.indexOf(PERMISSION.user_manage);
    var manage_user_index = 1;
    if (manage_user_index != -1) {
        var data = {
            plan_name: planName,
            vservice_template_id: vserviceTemplateId,
            display_name: display,
            description: description,
            detail: detail,
            color1: color1,
            color2: color2,
            price: price,
            featured: featured,
            allow_subscription: allow_subscription
        }
        if (typeof image != 'undefined' && image != '' && image != null) {
            data.image = image[0].filename;
        }
        Customermodel.createPlan(data, function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success) {
                data.plan_id = success;
                //Tag insert condition
                if (typeof tag != 'undefined' && tag != '' && tag != null) {
                    var tagSplit = tag.split(",");
                    var tagObject = [];
                    for (var count = 0; count < tagSplit.length; count++) {
                        tagObject.push({
                            "plan_id": data.plan_id,
                            "tag_id": tagSplit[count]
                        });
                        if (count == tagSplit.length - 1) {
                            Customermodel.createTagPlan(tagObject, function(error) {}, function(success2) {
                                var condiction = "p.plan_id =" + success;
                                var select = "p.plan_id as plan_id,(SELECT GROUP_CONCAT(tg.name) as tag_name from tag as tg LEFT JOIN plan_tag as ptg ON tg.tag_id=ptg.tag_id WHERE ptg.plan_id = p.plan_id) as plan_tag,(SELECT GROUP_CONCAT(tg.tag_id) as tag_name from tag as tg LEFT JOIN plan_tag as ptg ON tg.tag_id=ptg.tag_id WHERE ptg.plan_id = p.plan_id) as plan_tag_ids,ifnull(p.plan_name,'') as plan_name,CASE WHEN p.featured=1 THEN 'true' ELSE 'false' END as featured,CASE WHEN p.allow_subscription=1 THEN 'true' ELSE 'false' END as allow_subscription,ifnull(p.display_name,'') as display_name, ifnull(p.description,'') as description,ifnull(p.detail,'') as detail,ifnull(p.image,'') as image,ifnull(p.color1,'') as color1, ifnull(p.color2,'') as color2,ifnull(p.price,'0') as price,ifnull(v.name,'') as vservice_template_name,v.vservice_template_id as vservice_template_id";
                                var join = "LEFT JOIN vservice_template as v ON v.vservice_template_id=p.vservice_template_id";
                                var param = {
                                    condiction: condiction,
                                    join: join,
                                    select: select
                                }
                                Customermodel.getPlanByConditionWithServiceTemplate(param, function(err) {
                                    customodule.helper.winstonContext(req, res).info("Getting error on fetching plans");
                                }, function(userdata) {
                                    var response = {
                                        resCode: 200,
                                        response: "Plan created successfully",
                                        userdata: userdata
                                    }
                                    res.status(200).send(response);
                                });
                            });
                        }
                    }
                } else {
                    var condiction = "p.plan_id =" + success;
                    var select = "p.plan_id as plan_id,(SELECT GROUP_CONCAT(tg.name) as tag_name from tag as tg LEFT JOIN plan_tag as ptg ON tg.tag_id=ptg.tag_id WHERE ptg.plan_id = p.plan_id) as plan_tag,(SELECT GROUP_CONCAT(tg.tag_id) as tag_name from tag as tg LEFT JOIN plan_tag as ptg ON tg.tag_id=ptg.tag_id WHERE ptg.plan_id = p.plan_id) as plan_tag_ids,ifnull(p.plan_name,'') as plan_name,CASE WHEN p.featured=1 THEN 'true' ELSE 'false' END as featured,CASE WHEN p.allow_subscription=1 THEN 'true' ELSE 'false' END as allow_subscription,ifnull(p.display_name,'') as display_name, ifnull(p.description,'') as description,ifnull(p.detail,'') as detail,ifnull(p.image,'') as image,ifnull(p.color1,'') as color1, ifnull(p.color2,'') as color2,ifnull(p.price,'0') as price,ifnull(v.name,'') as vservice_template_name,v.vservice_template_id as vservice_template_id";
                    var join = "LEFT JOIN vservice_template as v ON v.vservice_template_id=p.vservice_template_id";
                    var param = {
                        condiction: condiction,
                        join: join,
                        select: select
                    }
                    Customermodel.getPlanByConditionWithServiceTemplate(param, function(err) {
                        customodule.helper.winstonContext(req, res).info("Getting error on fetching plans");
                    }, function(userdata) {
                        var response = {
                            resCode: 200,
                            response: "Plan created successfully.",
                            userdata: userdata
                        }
                        res.status(200).send(response);
                    });
                }
            })
            //}
    } else {
        res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
});
/* Plan Section */
/* This api is to delete plan and only admin can access this api
   Created  : 17-04-2017
*/
router.get('/deletePlan', customodule.authenticate, function(req, res) {
    var plan_id = req.query.planId;
    if (!plan_id) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        var permissions = req.session.permissions;
        var manage_user_index = 1;
        if (manage_user_index != -1) {
            var cond = {
                plan_id: plan_id
            }
            Customermodel.getPlanByCondition(cond.plan_id, function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success) {
                Customermodel.deletePlan(cond, function(error) {
                    var response = {
                        resCode: 201,
                        response: error
                    }
                    res.status(200).send(response);
                }, function(success) {
                    if (success) {
                        Customermodel.deleteTagPlan(cond, function(error) {},
                            function(successPlanTag) {
                                var response = {
                                    resCode: 200,
                                    response: "Plan deleted successfully"
                                }
                                res.status(200).send(response);
                            });
                    }
                })
            })
        } else {
            res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
        }
    }
});
/* Plan Section */
/* This api is to delete plan and only admin can access this api
   Created  : 17-04-2017
*/
router.post('/editPlan', upload.any(), function(req, res) {
    var plan_id = req.body.planId;
    var planName = req.body.planName;
    var vserviceTemplateId = req.body.vserviceTemplateId;
    var display = req.body.displayName;
    var description = req.body.description;
    var detail = req.body.detail;
    var image = req.files;
    var color1 = req.body.color1;
    var color2 = req.body.color2;
    var price = req.body.price;
    var featured = req.body.featured;
    var allow_subscription = req.body.allow_subscription;
    var tag = req.body.tag
    var permissions = req.session.permissions;
    //var manage_user_index = permissions.indexOf(PERMISSION.user_manage);
    var manage_user_index = 1;
    if (manage_user_index != -1) {
        if (!plan_id) {
            res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
        } else {
            if (req.body.featured == "true" || req.body.featured == "TRUE" || req.body.featured == true) {
                var featured = 1;
            } else {
                var featured = 0;
            }
            if (req.body.allow_subscription == "true" || req.body.allow_subscription == "TRUE" || req.body.allow_subscription == true) {
                var allow_subscription = 1;
            } else {
                var allow_subscription = 0;
            }
            if (typeof image != 'undefined' && image != '' && image != null) {

                var data = {
                    plan_name: planName,
                    vservice_template_id: vserviceTemplateId,
                    display_name: display,
                    description: description,
                    detail: detail,
                    image: image[0].filename,
                    color1: color1,
                    color2: color2,
                    price: price,
                    featured: featured,
                    allow_subscription: allow_subscription
                }
            } else {
                var data = {
                    plan_name: planName,
                    vservice_template_id: vserviceTemplateId,
                    display_name: display,
                    description: description,
                    detail: detail,
                    color1: color1,
                    color2: color2,
                    price: price,
                    featured: featured,
                    allow_subscription: allow_subscription
                }
            }
            var cond = {
                plan_id: plan_id
            }
            Customermodel.updatePlanByCondiction(data, cond, function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success) {
                if (typeof tag != 'undefined' && tag != '' && tag != null && tag != 0) {
                    Customermodel.deleteTagPlan(cond, function(error) {},
                        function(successPlanTag) {
                            var tagSplit = tag.split(",");
                            var tagObject = [];
                            for (var count = 0; count < tagSplit.length; count++) {
                                tagObject.push({
                                    "plan_id": plan_id,
                                    "tag_id": tagSplit[count]
                                });
                                if (count == tagSplit.length - 1) {
                                    Customermodel.createTagPlan(tagObject, function(error) {}, function(successCreatePlan) {
                                        var condiction = "p.plan_id =" + plan_id;
                                        //var select      = "p.plan_id as plan_id,ifnull(p.plan_name,'') as plan_name,p.featured as featured,ifnull(p.display_name,'') as display_name, ifnull(p.description,'') as description,ifnull(p.image,'') as image,ifnull(p.color1,'') as color1, ifnull(p.color2,'') as color2,ifnull(v.name,'') as vservice_template_name,v.vservice_template_id as vservice_template_id";
                                        //var join        = "LEFT JOIN vservice_template as v ON v.vservice_template_id=p.vservice_template_id";
                                        var select = "p.plan_id as plan_id,(SELECT GROUP_CONCAT(tg.name) as tag_name from tag as tg LEFT JOIN plan_tag as ptg ON tg.tag_id=ptg.tag_id WHERE ptg.plan_id = p.plan_id) as plan_tag,(SELECT GROUP_CONCAT(tg.tag_id) as tag_name from tag as tg LEFT JOIN plan_tag as ptg ON tg.tag_id=ptg.tag_id WHERE ptg.plan_id = p.plan_id) as plan_tag_ids,ifnull(p.plan_name,'') as plan_name,CASE WHEN p.featured=1 THEN 'true' ELSE 'false' END as featured,CASE WHEN p.allow_subscription=1 THEN 'true' ELSE 'false' END as allow_subscription,ifnull(p.display_name,'') as display_name, ifnull(p.description,'') as description,ifnull(p.detail,'') as detail,ifnull(p.image,'') as image,ifnull(p.color1,'') as color1, ifnull(p.color2,'') as color2,ifnull(p.price,'0') as price,ifnull(v.name,'') as vservice_template_name,v.vservice_template_id as vservice_template_id";
                                        var join = "LEFT JOIN vservice_template as v ON v.vservice_template_id=p.vservice_template_id";
                                        var param = {
                                            condiction: condiction,
                                            join: join,
                                            select: select
                                        }
                                        Customermodel.getPlanByConditionWithServiceTemplate(param, function(err) {
                                            customodule.helper.winstonContext(req, res).info("Getting error on fetching plans with service template");
                                        }, function(userdata) {
                                            if (featured == 1) {
                                                userdata[0].featured = "true"
                                            } else {
                                                userdata[0].featured = "false"
                                            }
                                            if (allow_subscription == 1) {
                                                userdata[0].allow_subscription = "true"
                                            } else {
                                                userdata[0].allow_subscription = "false"
                                            }
                                            var response = {
                                                resCode: 200,
                                                response: "client information updated successfully",
                                                userdata: userdata
                                            }
                                            res.status(200).send(response);
                                        });
                                    });
                                }
                            }
                        });
                } else {
                    Customermodel.deleteTagPlan(cond, function(error) {},
                        function(successPlanTag) {
                            var condiction = "p.plan_id =" + plan_id;
                            var select = "p.plan_id as plan_id,ifnull(p.plan_name,'') as plan_name,p.featured as featured,ifnull(p.display_name,'') as display_name, ifnull(p.description,'') as description,ifnull(p.detail,'') as detail,ifnull(p.image,'') as image,ifnull(p.color1,'') as color1, ifnull(p.color2,'') as color2,ifnull(p.price,'0') as price,ifnull(v.name,'') as vservice_template_name,v.vservice_template_id as vservice_template_id";
                            var join = "LEFT JOIN vservice_template as v ON v.vservice_template_id=p.vservice_template_id";
                            var param = {
                                condiction: condiction,
                                join: join,
                                select: select
                            }
                            Customermodel.getPlanByConditionWithServiceTemplate(param, function(err) {
                                customodule.helper.winstonContext(req, res).info("Getting error on fetching plans with service template");
                            }, function(userdata) {
                                if (featured == 1) {
                                    userdata[0].featured = "true"
                                } else {
                                    userdata[0].featured = "false"
                                }
                                var response = {
                                    resCode: 200,
                                    response: "client information updated successfully",
                                    userdata: userdata
                                }
                                res.status(200).send(response);
                            });
                        });
                }
            })
        }
    } else {
        res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
});
router.post('/getVserviceTemplateList', customodule.authenticate, function(req, res) {
    var permissions = req.session.permissions;
    //var manage_user_index = permissions.indexOf(PERMISSION.user_manage);
    var manage_user_index = 1;
    if (manage_user_index != -1) {
        Customermodel.getVServiceTemplate(function(error) {
            var response = {
                resCode: 201,
                response: error
            }
            res.status(200).send(response);
        }, function(success) {
            var response = {
                resCode: 200,
                response: success
            }
            res.status(200).send(response);
        })
    } else {
        res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
});
/* This api is to get all Plan and only admin can access this api
   Created  : 18-04-2017
*/
router.get('/getPlanList', customodule.authenticate, function(req, res, next) {
        var permissions = req.session.permissions;
        var manage_user_index = 1;
        if (manage_user_index != -1) {
            var getPlanQuery = "SELECT p.plan_id as plan_id,(SELECT GROUP_CONCAT(tg.name) as tag_name from tag as tg LEFT JOIN plan_tag as ptg ON tg.tag_id=ptg.tag_id WHERE ptg.plan_id = p.plan_id) as plan_tag,(SELECT GROUP_CONCAT(tg.description) as tag_description from tag as tg LEFT JOIN plan_tag as ptg ON tg.tag_id=ptg.tag_id WHERE ptg.plan_id = p.plan_id) as plan_tag_desc,(SELECT GROUP_CONCAT(tg.tag_id) as tag_name from tag as tg LEFT JOIN plan_tag as ptg ON tg.tag_id=ptg.tag_id WHERE ptg.plan_id = p.plan_id) as plan_tag_ids,ifnull(p.plan_name,'') as plan_name,CASE WHEN p.featured=1 THEN 'true' ELSE 'false' END as featured,CASE WHEN p.allow_subscription=1 THEN 'true' ELSE 'false' END as allow_subscription,ifnull(p.display_name,'') as display_name, ifnull(p.description,'') as description,ifnull(p.detail,'') as detail,ifnull(p.image,'') as image,ifnull(p.color1,'') as color1, ifnull(p.color2,'') as color2,ifnull(p.price,'0') as price,ifnull(v.name,'') as vservice_template_name,v.vservice_template_id as vservice_template_id from plan as p  LEFT JOIN vservice_template as v ON v.vservice_template_id=p.vservice_template_id where p.plan_id NOT IN (SELECT distinct(plan_id) from plan INNER JOIN vservice_template ON vservice_template.vservice_template_id = plan.vservice_template_id INNER JOIN vservice_template_addon ON vservice_template_addon.addon_id = vservice_template.vservice_template_id) ORDER BY p.allow_subscription DESC "
            Customermodel.getVServiceTemplate(function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success2) {
                Customermodel.getPlanWithServiceTemplateList(getPlanQuery, function(err) {
                    customodule.helper.winstonContext(req, res).info("Getting error on fetching plans with service template");
                }, function(userdata) {
                    var response = {
                        resCode: 200,
                        response: userdata,
                        absoluteLocalPath: SETTING.pathLocal,
                        absoluteLivePath: SETTING.pathLive,
                        serviceTemplate: success2
                    }
                    res.status(200).send(response)
                })
            });
        } else {
            res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
        }
    })
    /* Customer Management Section */

/* This api is to get all Plan and only admin can access this api
   Created  : 18-04-2017
*/
router.get('/getPlanListAdmin', customodule.authenticate, function(req, res, next) {
        var permissions = req.session.permissions;
        var manage_user_index = 1;
        if (manage_user_index != -1) {
            var condiction = " p.plan_id !=0 ORDER BY p.featured DESC";
            var select = "p.plan_id as plan_id,(SELECT GROUP_CONCAT(tg.name) as tag_name from tag as tg LEFT JOIN plan_tag as ptg ON tg.tag_id=ptg.tag_id WHERE ptg.plan_id = p.plan_id) as plan_tag,(SELECT GROUP_CONCAT(tg.description) as tag_description from tag as tg LEFT JOIN plan_tag as ptg ON tg.tag_id=ptg.tag_id WHERE ptg.plan_id = p.plan_id) as plan_tag_desc,(SELECT GROUP_CONCAT(tg.tag_id) as tag_name from tag as tg LEFT JOIN plan_tag as ptg ON tg.tag_id=ptg.tag_id WHERE ptg.plan_id = p.plan_id) as plan_tag_ids,ifnull(p.plan_name,'') as plan_name,CASE WHEN p.featured=1 THEN 'true' ELSE 'false' END as featured,CASE WHEN p.allow_subscription=1 THEN 'true' ELSE 'false' END as allow_subscription,ifnull(p.display_name,'') as display_name, ifnull(p.description,'') as description,ifnull(p.detail,'') as detail,ifnull(p.image,'') as image,ifnull(p.color1,'') as color1, ifnull(p.color2,'') as color2,ifnull(p.price,'0') as price,ifnull(v.name,'') as vservice_template_name,v.vservice_template_id as vservice_template_id";
            var join = "LEFT JOIN vservice_template as v ON v.vservice_template_id=p.vservice_template_id ";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getVServiceTemplate(function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success2) {
                Customermodel.getPlanWithServiceTemplate(param, function(err) {
                    customodule.helper.winstonContext(req, res).info("Getting error on fetching plans with service template");
                }, function(userdata) {
                    var response = {
                        resCode: 200,
                        response: userdata,
                        absoluteLocalPath: SETTING.pathLocal,
                        absoluteLivePath: SETTING.pathLive,
                        serviceTemplate: success2
                    }
                    res.status(200).send(response)
                })
            });
        } else {
            res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
        }
    })
    /* Customer Management Section */
    /* This api is to create client user and only admin can access this api
       Created  : 18-04-2017
    */
router.post('/createCustomerUser', customodule.authenticate, function(req, res, next) {
    var clientId = req.body.clientId;
    var email = req.body.customerUserEmail;
    var password = req.body.customerUserPassword;
    var user = req.body.customerUserName;
    var phone = req.body.customerUserPhone;
    var mobileNumber = req.body.customerUserMobilePhone;
    var role = req.body.customerUserRole;
    var status = req.body.customerUserStatus;
    var notes = req.body.customerUserNotes;
    var permissions = [];
    if(req.headers['clientregistration'] == 'Y')
    {
        permissions = req.session.permissions;
    }       
    //var manage_user_index = permissions.indexOf(PERMISSION.user_manage);
    var manage_user_index = 1;
    if (manage_user_index != -1) {
        if (!clientId) {
            res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
        } else {
            var data = {
                client_id: clientId,
                email: email,
                password: node_module.crypto.createHash('md5').update(customodule.setting.passprefix + password).digest("hex"),
                user: user,
                phone: phone,
                mobilePhone: mobileNumber,
                customer_role_id: role,
                status: status,
                notes: notes
            }
            Customermodel.createCustomerUser(data, function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success) {
                var condiction = "u.c_user_id =" + success;
                var select = "u.c_user_id as c_user_id,u.client_id as client_id,u.email as email,u.user as user,u.phone as phone,u.mobilePhone as mobilePhone,u.status as status,u.notes as notes,c.name as client_name,cr.name as c_role_name,cr.c_role_id as c_role_id";
                var join = "LEFT JOIN client as c ON c.client_id=u.client_id LEFT JOIN customer_role as cr ON u.customer_role_id = cr.c_role_id";
                var param = {
                    condiction: condiction,
                    join: join,
                    select: select
                }
                Customermodel.getCustomerUserFullInfoByCondiction(param, function(err) {
                    node_module.winston.info("Error to fetch data ", err);
                    customodule.helper.winstonContext(req, res).info("Getting error on fetching customer user ");
                }, function(userdata) {
                    var response = {
                        resCode: 200,
                        response: "Client user created successfully",
                        userdata: userdata[0]
                    }
                    res.status(200).send(response);
                })
            })
        }
    } else {
        res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
})
router.post('/getCustomerUserList', customodule.authenticate, function(req, res, next) {
    var permissions = req.session.permissions;
    var manage_user_index = 1;
    if(req.userType == 'USER')
    {
        var clientId = req.client_id;            
    }
    else
    {
        var clientId = req.body.client_id;
    }
    if (manage_user_index != -1) {
        if (!clientId) {
            res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
        } else {
            var condiction = "u.client_id =" + clientId;
            var select = "u.c_user_id as c_user_id,u.d_user_id as d_user_id,u.client_id as client_id,u.email as email,u.user as user,u.phone as phone,u.mobilePhone as mobilePhone,u.status as status,u.notes as notes,c.name as client_name,cr.name as c_role_name,cr.c_role_id as c_role_id";
            var join = "LEFT JOIN client as c ON c.client_id=u.client_id LEFT JOIN customer_role as cr ON u.customer_role_id = cr.c_role_id";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getCustomerUserFullInfoByCondiction(param, function(err) {
                customodule.helper.winstonContext(req, res).info("Getting error on fetching customer user");
            }, function(userdata) {
                var condiction = "u.client_id =" + clientId;
                var select = "u.client_id as client_id,u.name as client_name ";
                var join = "";
                var paramClient = {
                    condiction: condiction,
                    join: join,
                    select: select
                }
                Customermodel.getUserFullInfoByCondiction(paramClient, function(err) {
                    customodule.helper.winstonContext(req, res).info("Getting error on fetching user");
                }, function(clientDetails) {
                    var response = {
                        resCode: 200,
                        response: "Client User listing",
                        clientdetails: clientDetails,
                        userdata: userdata
                    }
                    res.status(200).send(response);
                });
            })
        }
    } else {
        res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
})

/* Customer User Section */
/* This api is to delete customer user and only admin can access this api
   Created  : 17-04-2017
*/
router.get('/deleteCustomerUser', customodule.authenticate, function(req, res) {
    var customer_user_id = req.query.customerUserId;
    if (!customer_user_id) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        var permissions = req.session.permissions;
        //var manage_user_index = permissions.indexOf(PERMISSION.user_manage);
        var manage_user_index = 1;
        if (manage_user_index != -1) {
            var cond = {
                c_user_id: customer_user_id
            }
            Customermodel.deleteCustomerUser(cond, function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success) {
                if (success) {
                    var response = {
                        resCode: 200,
                        response: "Customer user deleted successfully"
                    }
                    res.status(200).send(response);
                }
            })
        } else {
            res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
        }
    }
});
/* Customer User Section */
/* This api is to edit customer user and only admin can access this api
   Created  : 17-04-2017
*/
router.post('/editCustomerUser', customodule.authenticate, function(req, res, next) {
        var user = req.body.customerUserName;
        var phone = req.body.customerUserPhone;
        var mobileNumber = req.body.customerUserMobilePhone;
        var role = req.body.customerUserRole;
        var status = req.body.customerUserStatus;
        var notes = req.body.customerUserNotes;
        var customerUserId = req.body.customerUserId;
        var email = req.body.customerUserEmail;
        //var manage_user_index = permissions.indexOf(PERMISSION.user_manage);
        var manage_user_index = 1;
        if (manage_user_index != -1) {
            if (!customerUserId) {
                res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
            } else {
                var data = {
                    user: user,
                    phone: phone,
                    mobilePhone: mobileNumber,
                    customer_role_id: role,
                    status: status,
                    notes: notes,
                    email: email
                }
                var cond = {
                        c_user_id: customerUserId
                    }
                    //check wheather the email id belongs to this user or not
                Customermodel.updateCustomerUserByCondiction(data, cond, function(error) {
                    var response = {
                        resCode: 201,
                        response: error
                    }
                    res.status(200).send(response);
                }, function(success) {
                    var condiction = "u.c_user_id =" + customerUserId;
                    var select = "u.c_user_id as c_user_id,u.client_id as client_id,u.email as email,u.user as user,u.phone as phone,u.mobilePhone as mobilePhone,u.status as status,u.notes as notes,c.name as client_name,cr.name as c_role_name,cr.c_role_id as c_role_id";
                    var join = "LEFT JOIN client as c ON c.client_id=u.client_id LEFT JOIN customer_role as cr ON u.customer_role_id = cr.c_role_id";
                    var param = {
                        condiction: condiction,
                        join: join,
                        select: select
                    }
                    Customermodel.getCustomerUserFullInfoByCondiction(param, function(err) {
                        customodule.helper.winstonContext(req, res).info("Getting error on fetching customer user");
                    }, function(userdata) {
                        var response = {
                            resCode: 200,
                            response: "Client user edit successfully",
                            userdata: userdata[0]
                        }
                        res.status(200).send(response);
                    })
                })
            }
        } else {
            res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
        }
    })
    /* This api is to create Tag and only admin can access this api
       Created  : 19-04-2017
    */
router.post('/createTag', customodule.authenticate, function(req, res, next) {
        var name = req.body.tagName;
        var description = req.body.tagDescription;
        var tagColor    = req.body.tagColor;
        var permissions = req.session.permissions;
        //var manage_user_index = permissions.indexOf(PERMISSION.user_manage);
        var manage_user_index = 1;
        if (manage_user_index != -1) {
            var data = {
                name: req.body.tagName,
                description: req.body.tagDescription,
                tag_color:req.body.tagColor

            }
            Customermodel.createTag(data, function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success) {
                var condiction = "t.tag_id =" + success;
                var select = "t.tag_id as tag_id,t.name as tag_name,t.description as tag_description,t.tag_color as tagColor";
                var join = "";
                var param = {
                    condiction: condiction,
                    join: join,
                    select: select
                }
                Customermodel.getTagFullInfoByCondiction(param, function(err) {
                    customodule.helper.winstonContext(req, res).info("Getting error on fetching tag");
                }, function(userdata) {
                    var response = {
                        resCode: 200,
                        response: "Tag created successfully",
                        userdata: userdata[0]
                    }
                    res.status(200).send(response);
                })
            })
        } else {
            res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
        }
    })
    /* This api is to list Tag and only admin can access this api
       Created  : 19-04-2017
    */
router.get('/getTagList', customodule.authenticate, function(req, res, next) {
        var permissions = req.session.permissions;
        //var manage_user_index = permissions.indexOf(PERMISSION.user_manage);
        //var user_list_index = permissions.indexOf(PERMISSION.user_list);
        var manage_user_index = 1;
        if (manage_user_index != -1) {
            if (req.query.tagId) {
                var condiction = "t.tag_id =" + req.query.tagId;
            } else {
                var condiction = "t.tag_id !=1";
            }
            var select = "t.tag_id as tag_id,t.name as tag_name,t.description as tag_description,t.tag_color as tagColor";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getTagFullInfoByCondiction(param, function(err) {
                customodule.helper.winstonContext(req, res).info("Getting error on fetching tag");
            }, function(userdata) {
                var response = {
                    resCode: 200,
                    response: "Tag listing",
                    userdata: userdata
                }
                res.status(200).send(response);
            })
        } else {
            res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
        }
    })
    /* This api is to delete tag and only admin can access this api
       Created  : 17-04-2017
    */
router.get('/deleteTag', customodule.authenticate, function(req, res) {
    var tag_id = req.query.tagId;
    if (!tag_id) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        var permissions = req.session.permissions;
        //var manage_user_index = permissions.indexOf(PERMISSION.user_manage);
        var manage_user_index = 1;
        if (manage_user_index != -1) {
            var cond = {
                tag_id: tag_id
            }
            Customermodel.deleteTagUser(cond, function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success) {
                if (success) {
                    var response = {
                        resCode: 200,
                        response: "Tag deleted successfully"
                    }
                    res.status(200).send(response);
                }
            })
        } else {
            res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
        }
    }
});
/* Customer User Section */
/* This api is to edit tag and only admin can access this api
   Created  : 17-04-2017
*/
router.post('/editTag', customodule.authenticate, function(req, res, next) {
    var name = req.body.tagName;
    var description = req.body.tagDescription;
    var tagId = req.body.tagId;
    var tagColor = req.body.tagColor;
    //var manage_user_index = permissions.indexOf(PERMISSION.user_manage);
    var manage_user_index = 1;
    if (manage_user_index != -1) {
        if (!tagId) {
            res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
        } else {
            var data = {
                name: req.body.tagName,
                description: req.body.tagDescription,
                tag_color: req.body.tagColor
            }
            var cond = {
                tag_id: tagId
            }
            Customermodel.updateTagByCondiction(data, cond, function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success) {
                var condiction = "t.tag_id =" + tagId;
                var select = "t.tag_id as tag_id,t.name as tag_name,t.description as tag_description,t.tag_color as tagColor";
                var join = "";
                var param = {
                    condiction: condiction,
                    join: join,
                    select: select
                }
                Customermodel.getTagFullInfoByCondiction(param, function(err) {
                    customodule.helper.winstonContext(req, res).info("Getting error on fetching tag");
                }, function(userdata) {
                    var response = {
                        resCode: 200,
                        response: "Tag edit successfully",
                        userdata: userdata[0]
                    }
                    res.status(200).send(response);
                })
            })
        }
    } else {
        res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
})

router.post('/editTag', customodule.authenticate, function(req, res, next) {
    var name = req.body.tagName;
    var description = req.body.tagDescription;
    var tagId = req.body.tagId;
    var manage_user_index = 1;
    if (manage_user_index != -1) {
        if (!tagId) {
            res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
        } else {
            var data = {
                name: req.body.tagName,
                description: req.body.tagDescription
            }
            var cond = {
                tag_id: tagId
            }
            Customermodel.updateTagByCondiction(data, cond, function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success) {
                var condiction = "t.tag_id =" + tagId;
                var select = "t.tag_id as tag_id,t.name as tag_name,t.description as tag_description";
                var join = "";
                var param = {
                    condiction: condiction,
                    join: join,
                    select: select
                }
                Customermodel.getTagFullInfoByCondiction(param, function(err) {
                    customodule.helper.winstonContext(req, res).info("Getting error on fetching tag");
                }, function(userdata) {
                    var response = {
                        resCode: 200,
                        response: "Tag edit successfully",
                        userdata: userdata[0]
                    }
                    res.status(200).send(response);
                })
            })
        }
    } else {
        res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
})

router.post('/customerUserLogin', customodule.validateapi, function(req, res, next) {
    var customerUserEmail = req.body.customerUserEmail;
    var customerUserPassword = req.body.customerUserPassword;
    if (!customerUserEmail || !customerUserPassword) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        var md5Password = node_module.crypto.createHash('md5').update(customodule.setting.passprefix + customerUserPassword).digest("hex");
        var condiction = "u.email ='" + customerUserEmail + "' and u.password = '" + md5Password + "'";
        var select = "u.client_id as client_id,u.c_user_id as c_user_id,u.d_user_id,c.name as client_name,u.email as email,u.user as user,u.phone as phone,u.mobilePhone as mobilePhone,u.customer_role_id as customer_role_id,u.status as status,u.notes as notes,concat('" + SETTING.profileImagePath + "',CASE WHEN u.profile_image='' THEN '' ELSE u.profile_image END) as profile_image,ifnull(u.customer_role_id,'') as userRoleId ,ifnull(cr.name,'') as userRole,(SELECT GROUP_CONCAT(p.name) from customer_role_permissions as rp LEFT JOIN customer_permission as p ON rp.c_permission_id=p.c_permission_id where rp.c_role_id=u.customer_role_id) as permissionName, (SELECT GROUP_CONCAT(name) from customer_permission where 1) as fullpermission ";
        var join = "LEFT JOIN client as c ON u.client_id = c.client_id LEFT JOIN customer_role as cr ON cr.c_role_id=u.customer_role_id";
        var param = {
            condiction: condiction,
            join: join,
            select: select
        }
        Customermodel.getCustomerUserFullInfoByCondiction(param, function(err) {
            customodule.helper.winstonContext(req, res).info("Getting error on fetching customer user");
        }, function(userdata) {
            if (typeof userdata != 'undefined' && userdata != 'null' && userdata != '') {
                if (userdata.length > 0) {
                    var ret = new Date();
                    var exp_time = ret.setTime(ret.getTime() + 60 * 24 * 60000);
                    var expiredate = node_module.datetime.create(new Date(exp_time));
                    var expire = expiredate.format("Y-m-d H:M:S");
                    var access_token = node_module.jwt.sign({
                        time: new Date().getTime()
                    }, customodule.setting.jwtsecret);
                    /* End */
                    var token_data = {
                        client_id: userdata[0].client_id,
                        customer_user_id: userdata[0].c_user_id,
                        token: access_token,
                        expire_time: expire
                    }

                    Customermodel.createCustomerPortalAccessToken(token_data, function(err) {}, function(success) {
                        //sess = req.session;
                        //sess.admin = (userdata[0].userRoleId==1 || userdata[0].userRoleId=='1' )?true:false;
                        //sess.permissions = userdata[0].permissionName.split(',');
                        var subscriptionListApiPath = '';
                        subscriptionListApiPath = SETTING.clientSavePath + '/' + token_data.client_id + '/subscriptions';
                        globalAPI.listGlobalApi(subscriptionListApiPath, function(error) {
                            var response = {
                                resCode: 201,
                                response: error
                            }
                            res.status(200).send(response);
                        }, function(data, response) {
                            var response = {
                                resCode: 200,
                                token: access_token,
                                response: userdata[0]
                            }
                            if (typeof data != 'undefined' && data != '' && data != null) {
                                response.response.no_of_services = data.length
                            } else {
                                response.response.no_of_services = 0
                            }
                            res.status(200).send(response);
                        });
                    })
                } else {
                    var response = {
                        resCode: 201,
                        response: "Invalid Username and password"
                    }
                    res.status(200).send(response);
                }
            } else {
                var response = {
                    resCode: 201,
                    response: "Invalid Username and password"
                }
                res.status(200).send(response);
            }
        });
    }
});
/* This api is to change password
   Created  : 30-03-2017
*/
router.post('/changePassword', customodule.authenticate, function(req, res, next) {
    var old_password = req.body.oldPassword;
    var new_password = req.body.newPassword;
    var userId = req.body.userId;
    if (!old_password || !new_password || !userId) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        var data = {
            password: node_module.crypto.createHash('md5').update(customodule.setting.passprefix + new_password).digest("hex"),
        }
        var condiction = {
            password: node_module.crypto.createHash('md5').update(customodule.setting.passprefix + old_password).digest("hex"),
            c_user_id: userId,
            status: '1'
        }
        Customermodel.getCustomerByCondiction(condiction, function(err) {
            customodule.helper.winstonContext(req, res).info("Getting error on fetching customer");
        }, function(users) {
            if (users.length > 0) {
                Customermodel.updateCustomerUserByCondiction(data, condiction, function(err) {
                    customodule.helper.winstonContext(req, res).info("Error on updating customer user");
                }, function(success) {
                    if (success) {
                        var response = {
                            resCode: 200,
                            response: "You have successfully change your password"
                        }
                        res.status(200).send(response);
                    }
                })
            } else {
                res.status(200).send(ERROR.USER.INVALIDUSERPASSWORD);
            }
        })
    }
})

/* Api to logout customer user
  Created  : 24-04-2017
*/
router.post('/customerLogout', customodule.authenticate, function(req, res) {
    var customerUserid = req.body.customerUserId;
    var clientid = req.body.clientId;
    var cond = {
        client_id: clientid,
        customer_user_id: customerUserid,
        token: req.headers['token']
    }
    Customermodel.removeCustomerPortalAccessToken(cond, function(error) {}, function(success) {
        if (success) {
            req.session.destroy(function(err) {
                if (err) {
                    res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
                } else {
                    var response = {
                        resCode: 200,
                        response: "You are logout successfully"
                    }
                    res.status(200).send(response);
                }
            })
        }
    })
})

/* Api to Get connection type list
  Created  : 25-04-2017
*/
router.get('/getConnectionType', customodule.authenticate, function(req, res, next) {
        var customerTypeId = req.query.customerTypeId;
        if (typeof customerTypeId != 'undefined' && customerTypeId != '' && customerTypeId != null) {
            var condiction = "ct.connection_type_id ='" + customerTypeId + "'";
        } else {
            var condiction = "ct.connection_type_id != 0";
        }


        var select = "ct.connection_type_id as connection_type_id,ct.name as connection_type_name";
        var join = "";
        var param = {
            condiction: condiction,
            join: join,
            select: select
        }
        Customermodel.getConnectionTypeFullInfoByCondiction(param, function(err) {
            customodule.helper.winstonContext(req, res).info("Getting error on fetching connection type");
        }, function(userdata) {
            var response = {
                resCode: 200,
                response: userdata
            }
            res.status(200).send(response);
        });
    })
    /* Api to Get Site list
      Created  : 25-04-2017
    */
router.post('/getSiteList', customodule.authenticate, function(req, res, next) {       
        var clientId = '';
        if(req.userType == 'USER')
        {
            //Get client id from the customer portal
            clientId = req.client_id;            
        }
        else
        {
            //Get client id from the admin portal
            clientId = req.body.clientId;           
        }        
        if (typeof clientId != 'undefined' && clientId != '' && clientId != null) {
            var condiction = "st.client_id ='" + clientId + "'";
        } else {
            var condiction = "st.client_id != 0";
        }

        var select = "st.site_id as site_id,st.client_id as client_id,st.status as status,st.connection_type_id as connection_type_id,st.name as site_name,ct.name as connection_type_name";
        var join = "LEFT JOIN site as st ON st.connection_type_id = ct.connection_type_id";
        var param = {
            condiction: condiction,
            join: join,
            select: select
        }
        Customermodel.getSiteFullInfoByCondiction(param, function(err) {
            customodule.helper.winstonContext(req, res).info("Getting error on fetching site");
        }, function(userdata) {
            var response = {
                resCode: 200,
                response: userdata
            }
            res.status(200).send(response);
        });
    })
    /* Api to Get customer connection parameter list
      Created  : 25-04-2017
    */
router.get('/getCustomerParameterList', customodule.authenticate, function(req, res, next) {
    var connectionTypeParameterId = req.query.connectionTypeParameterId;
    var condiction = '';    
    if (typeof connectionTypeParameterId != 'undefined' && connectionTypeParameterId != '' && connectionTypeParameterId != null) {
        condiction = "ctp.connection_type_parameter_id ='" + connectionTypeParameterId + "'";
    } else {
        condiction = "ctp.connection_type_parameter_id != 0";
    }
    var select = "ctp.connection_type_parameter_id as connection_type_parameter_id,ctp.name as name,ctp.required as required,ctp.default_value as default_value,ctp.parameter_type as parameter_type,ctp.enum_values as enum_values";
    var join = "";
    var param = {
        condiction: condiction,
        join: join,
        select: select
    }
    Customermodel.getConnectionTypeParameterFullInfoByCondiction(param, function(err) {
        customodule.helper.winstonContext(req, res).info("Getting error on fetching connection type");
    }, function(userdata) {
        var userDetails = []
        if (typeof userdata != 'undefined' && userdata != '' && userdata != null) {
            for (var count = 0; count < userdata.length; count++) {
                userDetails.push({
                    "connection_type_parameter_id": userdata[count].connection_type_parameter_id,
                    "required": userdata[count].required,
                    "name": userdata[count].name,
                    "default_value": userdata[count].default_value,
                    "parameter_type": userdata[count].parameter_type
                })
                if (userdata[count].parameter_type == 'ENUM') {
                    userDetails[count].enumValueArray = userdata[count].enum_values.split(",")
                } else {
                    userDetails[count].enumValueArray = [];
                }
            }
            var response = {
                resCode: 200,
                response: userDetails
            }
        } else {
            var response = {
                resCode: 200,
                response: []
            }
        }
        res.status(200).send(response);
    });
})

/* Api to add site
  Created  : 26-04-2017
*/
router.post('/addSite', customodule.authenticate, function(req, res, next) {
        var clientId = req.body.client_id;
        var name = req.body.name;
        var connectionType = req.body.connection_type;
        req.body.id = req.userId;
        var userId = req.userId;
        var userType = req.userType;
        if (!clientId || !name || !connectionType) {
            res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
        } else {
            //send the request parameter through api call
            var args = {
                data: req.body,
                headers: {
                    "Content-Type": "application/json",
                    "id": req.userId,
                    "userIdType": req.userType
                },
                requestConfig: {
                    timeout: 10000, //request timeout in milliseconds
                    noDelay: true, //Enable/disable the Nagle algorithm
                    keepAlive: true, //Enable/disable keep-alive functionalityidle socket.
                    keepAliveDelay: 1000 //and optionally set the initial delay before the first keepalive probe is sent
                },
                responseConfig: {
                    timeout: 10000 //response timeout
                }
            };
            var siteUrl = SETTING.siteSavePath;
            globalAPI.saveGlobalApi(siteUrl, args, res, function(error) {
                customodule.helper.winstonContext(req, res).info("Getting error on calling global api ");
            }, function(data, response) {
                data.statusCode = response.statusCode;
                data.response = req.body;
                res.status(200).send(data);
            });
        }
    })
    /* Api to edit site
      Created  : 26-04-2017
      Modified : 24-05-2017
    */
router.post('/editSite/:siteId', customodule.authenticate, function(req, res, next) {
        var userId = req.userId;
        var userType = req.userType;
        req.body.id = userId;
        var name = req.body.name;
        var connectionType = req.body.connection_type;
        var siteId = req.params.siteId;
        if (!name || !connectionType) {
            res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
        } else {
            //send the request parameter through api call
            var args = {
                data: req.body,
                headers: {
                    "Content-Type": "application/json",
                    "id": req.userId,
                    "userIdType": req.userType
                }
            };
            var siteUrl = SETTING.siteSavePath + siteId;
            globalAPI.editGlobalApi(siteUrl, args, function(error) {
                customodule.helper.winstonContext(req, res).info("Getting error on calling global api");
            }, function(data, response) {
                data.statusCode = response.statusCode;
                res.status(200).send(data);
            });
        }
    })
    /* Api for delete site
      Created  : 26-04-2017
    */
router.post('/deleteSite', customodule.authenticate, function(req, res, next) {
        var siteId = req.body.site_id;
        if (!siteId) {
            res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
        } else {
            var deleteArgs = {
                headers: {
                    "Content-Type": "application/json",
                    "id": req.userId,
                    "userIdType": req.userType
                }
            };
            //send the request parameter through api call
            var siteUrl = SETTING.siteSavePath + siteId;
            globalAPI.deleteGlobalApi(siteUrl, deleteArgs, function(error) {
                customodule.helper.winstonContext(req, res).info("Getting error on calling global api");
            }, function(data, response) {
                res.status(200).send(data);
            });
        }
    })
    /* Api to add client
      Created  : 27-04-2017
    */

router.post('/addClient', customodule.authenticate, function(req, res, next) {
        //REGISTRATION FROM CUSTOMER PORTAL
        if(req.headers['clientregistration'] == 'Y')
        {
            var userId = 0;
            var userType = 'USER';
        }
        else{
            var userId = req.userId;
            var userType = req.userType;
        }
        var name = req.body.name;
        var description = req.body.description;
        var status = req.body.status;
        if (!name || !description || !status) {
            res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
        } else {
            var condiction = "cup.provision_id != 0 ";
            var select = " cup.* ";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getCustomerUsersProvisionFullInfoByCondiction(param, function(err) {},
                function(prvisonRes) {
                    if (typeof prvisonRes == 'undefined' || Object.keys(prvisonRes).length == 0) {
                        var response = {
                            resCode: 201,
                            resMessage: "No customer user provision found"
                        }
                        res.status(200).send(response);
                    } else {
                        var jsonObject = {
                            "extra_args": {}
                        };
                        jsonObject.id = userId;
                        jsonObject.name = name;
                        jsonObject.status = status;
                        jsonObject.description = description;
                        jsonObject.extra_args.city = prvisonRes.city;
                        jsonObject.extra_args.state = prvisonRes.state;
                        jsonObject.extra_args.country = prvisonRes.country;
                        var args = {
                            data: jsonObject,
                            headers: {
                                "Content-Type": "application/json",
                                "id": userId,
                                "userIdType": userType
                            },
                            requestConfig: {
                                timeout: 10000, //request timeout in milliseconds
                                noDelay: true, //Enable/disable the Nagle algorithm
                                keepAlive: true, //Enable/disable keep-alive functionalityidle socket.
                                keepAliveDelay: 1000 //and optionally set the initial delay before the first keepalive probe is sent
                            },
                            responseConfig: {
                                timeout: 10000 //response timeout
                            }
                        };
                        globalAPI.saveGlobalApi(SETTING.clientSavePath, args, res, function(error) {
                            var response = {
                                resCode: 201,
                                response: error
                            }
                            res.status(201).send(response);
                        }, function(data, response) {
                            data.userdata = {};
                            data.resCode = response.statusCode;
                            data.userdata.name = name;
                            data.userdata.description = description;
                            data.userdata.status = status;
                            res.status(200).send(data);
                        });
                    }
                });
        }
    })
    /* Api to get all client list
      Created  : 27-04-2017
    */
router.get('/getClientList', customodule.authenticate, function(req, res, next) {
        var clientListApiPath = '';
        var clientId = '';
        if (req.query.client_id) {
            clientId = req.query.client_id;
            clientListApiPath = SETTING.clientSavePath + '/' + clientId;
        } else {
            clientListApiPath = SETTING.clientSavePath;
        }
        globalAPI.listGlobalApi(clientListApiPath, function(error) {
            customodule.helper.winstonContext(req, res).info("Getting error on calling global api");
        }, function(data, response) {
            data.resCode = 200;
            res.status(200).send(data);
        });
    })
    /* Api to delete client list
      Created  : 27-04-2017
    */
router.post('/deleteClient', customodule.authenticate, function(req, res, next) {
        var clientId = req.body.client_id;
        if (!clientId) {
            res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
        } else {
            var deleteArgs = {
                headers: {
                    "Content-Type": "application/json",
                    "id": req.userId,
                    "userIdType": req.userType
                }
            };
            clientDeleteApiPath = SETTING.clientSavePath + '/' + clientId;
            globalAPI.deleteGlobalApi(clientDeleteApiPath, deleteArgs, function(error) {
                customodule.helper.winstonContext(req, res).info("Getting error on calling global api");
            }, function(data, response) {
                data.resCode = 200;
                res.status(200).send(data);
            });
        }
    })
    /* Api to edit client
      Created  : 15-05-2017
    */
router.post('/updateClient', customodule.authenticate, function(req, res, next) {
    var clientId = req.body.client_id;
    var name = req.body.name;
    var status = req.body.status;
    var description = req.body.description;
    var userId = req.userId;
    var userType = req.userType;
    var reqClientParam = {};
    if (!clientId || !name || !status || !description) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        reqClientParam.id = userId;
        reqClientParam.name = name;
        reqClientParam.status = status;
        reqClientParam.description = description;
        //send the request parameter through api call
        var args = {
            data: reqClientParam,
            headers: {
                "Content-Type": "application/json",
                "id": req.userId,
                "userIdType": req.userType
            }
        };
        var clientUrl = SETTING.clientSavePath + '/' + clientId;
        globalAPI.editGlobalApi(clientUrl, args, function(error) {
            customodule.helper.winstonContext(req, res).info("Getting error on calling global api");
        }, function(data, response) {
            data.resCode = response.statusCode;
            data.userdata = {};
            data.userdata.name = name;
            data.userdata.description = description;
            data.userdata.status = status;
            data.userdata.client_id = clientId;
            res.status(200).send(data);
        });
    }
})
/* Api to get plan detail
    Created  : 28-04-2017
*/
router.post('/getPlanDetails', function(req, res, next) {
    var permissions = req.session.permissions;
    var planId = req.body.plan_id;
    var manage_role_index = 1;
    var role_list_index = 1;
    if (manage_role_index != -1 || role_list_index != -1) {
        if (!planId) {
            res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
        } else {
            var condition = "p.plan_id = " + planId + " ";
            Customermodel.getPlanDetails(condition, function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success) {
                if (success) {
                    var response = {
                        resCode: 200,
                        planInfo: success[0]
                    }
                    if (success.length > 0) {
                        var condition = "PRODUCT.vservice_template_id = " + success[0].vservice_template_id + " ";
                        Customermodel.getProductDetails(condition, function(producterror) {
                            var response = {
                                resCode: 201,
                                response: producterror
                            }
                            res.status(200).send(response);
                        }, function(productSuccess) {
                            if (productSuccess) {
                                response.planInfo.absoluteLocalPath = SETTING.pathLocal;
                                response.planInfo.absoluteLivePath = SETTING.pathLive;
                                if (productSuccess.length > 0) {
                                    response.planInfo.productInfo = productSuccess[0];
                                } else {
                                    response.planInfo.productInfo = {};
                                }
                                //get the detail from vservice_template_parameter table
                                var condition = "VTP.vservice_template_id = " + success[0].vservice_template_id + " ";
                                Customermodel.getServiceTemplateParamDetails(condition, function(productServiceTempParamerror) {
                                    var response = {
                                        resCode: 201,
                                        response: productServiceTempParamerror
                                    }
                                    res.status(200).send(response);
                                }, function(productServiceTempParameSuccess) {
                                    if (productServiceTempParameSuccess) {
                                        if (productServiceTempParameSuccess.length > 0) {
                                            response.planInfo.productInfo.configurationInfo = productServiceTempParameSuccess;
                                        } else {
                                            response.planInfo.productInfo.configurationInfo = {};
                                        }
                                        response.absoluteLocalPath = SETTING.pathLocal;
                                        response.absoluteLivePath = SETTING.pathLive;
                                        res.status(200).send(response);
                                    }
                                })
                            }
                        })
                    }
                } else {
                    var response = {
                        resCode: 201,
                        response: 'No record found !!'
                    }
                    res.status(200).send(response);
                }
            })
        }
    } else {
        res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
})
/* Api to get getCustomerUserList
    Created  : 28-04-2017
*/
router.get('/getBillingInfo', customodule.authenticate, function(req, res, next) {
    var permissions = req.session.permissions;
    var manage_user_index = 1;
    if (manage_user_index != -1) {
        var condiction = "billing.billing_info_id != 0";
        var select = "billing.billing_info_id,billing.company_name,billing.address,billing.phone,billing.email ";
        var join = "";
        var param = {
            condiction: condiction,
            join: join,
            select: select
        }
        Customermodel.getBillingFullInfoByCondiction(param, function(err) {
            customodule.helper.winstonContext(req, res).info("Getting error on fetching billing information");
        }, function(billingInfoData) {
            var response = {
                resCode: 200,
                response: "Client billing info listing",
                billingInfoData: billingInfoData
            }
            res.status(200).send(response);
        })

    } else {
        res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
})
router.post('/updateUserInfo', function(req, res, next) {
    var c_user_id = req.body.c_user_id;
    var d_user_id = req.body.d_user_id;
    var condiction = "user.c_user_id = " + c_user_id + " ";
    var condiction = {
        c_user_id: c_user_id
    }
    var data = {
        d_user_id: d_user_id
    }
    Customermodel.updateCustomerUserByCondiction(data, condiction, function(error) {
        var response = {
            resCode: 201,
            response: error
        }
        res.status(200).send(response);
    }, function(success) {
        var response = {
            resCode: 200,
            response: "User info updated successfully",
        }
        res.status(200).send(response);
    })
});

/* Api to add customer billing info
  Created  : 10-05-2017
*/
router.post('/addBillingInfo', customodule.authenticate, function(req, res, next) {
        var company_name = req.body.company_name;
        var address = req.body.address;
        var phone = req.body.phone;
        var email = req.body.email;
        var permissions = req.session.permissions;       
        var manage_user_index = 1;
        if (manage_user_index != -1) {
            if (!company_name || !address) {
                res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
            } else {
                //check wheather any data added or not
                var condiction = "billing.billing_info_id != 0";
                var select = "billing.billing_info_id,billing.company_name,billing.address,billing.phone,billing.email ";
                var join = "";
                var param = {
                    condiction: condiction,
                    join: join,
                    select: select
                }
                Customermodel.getBillingFullInfoByCondiction(param, function(err) {
                    var response = {
                        resCode: 201,
                        response: error
                    }
                    res.status(200).send(response);
                }, function(billingInfoDetail) {
                    if (billingInfoDetail.length > 0) {
                        var billingInfoId = billingInfoDetail[0]['billing_info_id'];
                    } else {
                        var billingInfoId = 0;
                    }
                    var data = {
                        company_name: company_name,
                        address: address,
                        phone: phone,
                        email: email
                    }
                    if (billingInfoId == 0) {
                        Customermodel.addBillingInfoModel(data, function(error) {
                            var response = {
                                resCode: 201,
                                response: error
                            }
                            res.status(200).send(response);
                        }, function(billingInfoId) {
                            data.billing_info_id = billingInfoId;
                            var response = {
                                resCode: 200,
                                response: "Billing info added successfully",
                                billingdata: data
                            }
                            res.status(200).send(response);
                        })
                    } else {
                        var data = {
                            company_name: company_name,
                            address: address,
                            phone: phone,
                            email: email
                        }
                        var condition = {
                            billing_info_id: billingInfoId
                        }
                        Customermodel.updateBillingInfoModel(condition, data, function(error) {
                            var response = {
                                resCode: 201,
                                response: error
                            }
                            res.status(200).send(response);
                        }, function(success) {
                            data.billing_info_id = billingInfoId;
                            var response = {
                                resCode: 200,
                                response: "Billing info updated successfully",
                                billingdata: data
                            }
                            res.status(200).send(response);
                        })
                    }
                })
            }
        } else {
            res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
        }
    })
    /* Api to add customer billing info
      Created  : 10-05-2017
    */
router.post('/updateBillingInfo', customodule.authenticate, function(req, res, next) {
    var billing_info_id = req.body.billing_info_id;
    var company_name = req.body.company_name;
    var address = req.body.address;
    var phone = req.body.phone;
    var email = req.body.email;
    var permissions = req.session.permissions;
    var manage_user_index = 1;
    if (manage_user_index != -1) {
        if (!billing_info_id || !company_name || !address) {
            res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
        } else {
            var data = {
                company_name: company_name,
                address: address,
                phone: phone,
                email: email
            }
            var condition = {
                billing_info_id: billing_info_id
            }
            Customermodel.updateBillingInfoModel(condition, data, function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success) {
                data.billing_info_id = billing_info_id;
                var response = {
                    resCode: 200,
                    response: "Billing info updated successfully",
                    billingdata: data
                }
                res.status(200).send(response);
            })
        }
    } else {
        res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
})

/* Api to add customer billing info
  Created  : 10-05-2017
*/
router.get('/deleteBillingInfo', customodule.authenticate, function(req, res, next) {
        var billing_info_id = req.query.billing_info_id;
        var permissions = req.session.permissions;
        var manage_user_index = 1;
        if (manage_user_index != -1) {
            if (!billing_info_id) {
                res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
            } else {
                var condition = {
                    billing_info_id: billing_info_id
                }
                Customermodel.deleteBillingInfoModel(condition, function(error) {
                    var response = {
                        resCode: 201,
                        response: error
                    }
                    res.status(200).send(response);
                }, function(success) {
                    var response = {
                        resCode: 200,
                        response: "Billing info deleted successfully",
                    }
                    res.status(200).send(response);
                })
            }
        } else {
            res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
        }
    })
    /* Api to get vservice template list
      Created  : 17-05-2017
    */
router.get('/getServiceList', customodule.authenticate, function(req, res, next) {
    var permissions = req.session.permissions;
    var manage_user_index = 1;
    if (manage_user_index != -1) {
        var condiction = "vt.vservice_template_id != 0";
        var select = "vt.vservice_template_id,vt.name,ifnull(vt.description,'') as description,ifnull(vt.detail,'') as detail,ifnull(vt.connection_html,'') as connection_html,concat('" + SETTING.serviceLivePath + "',CASE WHEN ISNULL(vt.logo) THEN 'no-img.jpg' ELSE vt.logo END) as logo ";
        var join = "";
        var param = {
            condiction: condiction,
            join: join,
            select: select
        }
        Customermodel.getServiceFullInfoByCondition(param, function(err) {
            customodule.helper.winstonContext(req, res).info("Getting error on fetching service infomation");
        }, function(serviceInfoData) {
            var response = {
                resCode: 200,
                serviceInfoData: serviceInfoData
            }
            res.status(200).send(response);
        })

    } else {
        res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
})

/* This api is to edit service
   Created  : 17-05-2017
*/
var serviceStorage = node_module.multer.diskStorage({
    destination: function(req, file, cb) {        
        var dest = 'public/images/services';
        node_module.mkdirp(dest, function(err) {
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + '.' + 'jpg');
    }
});
var uploadservice = node_module.multer({
    storage: serviceStorage
});
router.post('/editService', uploadservice.any(), function(req, res) {
    var name = req.body.name;
    var description = req.body.description;
    var detail = req.body.detail;
    var connection_html = req.body.connection_html;
    var vservice_template_id = req.body.vservice_template_id;
    var permissions = req.session.permissions;
    var manage_user_index = 1;
    if (manage_user_index != -1) {
        if (!vservice_template_id || !name || !description) {
            res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
        } else {
            var fileName = '';
            var condition = {
                vservice_template_id: vservice_template_id
            }            
            if (req.files.length > 0) {
                fileName = req.files[0].filename;
                var data = {
                    "name": name,
                    "description": description,
                    "detail": detail,
                    "connection_html": connection_html,
                    "logo": fileName
                }
            } else {
                var data = {
                    "name": name,
                    "description": description,
                    "detail": detail,
                    "connection_html": connection_html
                }
            }
            Customermodel.updateServiceInfo(condition, data, function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success) {
                var response = {
                    resCode: 200,
                    response: data
                }
                res.status(200).send(response);
            })
        }
    } else {
        res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
});
/* Api to get Integrate Service List API
  Created  : 18-05-2017
*/
router.post('/getClientSubscriptions', customodule.authenticate, function(req, res, next) {
    var subscriptionListApiPath = '';
    var clientId = '';
    if(req.userType == 'USER')
    {            
        //client id passed through customer portal
        clientId = req.client_id;            
    }
    else
    {            
        //client id passed through admin portal
        clientId = req.body.client_id;
    }
    if (!clientId) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        subscriptionListApiPath = SETTING.clientSavePath + '/' + clientId + '/subscriptions';
        globalAPI.listGlobalApi(subscriptionListApiPath, function(error) {
            var response = {
                resCode: 201,
                response: error
            }
            res.status(200).send(response);
        }, function(data, response) {
            var resClientSubscriptions = {};
            resClientSubscriptions.resCode = SETTING.succResCode;
            resClientSubscriptions.responseParam = data;
            res.status(200).send(resClientSubscriptions);
        });
    }
})
/* Api to get Integrate Service List API
  Created  : 18-05-2017
*/
router.post('/getAvailableSiteAddon', function(req, res, next) {
    var vserviceTemplateId = req.body.vservice_template_id;
    if (!vserviceTemplateId) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        getAvailableSiteAddonApiPath = SETTING.clientUrl + 'availableAddons' + '/' + vserviceTemplateId;
        globalAPI.listGlobalApi(getAvailableSiteAddonApiPath, function(error) {
            var response = {
                resCode: 201,
                response: error
            }
            res.status(200).send(response);
        }, function(data, response) {
            var resAvailableSiteAddon = {};
            resAvailableSiteAddon.resCode = SETTING.succResCode;
            resAvailableSiteAddon.responseParam = data;
            resAvailableSiteAddon.url = SETTING.pathLive;
            res.status(200).send(resAvailableSiteAddon);
        });
    }
})
router.post('/addNewService', customodule.authenticate, function(req, res, next) {
    var userId = req.userId;
    var userType = req.userType
    var precessedSiteObj = {};
    var client_id = req.body.configurationDetail.client_id;
    var plan_id = req.body.configurationDetail.plan_id;
    req.body.configurationDetail.id = userId;
    if (!client_id || !plan_id) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        //send the request parameter through api call
        var args = {
            data: req.body.configurationDetail,
            headers: {
                "Content-Type": "application/json",
                "id": req.userId,
                "userIdType": req.userType
            },
            requestConfig: {
                timeout: 10000, //request timeout in milliseconds
                noDelay: true, //Enable/disable the Nagle algorithm
                keepAlive: true, //Enable/disable keep-alive functionalityidle socket.
                keepAliveDelay: 1000 //and optionally set the initial delay before the first keepalive probe is sent
            },
            responseConfig: {
                timeout: 10000 //response timeout
            }
        };
        var siteUrl = SETTING.serviceSavePath;
        globalAPI.saveGlobalApi(siteUrl, args, res, function(error) {
            customodule.helper.winstonContext(req, res).info("Getting error on calling global api");
        }, function(data, response) {
            if (data.hasOwnProperty('plan_subscription_id') && data.plan_subscription_id != 0) {
                var planSubscriptionId = data.plan_subscription_id;
                var response = {
                    resCode: SETTING.succResCode,
                    planSubscriptionId: planSubscriptionId,
                    response: 'A new service has been added successfully.'
                }
                res.status(200).send(response);
                saveSiteDetails(req, res, planSubscriptionId);
            } else {
                var response = {
                    resCode: 201,
                    response: 'Error occured while save data !!'
                }
                res.status(200).send(response);
            }
        });
    }
});

router.post('/addNewService1', customodule.authenticate, function(req, res, next) {
    var precessedSiteObj = {};
    var client_id = req.body.configurationDetail.client_id;
    var plan_id = req.body.configurationDetail.plan_id;
    if (!client_id || !plan_id) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        //send the request parameter through api call
        var args = {
            data: req.body.configurationDetail,
            headers: {
                "Content-Type": "application/json"
            },
            requestConfig: {
                timeout: 10000, //request timeout in milliseconds
                noDelay: true, //Enable/disable the Nagle algorithm
                keepAlive: true, //Enable/disable keep-alive functionalityidle socket.
                keepAliveDelay: 1000 //and optionally set the initial delay before the first keepalive probe is sent
            },
            responseConfig: {
                timeout: 10000 //response timeout
            }
        };
        var siteUrl = SETTING.serviceSavePath;
        globalAPI.saveGlobalApi(siteUrl, args, res, function(error) {
            customodule.helper.winstonContext(req, res).info("Getting error on calling global api");
        }, function(data, response) {
            if (data.hasOwnProperty('plan_subscription_id') && data.plan_subscription_id != 0) {
                var planSubscriptionId = data.plan_subscription_id;
                setTimeout(function() {
                    serviceDetailPath = SETTING.serviceSavePath + '/' + planSubscriptionId;
                    globalAPI.listGlobalApi(serviceDetailPath, function(error) {
                        var response = {
                            resCode: 201,
                            response: error
                        }
                        res.status(200).send(response);
                    }, function(data, response) {
                        data.resCode = response.statusCode;
                        if (data.hasOwnProperty('service_id') && data.service_id != 0) {
                            var serviceId = data.service_id;
                            //now check wheather any site detail provided or not
                            if (req.body.hasOwnProperty('siteDetail') && req.body.siteDetail.length > 0) {
                                var siteDetails = req.body.siteDetail;
                                var siteLen = req.body.siteDetail.length;
                                node_module.asyncForEach(siteDetails, function(item, index, arr) {
                                    //now check wheather the particular site and slot id is present or not
                                    var siteId = item.siteId;
                                    var slotId = item.slotId;
                                    requestParam = {};
                                    requestParam.site_id = siteId;
                                     var args = {
                                         data: requestParam,
                                         headers: {
                                             "Content-Type": "application/json"
                                         },
                                         requestConfig: {
                                             timeout: 10000, //request timeout in milliseconds
                                             noDelay: true, //Enable/disable the Nagle algorithm
                                             keepAlive: true, //Enable/disable keep-alive functionalityidle socket.
                                             keepAliveDelay: 1000 //and optionally set the initial delay before the first keepalive probe is sent
                                         },
                                         responseConfig: {
                                             timeout: 10000 //response timeout
                                         }
                                     };
                                     serviceDetailPath = SETTING.serviceSiteSavePath + '/' + serviceId + '/' + 'site' + '/' + slotId;
                                     globalAPI.saveGlobalApi(serviceDetailPath, args, res, function(error) {
                                         customodule.helper.winstonContext(req, res).info("Getting error on calling global api");
                                     }, function(data, response) {
                                         if (data.status == 'ASSIGNED') {
                                             precessedSiteObj[index] = siteId;
                                             var objlen = Object.keys(precessedSiteObj).length;
                                             if (objlen == siteLen) {
                                                 var response = {
                                                     resCode: SETTING.succResCode,
                                                     response: 'A new service has been added successfully.'
                                                 }
                                                 res.status(200).send(response);
                                             }
                                         } else {
                                             var response = {
                                                 resCode: 201,
                                                 response: 'Error occured while assigne site id.'
                                             }
                                             res.status(200).send(response);
                                         }
                                     });
                                 });
                             } else {
                                 var response = {
                                     resCode: 200,
                                     response: data
                                 }
                                 res.status(200).send(response);
                             }
                         } else {
                             var response = {
                                 resCode: 201,
                                 response: 'Unable to get service id!!'
                             }
                             res.status(200).send(response);
                         }
                     });
                 }, 3000);
             } else {
                 var response = {
                     resCode: 201,
                     response: 'Error occured while save data !!'
                 }
                 res.status(200).send(response);
             }
         });
     }
 });


/* function to call recursively
   Created  : 09-06-2017
*/
var errorCntArr = [];
function saveSiteDetails(req, res, planSubscriptionId) {
    Customermodel.saveSiteDetailsInfo(req, planSubscriptionId, function(err) {
        customodule.helper.winstonContext(req, res).info("Getting error on saving site details");
    }, function(serviceInfoData) {
        if (serviceInfoData.resCode == 201) {
            errorCntArr.push(serviceInfoData);
            if (errorCntArr.length < 20) {
                setTimeout( function(){
                	saveSiteDetails(req, res, planSubscriptionId)
		}, 1000);
	    } else {
            customodule.helper.winstonContext(req, res).info("Service Site assignation expired because Service wasn't created.");
        }
        } else {
            customodule.helper.winstonContext(req, res).info("A new service has been added successfully.");
        }
    })
}
/* Api to get service id
  Created  : 19-05-2017
*/
router.post('/getNewServiceDetail', customodule.authenticate, function(req, res, next) {
    var serviceDetailPath = '';
    var plan_subscription_id = req.body.plan_subscription_id;
    if (!plan_subscription_id) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        serviceDetailPath = SETTING.serviceSavePath + '/' + plan_subscription_id;
        globalAPI.listGlobalApi(serviceDetailPath, function(error) {
            customodule.helper.winstonContext(req, res).info("Getting error on calling global api");
        }, function(data, response) {
            data.resCode = response.statusCode;
            res.status(200).send(data);
        });
    }
})
/* Api to assign site to service
    Created  : 19-05-2017
*/
router.post('/addSiteToService', customodule.authenticate, function(req, res, next) {
    var serviceSavePath = '';
    var precessedSiteObj = {};
    var serviceId = req.body.service_id;
    var userId = req.userId;
    var userType = req.userType;
    if (!serviceId) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        //check the site details provided or not
        if (req.body.hasOwnProperty('siteDetail') && req.body.siteDetail.length > 0) {
            var siteDetails = req.body.siteDetail;
            var siteLen = req.body.siteDetail.length;
            node_module.asyncForEach(siteDetails, function(item, index, arr) {
                //now check wheather the particular site and slot id is present or not
                var siteId = item.siteId;
                var slotId = item.slotId;
                var condiction = "ss.service_id = " + serviceId + " AND ss.slot = " + slotId + " ";
                var select = "ss.service_site_id ";
                var join = "";
                var param = {
                    condiction: condiction,
                    join: join,
                    select: select
                }
                Customermodel.getSelectedSiteFullInfoByCondiction(param, function(err) {
                    var response = {
                        resCode: 201,
                        response: err
                    }
                    res.status(200).send(response);
                }, function(serviceInfoData) {
                    if (serviceInfoData.length > 0) {
                        //delete the slot and add
                        var deleteArgs = {
                            headers: {
                                "Content-Type": "application/json",
                                "id": req.userId,
                                "userIdType": req.userType
                            }
                        };
                        siteDeletePath = SETTING.serviceSiteSavePath + '/' + serviceId + '/' + 'site' + '/' + slotId;
                        globalAPI.deleteGlobalApi(siteDeletePath, deleteArgs, function(error) {
                            var response = {
                                resCode: 201,
                                response: err
                            }
                            res.status(200).send(response);
                        }, function(data, response) {
                            if (data.status == 'UNASSIGNED') {
                                requestParam = {};
                                if (siteId != null) {
                                    requestParam.id = userId;
                                    requestParam.site_id = siteId;
                                    var args = {
                                        data: requestParam,
                                        headers: {
                                            "Content-Type": "application/json",
                                            "id": req.userId,
                                            "userIdType": req.userType
                                        },
                                        requestConfig: {
                                            timeout: 10000, //request timeout in milliseconds
                                            noDelay: true, //Enable/disable the Nagle algorithm
                                            keepAlive: true, //Enable/disable keep-alive functionalityidle socket.
                                            keepAliveDelay: 1000 //and optionally set the initial delay before the first keepalive probe is sent
                                        },
                                        responseConfig: {
                                            timeout: 10000 //response timeout
                                        }
                                    };
                                    serviceDetailPath = SETTING.serviceSiteSavePath + '/' + serviceId + '/' + 'site' + '/' + slotId;
                                    globalAPI.saveGlobalApi(serviceDetailPath, args, res, function(error) {
                                        customodule.helper.winstonContext(req, res).info("Getting error on calling global api.");
                                    }, function(data, response) {
                                        if (data.status == 'ASSIGNED') {
                                            precessedSiteObj[index] = siteId;
                                            var objlen = Object.keys(precessedSiteObj).length;
                                            if (objlen == siteLen) {
                                                var response = {
                                                    resCode: SETTING.succResCode,
                                                    response: 'Site details has been updated successfully.'
                                                }
                                                res.status(200).send(response);
                                            }
                                        } else {
                                            var response = {
                                                resCode: 201,
                                                response: 'Error occured while assign site id.'
                                            }
                                            res.status(200).send(response);
                                        }
                                    });
                                } else {
                                    precessedSiteObj[index] = siteId;
                                    var objlen = Object.keys(precessedSiteObj).length;
                                    if (objlen == siteLen) {
                                        var response = {
                                            resCode: SETTING.succResCode,
                                            response: 'Site details has been updated successfully.'
                                        }
                                        res.status(200).send(response);
                                    }
                                }
                            } else {
                                var response = {
                                    resCode: 201,
                                    response: 'Error occured while unassign site id.'
                                }
                            }
                        });
                    } else {
                        //add the slot
                        requestParam = {};
                        if (siteId != null) {
                            requestParam.site_id = siteId;
                            requestParam.id = userId;
                            requestParam.userIdType = req.userType;
                            var args = {
                                data: requestParam,
                                headers: {
                                    "Content-Type": "application/json"
                                },
                                requestConfig: {
                                    timeout: 10000, //request timeout in milliseconds
                                    noDelay: true, //Enable/disable the Nagle algorithm
                                    keepAlive: true, //Enable/disable keep-alive functionalityidle socket.
                                    keepAliveDelay: 1000 //and optionally set the initial delay before the first keepalive probe is sent
                                },
                                responseConfig: {
                                    timeout: 10000 //response timeout
                                }

                            };
                            serviceDetailPath = SETTING.serviceSiteSavePath + '/' + serviceId + '/' + 'site' + '/' + slotId;
                            globalAPI.saveGlobalApi(serviceDetailPath, args, res, function(error) {
                                customodule.helper.winstonContext(req, res).info("Getting error on calling global api.");
                            }, function(data, response) {
                                if (data.status == 'ASSIGNED') {
                                    precessedSiteObj[index] = siteId;
                                    var objlen = Object.keys(precessedSiteObj).length;
                                    if (objlen == siteLen) {
                                        var response = {
                                            resCode: SETTING.succResCode,
                                            response: 'Site details has been updated successfully.'
                                        }
                                        res.status(200).send(response);
                                    }
                                } else {
                                    var response = {
                                        resCode: 201,
                                        response: 'Error occured while assign site id.'
                                    }
                                    res.status(200).send(response);
                                }
                            });
                        } else {
                            precessedSiteObj[index] = siteId;
                            var objlen = Object.keys(precessedSiteObj).length;
                            if (objlen == siteLen) {
                                var response = {
                                    resCode: SETTING.succResCode,
                                    response: 'Site details has been updated successfully.'
                                }
                                res.status(200).send(response);
                            }
                        }
                    }
                })
            });
        } else {
            var response = {
                resCode: 201,
                response: 'No site details provided.'
            }
            res.status(200).send(response);
        }
    }
});
/* Api to delete site from service
  Created  : 19-05-2017
*/
router.post('/deleteSiteFromService', customodule.authenticate, function(req, res, next) {
    var serviceSavePath = '';
    var service_id = req.body.service_id;
    var slot_id = req.body.slot_id;
    if (!service_id || !slot_id) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        //send the request parameter through api call
        requestParam = {};
        var deleteArgs = {
            data: requestParam,
            headers: {
                "Content-Type": "application/json",
                "id": req.userId,
                "userIdType": req.userType
            }
        };
        serviceDetailPath = SETTING.serviceSiteSavePath + '/' + service_id + '/' + 'site' + '/' + slot_id;
        globalAPI.deleteGlobalApi(serviceDetailPath, deleteArgs, function(error) {
            customodule.helper.winstonContext(req, res).info("Getting error on calling global api.");
        }, function(data, response) {
            res.status(200).send(data);
        });
    }
});
/* Api to Get connection type list
  Created  : 22-05-2017
*/
router.get('/getConnectionParameterDetails', customodule.authenticate, function(req, res, next) {
        var connectionTypeId = req.query.connectionType;
        var condiction = '';
        if (typeof connectionTypeId != 'undefined' && connectionTypeId != '' && connectionTypeId != null) {
            condiction = "ctp.connection_type_id ='" + connectionTypeId + "'";
        } else {
            condiction = "ctp.connection_type_id != 0";
        }
        var select = "ctp.connection_type_parameter_id as connection_type_parameter_id,ctp.name as name,ctp.required as required,ctp.default_value as default_value,ctp.parameter_type as parameter_type,ctp.enum_values as enum_values";
        var join = "";
        var param = {
            condiction: condiction,
            join: join,
            select: select
        }
        Customermodel.getConnectionTypeParameterFullInfoByCondiction(param, function(err) {
            customodule.helper.winstonContext(req, res).info("Getting error on fetching connection type");
        }, function(userdata) {
            var userDetails = [];
            if (typeof userdata != 'undefined' && userdata != '' && userdata != null) {
                for (var count = 0; count < userdata.length; count++) {
                    userDetails.push({
                        "connection_type_parameter_id": userdata[count].connection_type_parameter_id,
                        "required": userdata[count].required,
                        "name": userdata[count].name,
                        "default_value": userdata[count].default_value,
                        "parameter_type": userdata[count].parameter_type
                    })
                    if (userdata[count].parameter_type == 'ENUM') {
                        userDetails[count].enumValueArray = userdata[count].enum_values.split(",")
                    } else {
                        userDetails[count].enumValueArray = [];
                    }
                }
                var response = {
                    resCode: 200,
                    response: userDetails
                }
            } else {
                var response = {
                    resCode: 200,
                    response: []
                }
            }
            res.status(200).send(response);
        });
    })
/* Api to edit site
    Created  : 22-05-2017
*/
router.post('/editServiceConfiguration/service/:planSubscriptionId/configuration', customodule.authenticate, function(req, res, next) {
    var userId = req.userId;
    var userType = req.userType;
    req.body.id = userId;
    var planSubscriptionId = req.params.planSubscriptionId;
    if (!planSubscriptionId) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        //send the request parameter through api call
        var args = {
            data: req.body,
            headers: {
                "Content-Type": "application/json",
                "id": req.userId,
                "userIdType": req.userType
            }
        };
        var configurationUrl = SETTING.planSubscriptionConfigEditPath + planSubscriptionId + '/configuration';
        globalAPI.editGlobalApi(configurationUrl, args, function(error) {
            customodule.helper.winstonContext(req, res).info("Getting error on calling global api.");
        }, function(data, response) {
            data.statusCode = response.statusCode;
            res.status(200).send(data);
        });
    }
})
/* Api to get all client Billing Info
    Created  : 22-05-2017
*/
router.post('/getClientBillingInfo',function(req, res, next) {
    var clientListApiPath = '';
    var clientId = '';
    if(req.userType == 'USER')
    {
        clientId = req.client_id;            
    }
    else
    {
        clientId = req.body.client_id;
    }    
    if (!clientId) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        clientListApiPath = SETTING.clientSavePath + '/' + clientId + '/' + 'billingInfo';
        globalAPI.listGlobalApi(clientListApiPath, function(error) {
            customodule.helper.winstonContext(req, res).info("Getting error on calling global api.");
        }, function(data, response) {            
            if(typeof data.error == "undefined" || data.error == "" || data.error == null)
            {
                data.resCode = 200;
                res.status(200).send(data);
            }
            else{
                data.resCode = 201;
                res.status(200).send(data); 
            }
        });
    }
});
/* Api to get all client Billing Info
  Created  : 24-05-2017
*/
router.post('/updateClientBillingInfo/:clientId',function(req, res, next) {
    var clientId = req.params.clientId;       
    var name = req.body.name;
    if (!name) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        var args = {
            data: req.body,
            headers: {
                "Content-Type": "application/json"                
            }
        };
        clientBillingUpdateApiPath = SETTING.clientSavePath + '/' + clientId + '/' + 'billingInfo';
        globalAPI.editGlobalApi(clientBillingUpdateApiPath, args, function(error) {
            customodule.helper.winstonContext(req, res).info("Getting error on calling global api.");
        }, function(data, response) {               
            if(typeof data.error == "undefined" || data.error == "" || data.error == null)
            {
                data.resCode = 200;
                res.status(200).send(data);
            }
            else{
                data.resCode = 201;
                res.status(200).send(data); 
            }
        });
    }
})
/* Api to get all the invoices for a client
    Created  : 23-05-2017
*/
router.post('/getClientInvoiceList', customodule.authenticate, function(req, res, next) {
    var clientListApiPath = '';
    var clientId = '';  
    if(req.userType == 'USER')
    {
        clientId = req.client_id;            
    }
    else
    {
        clientId = req.body.client_id;
    }
    if (!clientId) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        clientInvoiceApiPath = SETTING.clientSavePath + '/' + clientId + '/' + 'invoices';
        globalAPI.listGlobalApi(clientInvoiceApiPath, function(error) {
            customodule.helper.winstonContext(req, res).info("Getting error on calling global api.");
        }, function(data, response) {
            data.resCode = 200;
            res.status(200).send(data);
        });
    }
});
/* Api to get API for Site Details
  Created  : 23-05-2017
*/
router.get('/getSiteDetails', customodule.authenticate, function(req, res, next) {
    var clientListApiPath = '';
    var siteId = req.query.site_id;
    if (!siteId) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        siteDetailApiPath = SETTING.siteSavePath + siteId;
        globalAPI.listGlobalApi(siteDetailApiPath, function(error) {
            customodule.helper.winstonContext(req, res).info("Getting error on calling global api.");
        }, function(data, response) {
            data.resCode = 200;
            res.status(200).send(data);
        });
    }
});
/* Api to get API for Site Details
  Created  : 23-05-2017
*/
router.post('/getPlanSubscriptionDetails', customodule.authenticate, function(req, res, next) {
    var planSubscriptionId = req.body.plan_subscription_id;
    var vserviceId = req.body.vservice_id;
    var requestType = req.body.requestType;

    if (!planSubscriptionId || !requestType || !vserviceId) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        if (requestType == "configuration") {
            //get plan service information
            var condiction = "ps.plan_subscription_id =" + planSubscriptionId;
            var select = " ps.name,ps.description ";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getSubscriptionDetailInfoByCondition(param, function(err) {
                customodule.helper.winstonContext(req, res).info("Getting error on fetching subscription details");
            }, function(planServiceInfoData) {
                if (planServiceInfoData.length > 0) {
                    var response = {
                            resCode: 200,
                            planServiceInfoData: planServiceInfoData[0]
                        }
                    //get plan service configuration
                    var condiction = "psa.plan_subscription_id =" + planSubscriptionId;
                    var select = " psa.name,psa.value ";
                    var join = "";
                    var param = {
                        condiction: condiction,
                        join: join,
                        select: select
                    }
                    Customermodel.getPlanSubscriptionDetailInfoByCondition(param, function(err) {
                        customodule.helper.winstonContext(req, res).info("Getting error on fetching plan subscription details");
                    }, function(planServiceConfigData) {
                        response.planServiceInfoData.plan_arguments = {};
                        if (planServiceConfigData.length > 0) {
                            var planServiceConfigLen = planServiceConfigData.length;
                            for (i = 0; i < planServiceConfigData.length; i++) {
                                var key = planServiceConfigData[i].name;
                                response.planServiceInfoData.plan_arguments[planServiceConfigData[i].name] = planServiceConfigData[i].value;
                                if (i == planServiceConfigLen - 1) {
                                    res.status(200).send(response);
                                }
                            }
                        } else {
                            res.status(200).send(response);
                        }
                    })
                } else {
                    var response = {
                        resCode: 201,
                        response: "Sorry no record found."
                    }
                    res.status(200).send(response);
                }
            })
        } else if (requestType == "site") {
            //get plan service information
            var condiction = "ps.plan_subscription_id =" + planSubscriptionId;
            var select = " ps.client_id,ps.service_id,ps.plan_id ";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getSubscriptionDetailInfoByCondition(param, function(err) {
                customodule.helper.winstonContext(req, res).info("Getting error on fetching subscription details");
            }, function(planServiceInfoRes) {
                if (planServiceInfoRes.length > 0) {
                    //get site list
                    var clientId = planServiceInfoRes[0]['client_id'];
                    if (!clientId) {
                        //res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
                    } else {
                        if (typeof clientId != 'undefined' && clientId != '' && clientId != null) {
                            var condiction = "st.client_id ='" + clientId + "'";
                        } else {
                            var condiction = "st.client_id != 0";
                        }


                        var select = "st.site_id as site_id,st.client_id as client_id,st.status as status,st.connection_type_id as connection_type_id,st.name as site_name,ct.name as connection_type_name";
                        var join = "LEFT JOIN site as st ON st.connection_type_id = ct.connection_type_id";
                        var param = {
                            condiction: condiction,
                            join: join,
                            select: select
                        }
                        Customermodel.getSiteFullInfoByCondiction(param, function(err) {
                            customodule.helper.winstonContext(req, res).info("Getting error on fetching site information");
                        }, function(userdata) {
                            if (userdata.length > 0) {
                                //get the plan detail
                                var response = {
                                    resCode: 200,
                                    response: userdata
                                }
                                var condiction = "plan.plan_id = " + planServiceInfoRes[0]['plan_id'];
                                var select = "vt.min_sites as min_sites,vt.max_sites as max_sites";
                                var join = "LEFT JOIN plan as plan ON vt.vservice_template_id = plan.vservice_template_id";
                                var param = {
                                    condiction: condiction,
                                    join: join,
                                    select: select
                                }
                                response.siteParam = {};
                                Customermodel.getPlanFullInfoByCondiction(param, function(err) {
                                    customodule.helper.winstonContext(req, res).info("Getting error on fetching plan details.");
                                }, function(planinfo) {
                                    if (planinfo.length > 0) {
                                        //get the selected sites
                                        response.siteParam = planinfo[0];
                                        response.selectedSiteParam = {};
                                        if (planServiceInfoRes[0]['service_id'] != '' && planServiceInfoRes[0]['service_id'] != 0 && planServiceInfoRes[0]['service_id'] != null) {
                                            var condiction = "ss.service_id = " + planServiceInfoRes[0]['service_id'];
                                            var select = "ss.slot as slot,ss.site_id as site_id";
                                            var join = "";
                                            var param = {
                                                condiction: condiction,
                                                join: join,
                                                select: select
                                            }

                                            Customermodel.getSelectedSiteFullInfoByCondiction(param, function(err) {
                                                customodule.helper.winstonContext(req, res).info("Getting error on fetching selected site");
                                            }, function(selectedSiteInfo) {
                                                if (selectedSiteInfo.length > 0) {
                                                    response.selectedSiteParam = selectedSiteInfo;
                                                    res.status(200).send(response);
                                                } else {
                                                    res.status(200).send(response);
                                                }
                                            });
                                        } else {
                                            res.status(200).send(response);
                                        }
                                    } else {
                                        res.status(200).send(response);
                                    }
                                });
                            } else {
                                var response = {
                                    resCode: 201,
                                    response: 'No record found !!'
                                }
                                res.status(200).send(response);
                            }
                        });
                    }
                } else {
                    var response = {
                        resCode: 201,
                        response: 'No record found !!'
                    }
                    res.status(200).send(response);
                }
            });
        } else {
            var response = {
                resCode: 201,
                response: "Invalid request !!"
            }
            res.status(200).send(response);
        }
    }
});
/* Billing. Integrate with API
  Created  : 25-05-2017
*/
router.post('/getClientBillingDetails', customodule.authenticate, function(req, res, next) {
    var clientListApiPath = '';
    var clientId = '';  
    if(req.userType == 'USER')
    {
        clientId = req.client_id;            
    }
    else
    {
        clientId = req.body.client_id;
    }
    if (!clientId) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        clientBillingDetailApiPath = SETTING.clientSavePath + '/' + clientId;
        globalAPI.listGlobalApi(clientBillingDetailApiPath, function(error) {
            customodule.helper.winstonContext(req, res).info("Getting error on calling global api.");
        }, function(data, response) {
            data.resCode = 200;
            res.status(200).send(data);
        });
    }
});
/* Service Details -> Unsubscribe
  Created  : 29-05-2017
*/
router.post('/unsubscribeServices', customodule.authenticate, function(req, res, next) {
    var clientListApiPath = '';
    var planSubscriptionId = req.body.plan_subscription_id;
    if (!planSubscriptionId) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        var deleteArgs = {
            headers: {
                "Content-Type": "application/json",
                "id": req.userId,
                "userIdType": req.userType
            }
        };
        unsubscribeServicesApiPath = SETTING.serviceSavePath + '/' + planSubscriptionId;
        globalAPI.deleteGlobalApi(unsubscribeServicesApiPath, deleteArgs, function(error) {
            customodule.helper.winstonContext(req, res).info("Getting error on calling global api.");
        }, function(data, response) {
            resDetails = {};
            resDetails.resCode = 200;
            resDetails.response = 'DELETED';
            res.status(200).send(resDetails);
        });
    }
});

/* Admin Dashboard Api
  Created  : 01-06-2017
*/
router.get('/getAdminSquareDashboardDetails', function(req, res, next) {
    var dashboardResObj = {};
    dashboardResObj.squeareRes = {};
    // dashboardResObj.lineRes = {};
    // dashboardResObj.radarRes = {};
    const influx = new node_module.Influx.InfluxDB({
        host: SETTING.influxHost,
        database: SETTING.influxDb,
        schema: []
    })
    const influxping = new node_module.Influx.InfluxDB({
        host: SETTING.influxHost,
        database: SETTING.influxDbPing,
        schema: []
    });
    var dashboardConfigObj = {};
    dashboardConfigObj.dasboardObj = {};
    dashboardConfigObj.dasboardObj.vmsup = {
        query: 'select * from vms_up order by time desc limit 1',
        db: 'influxdb'
    };
    dashboardConfigObj.dasboardObj.vmsupUptime = {
        query: 'select sum(upVms) / sum(totalVms) * 100 as availability from vms_up WHERE time > now() - 2d GROUP BY time(1d)',
        db: 'influxdb'
    };
    dashboardConfigObj.dasboardObj.hostsup = {
        query: 'select * from hosts_up order by time desc limit 1',
        db: 'influxdb'
    };
    dashboardConfigObj.dasboardObj.hostsupUptime = {
        query: 'select sum(upHosts) / sum(totalHosts) * 100 as availability from hosts_up WHERE time > now() - 2d GROUP BY time(1d)',
        db: 'influxdb'
    };
    dashboardConfigObj.dasboardObj.servicesup = {
        query: 'select * from services_up order by time desc limit 1',
        db: 'influxdb'
    };
    dashboardConfigObj.dasboardObj.servicesupUptime = {
        query: 'select sum(upVms) / sum(totalVms) * 100 as availability from services_up WHERE time > now() - 2d GROUP BY time(1d)',
        db: 'influxdb'
    };
    dashboardConfigObj.dasboardObj.ping = {
        query: 'select * from ping order by time desc limit 1',
        db: 'telegraf-vim-db'
    };
    node_module.asyncLoop(dashboardConfigObj.dasboardObj, function(item, next) {
        var slectedDb = item.value.db;
        if (slectedDb == 'influxdb') {
            influx.query(item.value.query).then(result => {
                if (item.key == 'vmsup') {
                    dashboardResObj.squeareRes.vmsup = result[0].upVms + '/' + result[0].totalVms;
                    next();
                }
                if (item.key == 'vmsupUptime') {
                    dashboardResObj.squeareRes.vmsupUptime = {};
                    if (result.length > 0) {
                        var totResultLen = result.length - 1;
                        node_module.asyncForEach(result, function(item, index, arr) {
                            if (item.time != null) {
                                result[index].time = ((new Date(item.time)).getTime()) / 1000;
                            }
                            if (index == totResultLen) {
                                var resultArr = sortObjectsArray(result, 'time');
                                if (resultArr.length > 0) {
                                    resultArr = resultArr.reverse();
                                    var istParam = resultArr[0].availability;
                                    var secondParam = istParam - resultArr[1].availability;
                                    dashboardResObj.squeareRes.vmsupUptime.istParam = istParam;
                                    dashboardResObj.squeareRes.vmsupUptime.secondParam = secondParam;
                                    next();
                                }
                            }
                        });
                    } else {
                        dashboardResObj.squeareRes.vmsupUptime = result;
                        next();
                    }
                }
                if (item.key == 'hostsup') {
                    dashboardResObj.squeareRes.hostsup = result[0].upHosts + '/' + result[0].totalHosts;
                    next();
                }
                if (item.key == 'hostsupUptime') {
                    dashboardResObj.squeareRes.hostsupUptime = {};
                    if (result.length > 0) {
                        var totResultLen = result.length - 1;
                        node_module.asyncForEach(result, function(item, index, arr) {
                            if (item.time != null) {
                                result[index].time = ((new Date(item.time)).getTime()) / 1000;
                            }
                            if (index == totResultLen) {
                                var resultArr = sortObjectsArray(result, 'time');
                                if (resultArr.length > 0) {
                                    resultArr = resultArr.reverse();
                                    var istParam = resultArr[0].availability;
                                    var secondParam = istParam - resultArr[1].availability;
                                    dashboardResObj.squeareRes.hostsupUptime.istParam = istParam;
                                    dashboardResObj.squeareRes.hostsupUptime.secondParam = secondParam;
                                    next();
                                }
                            }
                        });
                    } else {
                        dashboardResObj.squeareRes.hostsupUptime = result;
                        next();
                    }
                }
                if (item.key == 'servicesup') {
                    dashboardResObj.squeareRes.servicesup = result[0].upVms + '/' + result[0].totalVms;
                    next();
                }
                if (item.key == 'servicesupUptime') {
                    dashboardResObj.squeareRes.servicesupUptime = {};
                    if (result.length > 0) {
                        var totResultLen = result.length - 1;
                        node_module.asyncForEach(result, function(item, index, arr) {
                            if (item.time != null) {
                                result[index].time = ((new Date(item.time)).getTime()) / 1000;
                            }
                            if (index == totResultLen) {
                                var resultArr = sortObjectsArray(result, 'time');
                                if (resultArr.length > 0) {
                                    resultArr = resultArr.reverse();
                                    var istParam = resultArr[0].availability;
                                    var secondParam = istParam - resultArr[1].availability;
                                    dashboardResObj.squeareRes.servicesupUptime.istParam = istParam;
                                    dashboardResObj.squeareRes.servicesupUptime.secondParam = secondParam;
                                    next();
                                }
                            }
                        });
                    } else {
                        dashboardResObj.squeareRes.servicesupUptime = result;
                        next();
                    }
                }

            }).catch(err => {
                res.status(500).send(err.stack)
            })
        }
        if (slectedDb == 'telegraf-vim-db') {
            influxping.query(item.value.query).then(result => {
                if (item.key == 'ping') {
                    dashboardResObj.squeareRes.delayres = result[0].average_response_ms;
                }
                next();
            }).catch(err => {
                res.status(500).send(err.stack)
            })
        }
    }, function() {
        res.status(200).send(dashboardResObj)
    });
});

function sortObjectsArray(objectsArray, sortKey) {
    // Quick Sort:
    var retVal;
    if (1 < objectsArray.length) {
        var pivotIndex = Math.floor((objectsArray.length - 1) / 2); // middle index
        var pivotItem = objectsArray[pivotIndex]; // value in the middle index
        var less = [],
            more = [];
        objectsArray.splice(pivotIndex, 1); // remove the item in the pivot position
        objectsArray.forEach(function(value, index, array) {
            value[sortKey] <= pivotItem[sortKey] ? // compare the 'sortKey' proiperty
                less.push(value) :
                more.push(value);
        });
        retVal = sortObjectsArray(less, sortKey).concat([pivotItem], sortObjectsArray(more, sortKey));
    } else {
        retVal = objectsArray;
    }
    return retVal;
}
router.get('/getAdminLineDashboardDetails', function(req, res, next) {
    var dashboardResObj = {};
    dashboardResObj.lineRes = {};
    const influx = new node_module.Influx.InfluxDB({
        host: SETTING.influxHost,
        database: SETTING.influxDb,
        schema: []
    })
    const influxping = new node_module.Influx.InfluxDB({
        host: SETTING.influxHost,
        database: SETTING.influxDbPing,
        schema: []
    });
    var dashboardConfigObj = {};
    dashboardConfigObj.dasboardObj = {};
    dashboardConfigObj.dasboardObj.linecpu = {
        query: 'SELECT 100 - mean("cpu_idle") FROM "cpu_global_idleness_telegraf" WHERE time > now() - 1w GROUP BY time(1h)',
        db: 'influxdb'
    };
    dashboardConfigObj.dasboardObj.memUsage = {
        query: 'SELECT sum("used")/5 as used, sum("total")/5 as total FROM "mem_global_usage" WHERE time > now() - 1w GROUP BY time(5m)',
        //query: 'SELECT sum("used") as used, sum("total") as total FROM "mem_global_usage" WHERE time > now() - 1w GROUP BY time(1h)',
        db: 'influxdb'
    };
    dashboardConfigObj.dasboardObj.network = {
        query: 'SELECT mean("tx") AS "TX", mean("rx") AS "RX", mean("speed")*1000000 AS "MAX" FROM "net_global_usage" WHERE time > now() - 1w GROUP BY time(1h)',
        db: 'influxdb'
    };
    dashboardConfigObj.dasboardObj.stoarge = {
        //query: 'SELECT sum("used") as used, sum("total") as total FROM "mem_global_usage" WHERE time > now() - 1w GROUP BY time(1h)',
        query: 'SELECT mean("total") AS "total", mean("used") AS "used" FROM "disk" WHERE "path" =~ /data-vms/ AND time > now() - 1w GROUP BY time(1h)',
        db: 'telegraf-vim-db'
    };
    dashboardConfigObj.dasboardObj.bandwidth = {
        query: 'SELECT mean("gw_rx") AS "TX", mean("gw_tx") AS "TX", mean("gw_speed") AS "MAX" FROM "net_global_usage" WHERE time > now() - 1w GROUP BY time(1h)',
        db: 'influxdb'
    };
    node_module.asyncLoop(dashboardConfigObj.dasboardObj, function(item, next) {
        var slectedDb = item.value.db;
        if (slectedDb == 'influxdb') {
            influx.query(item.value.query).then(result => {
                if (item.key == 'linecpu') {
                    dashboardResObj.lineRes.cpuusage = result;
                    next();
                }
                if (item.key == 'memUsage') {
                    if (result.length > 0) {
                        var totResultLen = result.length - 1;
                        node_module.asyncForEach(result, function(item, index, arr) {
                            if (item.used != null) {
                                result[index].used = node_module.convert(item.used).from('B').to('GB');
                            }
                            if (item.total != null) {
                                result[index].total = node_module.convert(item.total).from('B').to('GB');
                            }
                            if (index == totResultLen) {
                                dashboardResObj.lineRes.memUsage = result;
                                next();
                            }
                        });
                    } else {
                        dashboardResObj.lineRes.memUsage = result;
                        next();
                    }
                }
                if (item.key == 'network') {
                    if (result.length > 0) {
                        var totResultLen = result.length - 1;
                        node_module.asyncForEach(result, function(item, index, arr) {
                            if (item.TX != null) {
                                result[index].TX = node_module.convert(item.TX).from('b').to('Mb');
                            }
                            if (item.RX != null) {
                                result[index].RX = node_module.convert(item.RX).from('b').to('Mb');
                            }
                            if (item.MAX != null) {
                                result[index].MAX = node_module.convert(item.MAX).from('b').to('Mb');
                            }
                            if (index == totResultLen) {
                                dashboardResObj.lineRes.network = result;
                                next();
                            }
                        });
                    } else {
                        dashboardResObj.lineRes.network = result;
                        next();
                    }
                }

                if (item.key == 'bandwidth') {
                    if (result.length > 0) {
                        var totResultLen = result.length - 1;
                        node_module.asyncForEach(result, function(item, index, arr) {
                            if (item.TX != null) {

                                result[index].TX = node_module.convert(parseFloat(item.TX)).from('b').to('Mb');                              
                            }
                            if (item.MAX != null) {
                                result[index].MAX = node_module.convert(parseFloat(item.MAX)).from('b').to('Mb');                                
                            }
                            if (index == totResultLen) {
                                dashboardResObj.lineRes.bandwidth = result;
                                next();
                            }
                        });
                    } else {
                        dashboardResObj.lineRes.bandwidth = result;
                        next();
                    }
                }
            }).catch(err => {
                res.status(500).send(err.stack)
            })
        }
        if (slectedDb == 'telegraf-vim-db') {
            influxping.query(item.value.query).then(result => {
                if (item.key == 'stoarge') {
                    if (result.length > 0) {
                        var totResultLen = result.length - 1;
                        node_module.asyncForEach(result, function(item, index, arr) {
                            if (item.used != null) {
                                result[index].used = node_module.convert(item.used).from('B').to('GB');
                            }
                            if (item.total != null) {
                                result[index].total = node_module.convert(item.total).from('B').to('GB');
                            }
                            if (index == totResultLen) {
                                dashboardResObj.lineRes.stoarge = result;
                                next();
                            }
                        });
                    } else {
                        dashboardResObj.lineRes.stoarge = result;
                        next();
                    }
                }
            }).catch(err => {
                res.status(500).send(err.stack)
            })

        }
    }, function() {
        res.status(200).send(dashboardResObj)
    });
});
router.get('/getAdminRadarDashboardDetails', customodule.authenticate, function(req, res, next) {
    var dashboardResObj = {};
    dashboardResObj.radarRes = {};
    const influx = new node_module.Influx.InfluxDB({
        host: SETTING.influxHost,
        database: SETTING.influxDb,
        schema: []
    })
    const influxping = new node_module.Influx.InfluxDB({
        host: SETTING.influxHost,
        database: SETTING.influxDbPing,
        schema: []
    });
    var dashboardConfigObj = {};
    dashboardConfigObj.dasboardObj = {};
    dashboardConfigObj.dasboardObj.radarCpu = {
        query: 'SELECT 100 - mean("cpu_idle") FROM "cpu_global_idleness_telegraf" WHERE time > now() - 5m',
        db: 'influxdb'
    };
    dashboardConfigObj.dasboardObj.radarMemory = {
        query: 'SELECT mean("used")*100/ mean("total") FROM "mem_global_usage" WHERE time > now() - 5m',
        db: 'influxdb'
    };
    dashboardConfigObj.dasboardObj.radarStorage = {
        query: 'SELECT mean(used)*100/mean(total) AS "used" FROM "disk" WHERE "path" =~ /data-vms/ AND time > now() - 5m',
        db: 'influxdb'
    };
    dashboardConfigObj.dasboardObj.networkRx = {
        query: 'SELECT mean("rx")/(mean(speed)*10000) FROM "net_global_usage" WHERE time > now() - 5m',
        db: 'influxdb'
    };
    dashboardConfigObj.dasboardObj.networkTx = {
        query: 'SELECT mean("tx")/(mean("speed")*10000) FROM "net_global_usage" WHERE time > now() - 5m',
        db: 'influxdb'
    };
    dashboardConfigObj.dasboardObj.bandwidthRx = {
        query: 'SELECT mean("gw_rx")*100/mean("gw_speed") FROM "net_global_usage" WHERE time > now() - 5m',
        db: 'influxdb'
    };
    dashboardConfigObj.dasboardObj.bandwidthTx = {
        query: 'SELECT mean("gw_tx")*100/mean("gw_speed") FROM "net_global_usage" WHERE time > now() - 5m',
        db: 'influxdb'
    };
    node_module.asyncLoop(dashboardConfigObj.dasboardObj, function(item, next) {
        var slectedDb = item.value.db;
        if (slectedDb == 'influxdb') {
            influx.query(item.value.query).then(result => {
                if (item.key == 'radarCpu') {
                    dashboardResObj.radarRes.radarCpu = result;
                }
                if (item.key == 'radarMemory') {
                    dashboardResObj.radarRes.radarMemory = result;
                }
                if (item.key == 'radarStorage') {
                    dashboardResObj.radarRes.radarStorage = result;
                }
                if (item.key == 'networkRx') {
                    dashboardResObj.radarRes.networkRx = result;
                }
                if (item.key == 'networkTx') {
                    dashboardResObj.radarRes.networkTx = result;
                }
                if (item.key == 'bandwidthRx') {
                    dashboardResObj.radarRes.bandwidthRx = result;
                }
                if (item.key == 'bandwidthTx') {
                    dashboardResObj.radarRes.bandwidthTx = result;
                }
                next();
            }).catch(err => {
                res.status(500).send(err.stack)
            })
        }
        if (slectedDb == 'telegraf-vim-db') {
            influxping.query(item.value.query).then(result => {
                next();
            }).catch(err => {
                res.status(500).send(err.stack)
            })
        }
    }, function() {
        res.status(200).send(dashboardResObj)
    });
});
//get host list
router.get('/getHostList', function(req, res, next) {
        var select = " host_id,hostname,virt_host_id,hostname ";
        var join = "";
        var condiction = " h.host_id != 0";
        var param = {
            condiction: condiction,
            join: join,
            select: select
        }
        Customermodel.getHostFullInfoByCondition(param, function(err) {
            customodule.helper.winstonContext(req, res).info("Getting error on fetching host information.");
        }, function(hostInfoData) {
            var response = {
                resCode: 200,
                hostInfoData: hostInfoData
            }
            res.status(200).send(response);
        })
    })
    /* Host Dashboard Api
      Created  : 06-06-2017
    */
    /* Host  Dashboard Api
      Created  : 06-06-2017
    */
router.get('/getHostSquareDashboardDetails', function(req, res, next) {
    var host = req.query.host;
    if (host != undefined && host != '') {
        var dashboardResObj = {};
        dashboardResObj.squeareRes = {};
        const telegrafVimDb = new node_module.Influx.InfluxDB({
            host: SETTING.influxHost,
            database: SETTING.telegrafVimDb,
            schema: []
        })
        const collectdDb = new node_module.Influx.InfluxDB({
            host: SETTING.influxHost,
            database: SETTING.collectdDb,
            schema: []
        });
        var dashboardConfigObj = {};
        dashboardConfigObj.dasboardObj = {};
        dashboardConfigObj.dasboardObj.hostsup = {
            query: "SELECT last(uptime) AS value FROM system WHERE host =  '" + host + "' limit 1",
            db: 'telegraf-vim'
        };
        dashboardConfigObj.dasboardObj.activevm = {
            query: "SELECT last(value) FROM statsd_value WHERE host = '" + host + "' AND type_instance = 'hosts.vms.active' ",
            db: 'collectd_db'
        };
        dashboardConfigObj.dasboardObj.totalvm = {
            query: "SELECT last(value) FROM statsd_value WHERE host = '" + host + "' AND type_instance = 'hosts.vms.total' ",
            db: 'collectd_db'
        };
        dashboardConfigObj.dasboardObj.internetett = {
            query: "SELECT mean(average_response_ms) FROM ping WHERE host = '" + host + "' AND time > now() - 5m GROUP BY time(1m)",
            db: 'telegraf-vim'
        };
        node_module.asyncLoop(dashboardConfigObj.dasboardObj, function(item, next) {
            var slectedDb = item.value.db;
            if (slectedDb == 'collectd_db') {
                collectdDb.query(item.value.query).then(result => {
                    if (item.key == 'activevm') {
                        dashboardResObj.squeareRes.vmsup = result;
                    }
                    if (item.key == 'totalvm') {
                        dashboardResObj.squeareRes.servicesup = result;
                    }
                    next();
                }).catch(err => {
                    res.status(500).send(err.stack)
                })
            }
            if (slectedDb == 'telegraf-vim') {
                telegrafVimDb.query(item.value.query).then(result => {
                    if (item.key == 'hostsup') {
                        if (result.length > 0) {
                            var totResultLen = result.length - 1;
                            node_module.asyncForEach(result, function(item, index, arr) {
                                if (item.value != null) {
                                    var miliSecondVal = item.value * 1000;
                                    result[index].value = node_module.humanizeDuration(miliSecondVal, {
                                        units: ['d', 'h'],
                                        round: true
                                    })
                                }
                                if (index == totResultLen) {
                                    dashboardResObj.squeareRes.hostsup = result;
                                    next();
                                }
                            });
                        } else {
                            dashboardResObj.squeareRes.hostsup = result;
                            next();
                        }
                    }
                    if (item.key == 'internetett') {
                        dashboardResObj.squeareRes.delayres = result;
                        next();
                    }

                }).catch(err => {
                    res.status(500).send(err.stack)
                })
            }
        }, function() {
            res.status(200).send(dashboardResObj)
        });
    } else {
        var response = {
            resCode: 201,
            response: 'Please provide host.'
        }
        res.status(200).send(response);
    }
});
/* Line Host  Dashboard Api
  Created  : 06-06-2017
*/


router.get('/getHostLineDashboardDetails', function(req, res, next) {
    var host = req.query.host;
    if (host != undefined && host != '') {
        var dashboardResObj = {};
        dashboardResObj.lineRes = {};
        const telegrafVimDb = new node_module.Influx.InfluxDB({
            host: SETTING.influxHost,
            database: SETTING.telegrafVimDb,
            schema: []
        })
        const collectdDb = new node_module.Influx.InfluxDB({
            host: SETTING.influxHost,
            database: SETTING.collectdDb,
            schema: []
        });
        const vcpeDb = new node_module.Influx.InfluxDB({
            host: SETTING.influxHost,
            database: SETTING.vcpeDb,
            schema: []
        });
        var dashboardConfigObj = {};
        dashboardConfigObj.dasboardObj = {};
        dashboardConfigObj.dasboardObj.linecpu = {
            query: "SELECT mean(usage_idle) as idle, mean(usage_user) as user1, mean(usage_system) as system, mean(usage_softirq) as softirq, mean(usage_steal) as steal, mean(usage_nice) as nice, mean(usage_irq) as irq, mean(usage_iowait) as iowait, mean(usage_guest) as guest, mean(usage_guest_nice) as guest_nice FROM cpu WHERE host ='" + host + "' and cpu = 'cpu-total' AND time > now() - 1w GROUP BY time(1h)",
            db: 'telegraf-vim'
        };
        dashboardConfigObj.dasboardObj.memUsage = {
            query: "SELECT mean(total) as total, mean(used) as used, mean(cached) as cached, mean(free) as free, mean(buffered) as buffered FROM mem WHERE host ='" + host + "' AND time > now() - 1w GROUP BY time(1h)",
            db: 'telegraf-vim'
        };
        dashboardConfigObj.dasboardObj.stoarge = {
            query: "SELECT mean(free) AS free FROM disk WHERE host ='" + host + "' AND path !~ /glusterSD/ AND time > now() - 1w GROUP BY time(1h) ",
            db: 'telegraf-vim'
        };
        dashboardConfigObj.dasboardObj.network = {      
            query: "SELECT non_negative_derivative(mean(bytes_recv),1s) as inval, non_negative_derivative(mean(bytes_sent),1s) as out FROM net WHERE host = '" + host + "' AND interface =~ /(bond0$|enp6s0$)/ AND time > now() - 1w GROUP BY time(1h), *",
            db: 'telegraf-vim'
        };
        dashboardConfigObj.dasboardObj.activevm = {
            query: "SELECT last(value) FROM statsd_value WHERE host ='" + host + "' AND type_instance = 'hosts.vms.active' AND time > now() - 1w GROUP BY time(1h)",
            db: 'collectd_db'
        };
        dashboardConfigObj.dasboardObj.networkRx = {
            query: "SELECT mean(rx)/(mean(speed)*10000) FROM net_global_usage WHERE host = '" + host + "' AND time > now() - 5m",
            db: 'vcpe'
        };
        dashboardConfigObj.dasboardObj.networkTx = {
            query: "SELECT mean(tx)/(mean(speed)*10000) FROM net_global_usage WHERE host = '" + host + "' AND time > now() - 5m",
            db: 'vcpe'
        };
        node_module.asyncLoop(dashboardConfigObj.dasboardObj, function(item, next) {
            var slectedDb = item.value.db;
            if (slectedDb == 'telegraf-vim') {
                telegrafVimDb.query(item.value.query).then(result => {
                    if (item.key == 'linecpu') {
                        dashboardResObj.lineRes.cpuusage = result;
                        next();
                    }
                    if (item.key == 'memUsage') {
                        if (result.length > 0) {
                            var totResultLen = result.length - 1;
                            node_module.asyncForEach(result, function(item, index, arr) {
                                if (item.used != null) {
                                    result[index].used = node_module.convert(item.used).from('B').to('GB');
                                }
                                if (item.total != null) {
                                    result[index].total = node_module.convert(item.total).from('B').to('GB');
                                }
                                if (index == totResultLen) {
                                    dashboardResObj.lineRes.memUsage = result;
                                    next();
                                }
                            });
                        } else {
                            dashboardResObj.lineRes.memUsage = result;
                            next();
                        }
                    }
                    if (item.key == 'network') {
                        if (result.length > 0) {
                            var totResultLen = result.length - 1;
                            node_module.asyncForEach(result, function(item, index, arr) {
                                if (item.inval != null) {
                                    result[index].inval = node_module.convert(item.inval).from('b').to('Mb');
                                }
                                if (item.out != null) {
                                    result[index].out = node_module.convert(item.out).from('b').to('Mb');
                                }
                                if (index == totResultLen) {
                                    dashboardResObj.lineRes.network = result;
                                    next();
                                }
                            });
                        } else {
                            dashboardResObj.lineRes.network = result;
                            next();
                        }
                    }
                    if (item.key == 'stoarge') {
                        var storageRes = node_module.arraySort(result, 'time');

                        var totResultLen = result.length - 1;
                        node_module.asyncForEach(storageRes, function(item, index, arr) {
                            if (item.free != null) {
                                storageRes[index].free = node_module.convert(item.free).from('B').to('GB');
                            }
                            if (index == totResultLen) {
                                dashboardResObj.lineRes.stoarge = storageRes;
                                next();
                            }
                        });
                    }
                }).catch(err => {
                    res.status(500).send(err.stack)
                })
            }
            if (slectedDb == 'collectd_db') {
                collectdDb.query(item.value.query).then(result => {
                    if (item.key == 'activevm') {
                        dashboardResObj.lineRes.activevm = result;
                    }
                    next();
                }).catch(err => {
                    res.status(500).send(err.stack)
                })
            }
            if (slectedDb == 'vcpe') {
                vcpeDb.query(item.value.query).then(result => {
                    if (item.key == 'networkRx') {
                        dashboardResObj.lineRes.networkRx = result;
                    }
                    if (item.key == 'networkTx') {
                        dashboardResObj.lineRes.networkTx = result;
                    }
                    next();
                }).catch(err => {
                    res.status(500).send(err.stack)
                })
            }
        }, function() {
            res.status(200).send(dashboardResObj)
        });
    } else {
        var response = {
            resCode: 201,
            response: 'Please provide host.'
        }
        res.status(200).send(response);
    }
});
/* Radar Host  Dashboard Api
  Created  : 06-06-2017
*/
router.get('/getHostRadarDashboardDetails', function(req, res, next) {
    var host = req.query.host;
    if (host != undefined && host != '') {
        var dashboardResObj = {};
        dashboardResObj.radarRes = {};
        const telegrafVimDb = new node_module.Influx.InfluxDB({
            host: SETTING.influxHost,
            database: SETTING.telegrafVimDb,
            schema: []
        })
        const vcpeDb = new node_module.Influx.InfluxDB({
            host: SETTING.influxHost,
            database: SETTING.vcpeDb,
            schema: []
        });
        const collectdDb = new node_module.Influx.InfluxDB({
            host: SETTING.influxHost,
            database: SETTING.collectdDb,
            schema: []
        });
        var dashboardConfigObj = {};
        dashboardConfigObj.dasboardObj = {};
        dashboardConfigObj.dasboardObj.radarCpu = {
            query: "SELECT 100 - mean(usage_idle) FROM cpu WHERE host = '" + host + "' AND cpu = cpu-total AND time > now() - 5m",
            db: 'telegraf-vim'
        };
        dashboardConfigObj.dasboardObj.radarMemory = {
            query: "SELECT mean(used) * 100 / mean(total) FROM mem WHERE host ='" + host + "' AND time > now() - 5m",
            db: 'telegraf-vim'
        };
        dashboardConfigObj.dasboardObj.radarStorage = {
            query: "SELECT mean(used)*100/mean(total) AS used FROM disk WHERE path =~ /storage/ AND host ='" + host + "' AND time > now() - 5m",
            db: 'telegraf-vim'
        };
        dashboardConfigObj.dasboardObj.networkRx = {
            query: "SELECT mean(rx)/(mean(speed)*10000) FROM net_global_usage WHERE host ='" + host + "' AND time > now() - 5m",
            db: 'vcpe'
        };
        dashboardConfigObj.dasboardObj.networkTx = {
            query: "SELECT mean(tx)/(mean(speed)*10000) FROM net_global_usage WHERE host ='" + host + "' AND time > now() - 5m",
            db: 'vcpe'
        };
        node_module.asyncLoop(dashboardConfigObj.dasboardObj, function(item, next) {
            var slectedDb = item.value.db;
            if (slectedDb == 'telegraf-vim') {
                telegrafVimDb.query(item.value.query).then(result => {
                    if (item.key == 'radarCpu') {
                        dashboardResObj.radarRes.radarCpu = result;
                    }
                    if (item.key == 'radarMemory') {
                        dashboardResObj.radarRes.radarMemory = result;
                    }
                    if (item.key == 'radarStorage') {
                        dashboardResObj.radarRes.radarStorage = result;
                    }
                    next();
                }).catch(err => {
                    res.status(500).send(err.stack)
                })
            }
            if (slectedDb == 'vcpe') {
                vcpeDb.query(item.value.query).then(result => {
                    if (item.key == 'networkRx') {
                        dashboardResObj.radarRes.networkRx = result;
                    }
                    if (item.key == 'networkTx') {
                        dashboardResObj.radarRes.networkTx = result;
                    }
                    next();
                }).catch(err => {
                    res.status(500).send(err.stack)
                })
            }
            if (slectedDb == 'collectd_db') {
                collectdDb.query(item.value.query).then(result => {
                    if (item.key == 'activeVm') {
                        dashboardResObj.radarRes.activeVm = result;
                    }
                    next();
                }).catch(err => {
                    res.status(500).send(err.stack)
                })
            }
        }, function() {
            res.status(200).send(dashboardResObj)
        });
    } else {
        var response = {
            resCode: 201,
            response: 'Please provide host.'
        }
        res.status(200).send(response);
    }
});
router.get('/getUnit', customodule.authenticate, function(req, res, next) {
    var testvar = 30000000000;
    var convertvar = node_module.convert(testvar).from('B').to('GB');
});
/* get business report dashboard details
  Created  : 25-04-2017
*/
router.get('/getBusinessReport', function(req, res, next) {
    var select = " count(*) as totPlanCnt";
    var join = "";
    var condiction = " p.plan_id != 0 ";
    var param = {
        condiction: condiction,
        join: join,
        select: select
    }
    Customermodel.getPlanWithServiceTemplate(param, function(err) {
        customodule.helper.winstonContext(req, res).info("Getting error on fetching plans with service template ");
    }, function(totPlanCntData) {
        Customermodel.getRevenuePlanByCondition(param, function(err) {
            customodule.helper.winstonContext(req, res).info("Getting error on fetching revenue plans");
        }, function(resRevenuePlanData) {
            var response = {
                resCode: 200,
                planRes: totPlanCntData[0],
                reveuneRes: resRevenuePlanData
            }
            res.status(200).send(response);
        });
    });
})
/* get business report mrr by plan dashboard details
  Created  : 12-06-2017
*/
router.get('/getBusinessMrrByPlanReport', function(req, res, next) {
    var select = " ";
    var join = "";
    var condiction = " ";
    var param = {
        condiction: condiction,
        join: join,
        select: select
    }
    Customermodel.getMprPlanByCondition(param, function(err) {
        customodule.helper.winstonContext(req, res).info("Getting error on fetching mpr plans.");
    }, function(resMrrPlanData) {
        var mrrRes = node_module.arraySort(resMrrPlanData, 'day');
        var response = {
            resCode: 200,
            mrrRes: mrrRes
        }
        res.status(200).send(response);
    });
})
/* get business report mrr by plan dashboard details
  Created  : 12-06-2017
*/
router.get('/getBusinessSubscriptionReport', function(req, res, next) {
    var select = " ";
    var join = "";
    var condiction = " ";
    var param = {
        condiction: condiction,
        join: join,
        select: select
    }
    Customermodel.getBusinessAccountsSummarySubscriptionByCondition(param, function(err) {
        customodule.helper.winstonContext(req, res).info("Getting error on fetching business accounts summary");
    }, function(resBusinessAccountsSummarySubscriptionData) {
        if (resBusinessAccountsSummarySubscriptionData == undefined) {
            resBusinessAccountsSummarySubscriptionData = {};
        }
        Customermodel.getBusinessReportCancellationsSubscriptionByCondition(param, function(err) {
            customodule.helper.winstonContext(req, res).info("Getting error on fetching business report cancellation subscription");
        }, function(resReportCancellationSubscriptionData) {
            if (resReportCancellationSubscriptionData == undefined) {
                resReportCancellationSubscriptionData = {};
            }
            var response = {
                resCode: 200,
                resBusinessAccountsSummaryRes: resBusinessAccountsSummarySubscriptionData,
                resBusinessReportCancellationRes: resReportCancellationSubscriptionData,
            }
            res.status(200).send(response);
        });
    });
})
/* get business report dashboard details
  Created  : 25-04-2017
*/
router.post('/getCustomerTimeline', function(req, res, next) {
    var timelineListApiPath = '';
    var client_id = req.body.client_id;
    if (!client_id) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        timelineListApiPath = SETTING.clientSavePath + '/' + client_id + '/events/';
        globalAPI.listGlobalApi(timelineListApiPath, function(error) {
            customodule.helper.winstonContext(req, res).info("Getting error on calling global api");
        }, function(data, response) {
            if (data.length > 0) {
                data.resCode = 200;
                data[0].userIdType = req.userType;
                res.status(200).send(data);
            } else {
                res.status(200).send([]);
            }
        });
    }
})

/* API for Users provisioning */
router.post('/addCustomerUsersProvision', function(req, res) {
    var adminPortalProvision = req.body.adminPortalProvision;
    var apiProvision = req.body.apiProvision;
    var customerPortalRegd = req.body.customerPortalRegd;
    var customerIntialStatus = req.body.customerIntialStatus;
    var country = req.body.country;
    var state = req.body.state;
    var city = req.body.city;
    if (!adminPortalProvision || !customerPortalRegd || !apiProvision || !customerIntialStatus || !country || !state || !city) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        if (req.body.adminPortalProvision == "true" || req.body.adminPortalProvision == "TRUE" || req.body.adminPortalProvision == true) {
            var adminPortalProvision = 1;
        } else {
            var adminPortalProvision = 0;
        }
        if (apiProvision == "true" || apiProvision == "TRUE" || apiProvision == true) {
            var apiProvision = 1;
        } else {
            var apiProvision = 0;
        }
        if (req.body.customerPortalRegd == "true" || req.body.customerPortalRegd == "TRUE" || req.body.customerPortalRegd == true) {
            var customerPortalRegd = 1;
        } else {
            var customerPortalRegd = 0;
        }
        if (req.body.customerIntialStatus == "true" || req.body.customerIntialStatus == "TRUE" || req.body.customerIntialStatus == true) {
            var customerIntialStatus = 1;
        } else {
            var customerIntialStatus = 0;
        }
        var data = {
            admin_portal_provision: adminPortalProvision,
            api_provision: apiProvision,
            customer_portal_regd: customerPortalRegd,
            customer_intial_status: customerIntialStatus,
            country: country,
            state: state,
            city: city
        }
        var condiction = "cup.provision_id != 0 ";
        var select = " cup.* ";
        var join = "";
        var param = {
            condiction: condiction,
            join: join,
            select: select
        }
        Customermodel.getCustomerUsersProvisionFullInfoByCondiction(param, function(err) {
            var response = {
                resCode: 201,
                response: error
            }
            res.status(200).send(response);
        }, function(prvisonRes) {
            if (prvisonRes == undefined || Object.keys(prvisonRes).length == 0) {
                Customermodel.createUsersProvision(data, function(error) {
                    var response = {
                        resCode: 201,
                        response: error
                    }
                    res.status(200).send(response);
                }, function(success) {
                    data.provision_id = success;
                    data.resCode = 200;
                    res.status(200).send(data);
                });
            } else {
                var cond = {
                    provision_id: prvisonRes.provision_id
                }
                Customermodel.updateUsersProvision(data, cond, function(error) {
                    var response = {
                        resCode: 201,
                        response: error
                    }
                    res.status(200).send(response);
                }, function(success) {
                    data.provision_id = prvisonRes.provision_id;
                    data.resCode = 200;
                    res.status(200).send(data);
                });
            }
        });
    }
});
/* API for get customer Users provision */
router.get('/getCustomerUsersProvision', function(req, res, next) {
    var condiction = "cup.provision_id != 0 ";
    var select = " cup.* ";
    var join = "";
    var param = {
        condiction: condiction,
        join: join,
        select: select
    }
    Customermodel.getCustomerUsersProvisionFullInfoByCondiction(param, function(err) {
        var response = {
            resCode: 201,
            response: error
        }
        res.status(200).send(response);
    }, function(prvisonRes) {
        if (typeof prvisonRes != 'undefined' && prvisonRes != '' && prvisonRes != null && Object.keys(prvisonRes).length > 0) {
            var data = {
                adminPortalProvision: prvisonRes.admin_portal_provision,
                apiProvision: prvisonRes.api_provision,
                customerPortalRegd: prvisonRes.customer_portal_regd,
                customerIntialStatus: prvisonRes.customer_intial_status,
                country: prvisonRes.country,
                state: prvisonRes.state,
                city: prvisonRes.city
            }
            res.status(200).send(data);
        } else {
            var response = {
                resCode: 201,
                response: []
            }
            res.status(200).send(response);
        }
    });
});

var storage2 = node_module.multer.diskStorage({
    destination: function(req, file, cb) {       
        var dest = 'public/images/option';
        node_module.mkdirp(dest, function(err) {
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + '.' + 'jpg');
    }
});
var uploadOption = node_module.multer({
    storage: storage2
});
//THIS API IS USED TO CREATE THE CUSTOMER OPTIONS
router.post('/createOption', uploadOption.any(), function(req, res) {
    var companyName = req.body.companyName;
    var companyDescription = req.body.companyDescription;
    var colorPicker = req.body.colorPicker;
    var websiteUrl = req.body.websiteUrl;
    var image = req.files;
    var companylogoImage = "";
    var smallCompanylogoImage = "";
    var favIcon = "";

    if (typeof image != 'undefined' && image != '' && image != null) {
        for (var count = 0; count < image.length; count++) {
            if (image[count].fieldname == 'companyLogo') {
                companylogoImage = image[count].filename;
            } else if (image[count].fieldname == 'smallCompanyLogo') {
                smallCompanylogoImage = image[count].filename;
            } else if (image[count].fieldname == 'favIcon') {
                favIcon = image[count].filename;
            }
        }
    }
    var data = {
        company_name: companyName,
        company_description: companyDescription,
        color_picker: colorPicker,
        website_url: websiteUrl,
        company_logo: companylogoImage,
        small_company_logo: smallCompanylogoImage,
        favicon: favIcon
    }
    if (typeof favIcon == 'undefined' || favIcon == '' || favIcon == null) {
        delete data.favicon;
    }
    if (typeof companylogoImage == 'undefined' || companylogoImage == '' || companylogoImage == null) {
        delete data.company_logo;
    }
    if (typeof smallCompanylogoImage == 'undefined' || smallCompanylogoImage == '' || smallCompanylogoImage == null) {
        delete data.small_company_logo;
    }
    var condiction = "cpo.id != 0 ";
    var select = " cpo.* ";
    var join = "";
    var param = {
        condiction: condiction,
        join: join,
        select: select
    }
    Customermodel.getCustomerOptionFullInfoByCondiction(param, function(err) {
        var response = {
            resCode: 201,
            response: error
        }
        res.status(200).send(response);
    }, function(prvisonRes) {
        if (prvisonRes.length == 0) {
            Customermodel.createCustomerOption(data, function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success) {
                var condiction = "cpo.id != 0 ";
                var select = " cpo.* ";
                var join = "";
                var provisionResObj = {};
                var param = {
                    condiction: condiction,
                    join: join,
                    select: select
                }
                Customermodel.getCustomerOptionFullInfoByCondiction(param, function(err) {
                    var response = {
                        resCode: 201,
                        response: error
                    }
                    res.status(200).send(response);
                }, function(prvisonRes) {
                    if (prvisonRes.length > 0) {
                        provisionResObj.absoluteLocalPath = SETTING.optionPathLocal;
                        provisionResObj.absoluteLivePath = SETTING.optionPathLive;
                        provisionResObj.resCode = 200;
                        provisionResObj.id = prvisonRes[0].id;
                        provisionResObj.company_name = prvisonRes[0].company_name;
                        provisionResObj.website_url = prvisonRes[0].website_url;
                        provisionResObj.favicon = prvisonRes[0].favicon;
                        provisionResObj.company_logo = prvisonRes[0].company_logo;
                        provisionResObj.company_description = prvisonRes[0].company_description;
                        provisionResObj.color_picker = prvisonRes[0].color_picker;
                        res.status(200).send(provisionResObj);
                    } else {
                        provisionResObj.absoluteLocalPath = SETTING.optionPathLocal;
                        provisionResObj.absoluteLivePath = SETTING.optionPathLive;
                        provisionResObj.resCode = 200;
                        provisionResObj.id = "";
                        provisionResObj.company_name = "";
                        provisionResObj.website_url = "";
                        provisionResObj.favicon = "";
                        provisionResObj.company_logo = "";
                        provisionResObj.company_description = "";
                        provisionResObj.color_picker = "";
                        res.status(200).send(provisionResObj);
                    }
                });
            });
        } else {
            var cond = {
                id: prvisonRes[0].id
            }
            var provisionResObj = {};
            Customermodel.updateCustomerOption(data, cond, function(error) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(success) {
                var condiction = "cpo.id != 0 ";
                var select = " cpo.* ";
                var join = "";
                var param = {
                    condiction: condiction,
                    join: join,
                    select: select
                }
                Customermodel.getCustomerOptionFullInfoByCondiction(param, function(err) {
                    var response = {
                        resCode: 201,
                        response: error
                    }
                    res.status(200).send(response);
                }, function(prvisonRes) {
                    if (prvisonRes.length > 0) {
                        provisionResObj.absoluteLocalPath = SETTING.optionPathLocal;
                        provisionResObj.absoluteLivePath = SETTING.optionPathLive;
                        provisionResObj.resCode = 200;
                        provisionResObj.id = prvisonRes[0].id;
                        provisionResObj.company_name = prvisonRes[0].company_name;
                        provisionResObj.website_url = prvisonRes[0].website_url;
                        provisionResObj.favicon = prvisonRes[0].favicon;
                        provisionResObj.company_logo = prvisonRes[0].company_logo;
                        provisionResObj.company_description = prvisonRes[0].company_description;
                        provisionResObj.color_picker = prvisonRes[0].color_picker;
                        res.status(200).send(provisionResObj);
                    } else {
                        provisionResObj.absoluteLocalPath = SETTING.optionPathLocal;
                        provisionResObj.absoluteLivePath = SETTING.optionPathLive;
                        provisionResObj.resCode = 200;
                        provisionResObj.id = "";
                        provisionResObj.company_name = "";
                        provisionResObj.website_url = "";
                        provisionResObj.favicon = "";
                        provisionResObj.company_logo = "";
                        provisionResObj.company_description = "";
                        provisionResObj.color_picker = "";
                        res.status(200).send(provisionResObj);
                    }
                });
            });
        }
    });
});

//THIS API IS USED TO CREATE THE CUSTOMER OPTIONS
router.get('/getOption', function(req, res) {
    var condiction = "cpo.id != 0 ";
    var select = " cpo.* ";
    var join = "";
    var param = {
        condiction: condiction,
        join: join,
        select: select
    }
    Customermodel.getCustomerOptionFullInfoByCondiction(param, function(err) {
        var response = {
            resCode: 201,
            response: error
        }
        res.status(200).send(response);
    }, function(prvisonRes) {
        if (typeof prvisonRes != 'undefined' && prvisonRes != '' && prvisonRes != null) {
            var optionRes = {};
            optionRes = prvisonRes[0];
            optionRes.absoluteLocalPath = SETTING.optionPathLocal;
            optionRes.absoluteLivePath = SETTING.optionPathLive;
            optionRes.resCode = 200;
            res.status(200).send(optionRes);
        } else {
            var optionRes = {};
            res.status(200).send(optionRes);
        }

    });
});

var storage3 = node_module.multer.diskStorage({
    destination: function(req, file, cb) {        
        var dest = 'public/images/profile';
        node_module.mkdirp(dest, function(err) {
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + '.' + 'jpg');
    }
});
var uploadOption = node_module.multer({
    storage: storage3
});
//THIS API IS USED TO UPDATE PROFILE
router.post('/updateProfile', uploadOption.any(), function(req, res) {
    var profileRes = {};
    var image = req.files;
    var profileType = req.body.profileType;
    var userId = req.body.userId;
    if (typeof image != 'undefined' && image != '' && image != null) {
        for (var count = 0; count < image.length; count++) {
            if (image[count].fieldname == 'profileImage') {
                profileImage = image[count].filename;
            }
        }
    }
    if (profileType == 'administrator') {
        var data = {
            profile_image: profileImage,
        }
        var cond = {
            user_id: userId
        }
        Customermodel.updateUserByCondiction(data, cond, function(error) {
            var response = {
                resCode: 201,
                response: error
            }
            res.status(200).send(response);
        }, function(success) {
            if (success) {
                profileRes.response = 'Profile image updated successfully.';
                profileRes.resmessage = 200;
                profileRes.profile_image = SETTING.profileImagePath + profileImage;
                res.status(200).send(profileRes);
            } else {
                res.status(200).send(profileRes);
            }
        });
    }
    if (profileType == 'customer') {
        var data = {
            profile_image: profileImage,
        }
        var cond = {
            c_user_id: userId
        }
        Customermodel.updateCustomerUserByCondiction(data, cond, function(error) {
            var response = {
                resCode: 201,
                response: error
            }
            res.status(200).send(response);
        }, function(success) {
            if (success) {
                profileRes.response = 'Profile image updated successfully.';
                profileRes.resmessage = 200;
                profileRes.profile_image = SETTING.profileImagePath + profileImage;
                res.status(200).send(profileRes);
            } else {
                res.status(200).send(profileRes);
            }
        });
    }
});
//THIS API IS USED TO CREATE THE CUSTOMER OPTIONS
router.post('/getSubscriptionDetails',customodule.authenticate, function(req, res) {
    var subscriptionDetailRes = {};
    var planSubscriptionId  = req.body.plan_subscription_id;
    var clientId = ''
    if(req.userType == 'USER')
    {       
        clientId = req.client_id;            
    }
    else
    {
        clientId = req.body.client_id;
    }    
    if (!planSubscriptionId) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        var condiction = '';
        if(typeof clientId != 'undefined' && clientId != null && clientId != '' )
        {
            condiction = "ps.plan_subscription_id = " + planSubscriptionId +" AND ps.client_id = "+ clientId + " ";        
        }
        else
        {
            condiction = "ps.plan_subscription_id = " + planSubscriptionId +" ";
        }        
        var select = " ps.*,pl.*,vt.connection_html ";
        var join = "LEFT JOIN plan as pl ON ps.plan_id = pl.plan_id";
        join += " LEFT JOIN vservice_template as vt ON pl.vservice_template_id = vt.vservice_template_id ";
        var param = {
            condiction: condiction,
            join: join,
            select: select
        }
        Customermodel.getSubscriptionDetailsFullInfoByCondiction(param, function(err) {
            var response = {
                resCode: 201,
                response: error
            }
            res.status(200).send(response);
        }, function(subscriptionRes) {            
            if(typeof subscriptionRes != 'undefined' && subscriptionRes != '' && subscriptionRes != null)
            {
                if (Object.keys(subscriptionRes).length > 0) {
                    //get data from vms service detail
                    var serviceVmsRes = {};
                    var getVmsDetailSql = "SELECT * FROM vnet_vm vv LEFT JOIN service_vm sv ON vv.vnet_vm_id = sv.vnet_vm_id "
                    getVmsDetailSql += " LEFT JOIN vservice vs ON vs.vservice_id = sv.vservice_id WHERE vv.vnet_vm_id != 0 ";
                    getVmsDetailSql += " AND vs.vservice_template_id = " + subscriptionRes.vservice_template_id;
                    getVmsDetailSql += " AND vs.vservice_id = " + subscriptionRes.service_id;                
                    Customermodel.getVmsDetailInfoByCondition(getVmsDetailSql, function(err) {
                        customodule.helper.winstonContext(req, res).info("Getting error on fetching vms details");
                    }, function(getVmsDetailRes) {
                        if (getVmsDetailRes != undefined) {
                            if (getVmsDetailRes.length > 0) {
                                serviceVmsRes = getVmsDetailRes[0];
                            } else {
                                serviceVmsRes = {};
                            }
                        }
                        //get all fields of service (all the fields in "vservice")
                        var getServiceSql = "SELECT * FROM vservice WHERE vservice_id = " + subscriptionRes.service_id;
                        Customermodel.getCommonInfoByCondition(getServiceSql, function(err) {
                            customodule.helper.winstonContext(req, res).info("Getting error on fetching vms details");
                        }, function(getServiceRes) {
                            var templateServiceRes = {};
                            if (getServiceRes != undefined) {
                                if (getServiceRes.length > 0) {
                                    templateServiceRes = getServiceRes[0];
                                } else {
                                    templateServiceRes = {};
                                }
                            }
                            //get all fields of plan subscription (all the fields in "plan_subscription")
                            var getPlanSubscriptionSql = "SELECT * FROM plan_subscription WHERE plan_subscription_id = " + subscriptionRes.plan_subscription_id;
                            Customermodel.getCommonInfoByCondition(getPlanSubscriptionSql, function(err) {
                                customodule.helper.winstonContext(req, res).info("Getting error on fetching plan subscription details");
                            }, function(getPlanSubscriptionRes) {
                                var templatePlanSubscriptionRes = {};
                                if (getPlanSubscriptionRes != undefined) {
                                    if (getPlanSubscriptionRes.length > 0) {
                                        templatePlanSubscriptionRes = getPlanSubscriptionRes[0];
                                    } else {
                                        templatePlanSubscriptionRes = {};
                                    }
                                }

                                //get all fields of client (all the fields in "customer")
                                var getClientSql = "SELECT * FROM client WHERE client_id = " + subscriptionRes.client_id;
                                Customermodel.getCommonInfoByCondition(getClientSql, function(err) {
                                    customodule.helper.winstonContext(req, res).info("Getting error on fetching plan subscription details");
                                }, function(getClientRes) {
                                    var templateClientRes = {};
                                    if (getClientRes != undefined) {
                                        if (getClientRes.length > 0) {
                                            templateClientRes = getClientRes[0];
                                        } else {
                                            templateClientRes = {};
                                        }
                                    }
                                    //get plan service configuration
                                    var condiction = "psa.plan_subscription_id =" + planSubscriptionId;
                                    var select = " psa.name,psa.value ";
                                    var join = "";
                                    var param = {
                                        condiction: condiction,
                                        join: join,
                                        select: select
                                    }
                                    var planConfiguration = {};
                                    Customermodel.getPlanSubscriptionDetailInfoByCondition(param, function(err) {
                                        customodule.helper.winstonContext(req, res).info("Getting error on fetching plan subscription details");
                                    }, function(planServiceConfigData) {
                                        var planServiceConfigLen = planServiceConfigData.length;
                                        for (i = 0; i < planServiceConfigData.length; i++) {
                                            var key = planServiceConfigData[i].name;
                                            planConfiguration[key] = planServiceConfigData[i].value;                                        
                                        }
                                        subscriptionDetailRes = subscriptionRes;
                                        var templateParam = subscriptionRes.connection_html;
                                        var resolveTemplate = node_module.esTemplate(templateParam, { config: planConfiguration, vms: getVmsDetailRes, service: templateServiceRes, subscription: templatePlanSubscriptionRes, customer: templateClientRes });                                                
                                        subscriptionDetailRes.connection_html = resolveTemplate;                                           
                                        var serviceSavePath = SETTING.serviceSavePath + '/' + planSubscriptionId;
                                        globalAPI.listGlobalApi(serviceSavePath, function(error) {
                                            customodule.helper.winstonContext(req, res).info("Getting error on calling global apis");
                                        }, function(data, serviceResponse) {
                                            subscriptionDetailRes.serviceResponse = data;
                                            subscriptionDetailRes.resCode = 200;
                                            subscriptionDetailRes.planConfiguration = planConfiguration;
                                            subscriptionDetailRes.getVmsDetailRes = serviceVmsRes;
                                            subscriptionDetailRes.service = templateServiceRes;
                                            subscriptionDetailRes.subscription = templatePlanSubscriptionRes;
                                            subscriptionDetailRes.customer = templateClientRes;
                                            res.status(200).send(subscriptionDetailRes);
                                        });
                                    });
                                });
                            });
                        });
                    });
                } else {
               res.status(200).send(subscriptionDetailRes);
                }
            }
            else
            {
            res.status(200).send(subscriptionDetailRes);  
            }
        });
    }
});

router.get('/getCustomerSupport', function(req, res) {
    var wsse = node_module.wsse;
    var request = node_module.request;
    var moment = node_module.moment;
    var WSSEToken = node_module.WSSEToken;

    var token2 = new wsse.UsernameToken({
        username: SETTING.diamenteDeskUsername, // (required)
        password: SETTING.diamenteDeskPassword // (required)
    });
    request({
        'method': "GET",
        'uri': SETTING.diamantedeskurl + "tickets",
        'headers': {
            'Authorization': 'WSSE profile="UsernameToken"',
            'X-WSSE': token2.getWSSEHeader({ nonceBase64: true }),
            'Accept': 'application/json',
            'Content-type': 'application/json'
        }
    }, (err, res, body) => {
        customodule.helper.winstonContext(req, res).info("Getting error on calling customer support api in diamente");
    });
});
router.get('/getTicket', function(req, res) {
    var token2 = new wsse.UsernameToken({
        username: SETTING.diamenteDeskUsername, // (required)
        password: SETTING.diamenteDeskPassword // (required)
    });
    //diamanteDeskBaseUrl
    request({
        'method': "GET",
        'uri': SETTING.diamantedeskurl + "tickets",
        'headers': {
            'Authorization': 'WSSE profile="UsernameToken"',
            'X-WSSE': token2.getWSSEHeader({
                nonceBase64: true
            }),
            'Accept': 'application/json',
            'Content-type': 'application/json'
        }
    }, (err, res, body) => {
        customodule.helper.winstonContext(req, res).info("Getting error on calling get ticket api in diamente");
    });
});

router.post('/addBranch', function(req, res) {
    var token2 = new wsse.UsernameToken({
        username: SETTING.diamenteDeskUsername, // (required)
        password: SETTING.diamenteDeskPassword // (required)           
    });
    request({
        'method': "POST",
        'uri': SETTING.diamantedeskurl + "branches",
        'headers': {
            'Authorization': 'WSSE profile="UsernameToken"',
            'X-WSSE': token2.getWSSEHeader({
                nonceBase64: true
            }),
            'Accept': 'application/json',
            'Content-type': 'application/json'
        }
    }, (err, res, body) => {
        customodule.helper.winstonContext(req, res).info("Getting error on adding branch in diamente");
    });
});
//API to get the list of addon already booked
router.post('/getBookedAddonList', function(req, res) {
    addOnRes = {};
    var planSubscriptionId = req.body.plan_subscription_id;
    if (!planSubscriptionId) {
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    } else {
        var condiction = "ps.parent_plan_id = " + planSubscriptionId;
        var select = " ps.*,p.plan_name,p.display_name ";
        var join = " LEFT JOIN plan p ON ps.plan_id = p.plan_id ";
        var param = {
            condiction: condiction,
            join: join,
            select: select
        }
        Customermodel.getSubscriptionDetailsFullInfoByCondictionDetails(param, function(err) {
            var response = {
                resCode: 201,
                response: error
            }
            res.status(200).send(response);
        }, function(bookedAddonRes) {
            addOnRes.resCode = 200;
            if (bookedAddonRes != undefined) {
                addOnRes.bookedAddonRes = bookedAddonRes;
            } else {
                addOnRes.bookedAddonRes = {};
            }
            res.status(200).send(addOnRes);
        });
    }
});
// 
//This is used to fetch Plan Subscription API
router.post('/planSubscription',customodule.authenticate,function(req, res, next) {
    var subscriptionId = req.body.subscription_id
    var subscriptionListApiPath = '';
    subscriptionListApiPath = SETTING.planSubscriptionConfigEditPath + subscriptionId + '/events/';
    globalAPI.listGlobalApi(subscriptionListApiPath, function(error) {
        var response = {
            resCode: 201,
            response: error
        }
        res.status(200).send(response);
    }, function(data, response) {        
        if(data.length >0)
        {
            data[0].userIdType = req.userType;
        }        
        var response = {
            resCode: 200,
            response: data
        }
        res.status(200).send(response);
    });
});
router.get('/businessRepostDashboardDetails', function(req, res, next) {
    var async = node_module.async;
    async.parallel({
        activeClientCount: function(callback) {
            var condiction = "status = 1";
            var select = "";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getActiveAndDeactiveClient(param, function(err) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(activeClients) {
                callback(null, activeClients);
            });
        },
        deactiveClientCount: function(callback) {
            var condiction = "status IN (1,0)";
            var select = "";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getActiveAndDeactiveClient(param, function(err) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(deactiveClients) {
                callback(null, deactiveClients);
            });
        },
        subscribingCustomer: function(callback) {
            var condiction = "";
            var select = "";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getCustomerSubscriptionCondition(param, function(err) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(customerSubscribingDetails) {                
                callback(null, customerSubscribingDetails);
            });
        }
    }, function done(err, results) {
        var activeClient =0;
        var deactiveClient =0;
        var subscribingCustomers =0;
        if(typeof results.activeClientCount != 'undefined' && results.activeClientCount != '' && results.activeClientCount != null)
        {
            activeClient = results.activeClientCount[0].clientCount;
        }
        if(typeof results.deactiveClientCount != 'undefined' && results.deactiveClientCount != '' && results.deactiveClientCount != null)
        {
            deactiveClient = results.deactiveClientCount[0].clientCount;
        }
        if(typeof results.subscribingCustomer != 'undefined' && results.subscribingCustomer != '' && results.subscribingCustomer != null)
        {
            subscribingCustomers = results.subscribingCustomer[0].count;
        }    
        var response = {
            resCode: 200,
            activeClient: activeClient,
            deactiveClient: deactiveClient,
            subscribingCustomers: subscribingCustomers         
        }        
        res.status(200).send(response);
    });
});
router.get('/businessRepostLineChartDetails', function(req, res, next) {
    var async = node_module.async;
    async.parallel({
        customerByDay: function(callback) {
            var condiction = "";
            var select = "";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getCustomerByDayCondition(param, function(err) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(customerByDayDetails) {
                callback(null, customerByDayDetails);
            });
        },
        conversionsByDay: function(callback) {
            var condiction = "";
            var select = "";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getConversionsByDayCondition(param, function(err) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(conversionByDayDetails) {
                callback(null, conversionByDayDetails);
            });
        },
        trialStartedByDay: function(callback) {
            var condiction = "";
            var select = "";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getTrialStartedByDayCondition(param, function(err) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(trialStartedByDayDetails) {
                callback(null, trialStartedByDayDetails);
            });
        },
        cancellationByDay: function(callback) {
            var condiction = "";
            var select = "";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getCancellationByDayCondition(param, function(err) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(cancellationByDayDetails) {
                callback(null, cancellationByDayDetails);
            });
        }
    }, function done(err, results) {
        var response = {
            resCode: 200,
            customerByDay: node_module.arraySort(results.customerByDay, 'day'),
            conversionsByDay: node_module.arraySort(results.conversionsByDay, 'day'),
            trialStartedByDay: node_module.arraySort(results.trialStartedByDay, 'day'),
            cancellationByDay: node_module.arraySort(results.cancellationByDay, 'day')
        }
        res.status(200).send(response);        
    });
});
router.get('/businessRepostDoughnutChartDetails', function(req, res, next) {
    var async = node_module.async;
    var dounutChart = {
                        "label":'',
                        "data":''
                        }
    var dounutChart2 = {
                        "label":'',
                        "data":''
                        }
    var label = [];
    var data = [];
    var labelOverdue = [];
    var dataOverdue = [];
    async.parallel({
        pieChartDataForSubscriptionNNonSubscription: function(callback) {
            var condiction = "";
            var select = "";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getCustomerSubscriptionNonsubscriptionCondition(param, function(err) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(SubscriptionNNonSubscriptionDetails) {
                callback(null, SubscriptionNNonSubscriptionDetails);
            });
        },
        pieChartDataForOverDueStatus: function(callback) {
            var condiction = "";
            var select = "";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getCustomerOverdueStatusCondition(param, function(err) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(customerOverdueStatusConditionDetails) {
                callback(null, customerOverdueStatusConditionDetails);
            });
        }
    }, function done(err, results) {         
        if(typeof results.pieChartDataForSubscriptionNNonSubscription != 'undefined' && results.pieChartDataForSubscriptionNNonSubscription != '' && results.pieChartDataForSubscriptionNNonSubscription != null && results.pieChartDataForSubscriptionNNonSubscription.length >0)
        {
            for(var countElement = 0;countElement<results.pieChartDataForSubscriptionNNonSubscription.length;countElement++)
            {
                label[countElement] = results.pieChartDataForSubscriptionNNonSubscription[countElement].label;
                data[countElement] = results.pieChartDataForSubscriptionNNonSubscription[countElement].count;
            }
        }
        if(typeof results.pieChartDataForOverDueStatus != 'undefined' && results.pieChartDataForOverDueStatus != '' && results.pieChartDataForOverDueStatus != null && results.pieChartDataForOverDueStatus.length >0)
        {
            for(var countElementOver = 0;countElementOver<results.pieChartDataForOverDueStatus.length;countElementOver++)
            {
                labelOverdue[countElementOver] = results.pieChartDataForOverDueStatus[countElementOver].state;
                dataOverdue[countElementOver] = results.pieChartDataForOverDueStatus[countElementOver].count;
            }
        }     
        if(dataOverdue.length==0)
        {
            dataOverdue[0] = 0; 
        }
        if(labelOverdue.length==0)
        {
            labelOverdue[0] = "No data"; 
        }
        if(label.length==0)
        {
            label[0] = "No data";
        }
        if(data.length==0)
        {
            data[0] = 0; 
        }           
        dounutChart2.label = label;
        dounutChart2.data = data;
        dounutChart.label = labelOverdue;
        dounutChart.data = dataOverdue;        
        var response = {
            resCode: 200,            
            "firstPieChartData":dounutChart,
            "secondPieChartData":dounutChart2
        }
        res.status(200).send(response);        
    });    
});
router.get('/businessReportClientLineChart_test2', function(req, res, next) {
    var condiction = "";
    var select = "";
    var join = "";
    var param = {
        condiction: condiction,
        join: join,
        select: select
    }
    Customermodel.getCustomerMultipleLineChartCondition(param, function(err) {
        var response = {
            resCode: 201,
            response: error
        }
        res.status(200).send(response);
    }, function(customerMultipleLineChartDetails) {        
        var response = {
            resCode: 200,            
            response: customerMultipleLineChartDetails
        }
        res.status(200).send(response);
    });

    // var lineChartData = [{day: "2017-09-11T12:00:00.000Z", count: 16.663122172312967},
    //                         {day: "2017-09-11T13:00:00.000Z", count: 16.690068016932585},
    //                         {day: "2017-09-11T14:00:00.000Z", count: 16.763063700052314},
    //                         {day: "2017-09-12T15:00:00.000Z", count: 16.76967407725155},
    //                         {day: "2017-09-12T16:00:00.000Z", count: 16.76860774296057},
    //                         {day: "2017-09-12T17:00:00.000Z", count: 17.224053948822757},
    //                         {day: "2017-09-12T18:00:00.000Z", count: 20.957346172674534},
    //                         {day: "2017-09-13T19:00:00.000Z", count: 19.258520596813554},
    //                         {day: "2017-09-13T20:00:00.000Z", count: 11.9331811191648},
    //                         {day: "2017-09-13T21:00:00.000Z", count: 11.708534695202388},
    //                         {day: "2017-09-13T22:00:00.000Z", count: 11.639587020290136},
    //                         {day: "2017-09-14T23:00:00.000Z", count: 11.823657942700251},
    //                         {day: "2017-09-15T00:00:00.000Z", count: 12.045083043238122},
    //                         {day: "2017-09-15T01:00:00.000Z", count: 12.320369176267334},
    //                         {day: "2017-09-15T02:00:00.000Z", count: 12.553049193581018},
    //                         {day: "2017-09-15T03:00:00.000Z", count: 12.6315963439598},
    //                         {day: "2017-09-16T04:00:00.000Z", count: 12.835838539726737},
    //                         {day: "2017-09-17T05:00:00.000Z", count: 30.919176560685528},
    //                         {day: "2017-09-18T05:00:00.000Z", count: 31.919176560685528},
    //                         {day: "2017-09-19T05:00:00.000Z", count: 32.919176560685528},
    //                         {day: "2017-09-20T05:00:00.000Z", count: 0}
    //                       ];  
  
});
router.get('/businessReportClientLineChart', function(req, res, next) {
    var underscore = node_module.underscore;
    var days = [];  
    var customerDetails = [];      
    var clientDays = [];
    var condiction = "";
    var select = "";
    var join = "";
    var param = {
        condiction: condiction,
        join: join,
        select: select
    }
    Customermodel.getCustomerMultipleLineChartCondition(param, function(err) {
        var response = {
            resCode: 201,
            response: error
        }
        res.status(200).send(response);
    }, function(customerMultipleLineChartDetails) {         
        var customerLength = '';
        //Actual customer data from database       
        for(var customerDataCount = 0;customerDataCount<customerMultipleLineChartDetails.length;customerDataCount++)
        {
            var dynamicCustomerDate = new Date(customerMultipleLineChartDetails[customerDataCount].day);
            customerDetails.push({"day":dynamicCustomerDate.getFullYear()+'-'+(dynamicCustomerDate.getMonth()+1)+'-'+dynamicCustomerDate.getDate(),"count":customerMultipleLineChartDetails[customerDataCount].count});  
        }
        customerLength = customerDetails.length;
        //default last 7 days data created
        for(var countDate=0;countDate<7;countDate++)
        {
            var date = new Date();
            date ;
            date.setDate(date.getDate() - countDate);
            days.push({day: date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate(), count: 0});
        }        
        //tranverse thought default loop
        for(var outerCount = 0;outerCount<days.length;outerCount++)
        {
            //tranverse thought customer data from database loop
            if(customerLength>0)
            {
                for(var innerCount = 0;innerCount<customerLength;innerCount++)
                {
                    if(days[outerCount].day.toString() == customerDetails[innerCount].day.toString())
                    {
                        break;                    
                    }
                    else
                    {
                        if(innerCount==customerLength-1)
                        {
                            customerDetails.push(days[outerCount]);
                        }
                    }
                }
            }
            else{
                //When there is no actual customer data
                customerDetails.push(days[outerCount]);
            }            
        }       
        var response = {
            resCode: 200,          
            response: underscore.sortBy( customerDetails, 'day')
        }
        
        res.status(200).send(response);
    });
});
router.get('/businessRepostLineChartDetails_test', function(req, res, next) {
    var async = node_module.async;
    async.parallel({
        customerByDay: function(callback) {
            var condiction = "";
            var select = "";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getCustomerByDayCondition(param, function(err) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(customerByDayDetails) {
                callback(null, customerByDayDetails);
            });
        },
        conversionsByDay: function(callback) {
            var condiction = "";
            var select = "";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getConversionsByDayCondition(param, function(err) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(conversionByDayDetails) {
                callback(null, conversionByDayDetails);
            });
        },
        trialStartedByDay: function(callback) {
            var condiction = "";
            var select = "";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getTrialStartedByDayCondition(param, function(err) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(trialStartedByDayDetails) {
                callback(null, trialStartedByDayDetails);
            });
        },
        cancellationByDay: function(callback) {
            var condiction = "";
            var select = "";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getCancellationByDayCondition(param, function(err) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(cancellationByDayDetails) {
                callback(null, cancellationByDayDetails);
            });
        }
    }, function done(err, results) {
        var response = {
            resCode: 200,
            customerByDay: node_module.arraySort(results.customerByDay, 'day'),
            conversionsByDay: node_module.arraySort(results.conversionsByDay, 'day'),
            trialStartedByDay: node_module.arraySort(results.trialStartedByDay, 'day'),
            cancellationByDay: node_module.arraySort(results.cancellationByDay, 'day')
        }
        res.status(200).send(response);      
    });
});
router.post('/businessReportClient', function(req, res, next) {
    var async = node_module.async;    
    async.parallel({
        totalCustomers: function(callback) {
            var condiction = " where client_id != 0";
            var select = "select * ";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getTotalCustomerCondition(param, function(err) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(totalCustomerDetails) {                
                callback(null, totalCustomerDetails);
            });
        },
        activeCustomers: function(callback) {
            var condiction = " where status = 1";
            var select = "";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getActiveCustomerCondition(param, function(err) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(activeCustomerDetails) {                
                callback(null, activeCustomerDetails);
            });
        },
        subscriptionCustomersAverage: function(callback) {
            var condiction = " where 1";
            var select = "";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getCustomerSubscriptionAverageCondition(param, function(err) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(subscriptionCustomersAverageDetails) {                
                callback(null, subscriptionCustomersAverageDetails);
            });
        },
        eachCustomerSubscription: function(callback) {
            var condiction = "";
            var select = "";
            var join = "";
            var param = {
                condiction: condiction,
                join: join,
                select: select
            }
            Customermodel.getClientCountCondition(param, function(err) {
                var response = {
                    resCode: 201,
                    response: error
                }
                res.status(200).send(response);
            }, function(clientsDetails) {                
                var numberofClient = 0;
                if(typeof clientsDetails != 'undefined' && clientsDetails != '' && clientsDetails != null)
                {
                    numberofClient = clientsDetails[0].customerCount; 
                }                               
                var condiction = "";
                var select = "";
                var join = "";
                var param = {
                    condiction: condiction,
                    join: join,
                    select: select
                }                
                Customermodel.getBusinessReportPlansCondition(param, function(err) {
                    var response = {
                        resCode: 201,
                        response: error
                    }
                    res.status(200).send(response);
                }, function(businessReportPlanDetails) {
                    var customerSubscriptionObject = {};
                    var finalCustomerSubscriptionJSON = [];              
                    if(typeof businessReportPlanDetails != 'undefined' && businessReportPlanDetails != '' && businessReportPlanDetails != null)
                    {
                        if(businessReportPlanDetails.length>0)
                        {
                            for(var subscriptionCount =0;subscriptionCount<businessReportPlanDetails.length;subscriptionCount++)
                            {
                                customerSubscriptionObject[subscriptionCount] = 0;
                            }
                        }
                        var condiction = "";
                        var select = "";
                        var join = "";
                        var param = {
                            condiction: condiction,
                            join: join,
                            select: select
                        }
                        Customermodel.getEachCustomerSubscriptionCondition(param, function(err) {
                            var response = {
                                resCode: 201,
                                response: error
                            }
                            res.status(200).send(response);
                        }, function(eachCustomerSubscriptionDetails) {
                            var subscribeCustomer = 0;
                            if(typeof eachCustomerSubscriptionDetails != 'undefined' && eachCustomerSubscriptionDetails != '' && eachCustomerSubscriptionDetails != null)
                            {
                                if(eachCustomerSubscriptionDetails.length>0)
                                {
                                    for(var eachCustomerCount =0;eachCustomerCount<eachCustomerSubscriptionDetails.length;eachCustomerCount++)
                                    {
                                        var subscriptionLength = eachCustomerSubscriptionDetails[eachCustomerCount].subscriptions.split(',').length;
                                        customerSubscriptionObject[subscriptionLength] = customerSubscriptionObject[subscriptionLength]+1;                                    
                                        if(eachCustomerCount == eachCustomerSubscriptionDetails.length-1)
                                        {
                                            for(key in customerSubscriptionObject)
                                            {
                                                subscribeCustomer = customerSubscriptionObject[key]+subscribeCustomer;
                                            }
                                        }
                                    }
                                }
                            }
                            customerSubscriptionObject[0] = numberofClient-subscribeCustomer;                            
                            for(key in customerSubscriptionObject)
                            {
                                finalCustomerSubscriptionJSON.push({"subscriptions":key,"customers":customerSubscriptionObject[key]});                                
                            }                            
                            callback(null, finalCustomerSubscriptionJSON);
                        });
                    }
                });
            });
        }        
    }, function done(err, results) {        
        var totalCustomers =0;
        var activeCustomers =0;
        var subscriptionCustomerAverage =0;
        var customerSubscriptions =0;
        if(typeof results.totalCustomers != 'undefined' && results.totalCustomers != '' && results.totalCustomers != null)
        {
            totalCustomers =results.totalCustomers[0].totalCustomer;
        }
        if(typeof results.activeCustomers != 'undefined' && results.activeCustomers != '' && results.activeCustomers != null)
        {
            activeCustomers = results.activeCustomers[0].activeCustomer;
        }
        if(typeof results.subscriptionCustomersAverage != 'undefined' && results.subscriptionCustomersAverage != '' && results.subscriptionCustomersAverage != null)
        {
            subscriptionCustomerAverage = results.subscriptionCustomersAverage[0].averageCustomerSubscription;
        }
        if(typeof results.eachCustomerSubscription != 'undefined' && results.eachCustomerSubscription != '' && results.eachCustomerSubscription != null)
        {
            customerSubscriptions =results.eachCustomerSubscription;
        }
        var response = {
            resCode: 200,
            totalCustomers: totalCustomers,
            activeCustomers: activeCustomers,
            subscriptionCustomerAverage: subscriptionCustomerAverage,
            customerSubscriptions: customerSubscriptions            
        }
        res.status(200).send(response);        
    });
});
router.post('/businessReportClientSaleIncome', function(req, res, next) {
    var async = node_module.async;    
    async.parallel({
        currentSale: function(callback) {
            var graphArray = [];
            var currenMonthSaleObject = {"saleInCurrentMonthMain":"193,45","pageVisit" :"26,900","newVisit":"46.11%","lastWeek":"432.021","graph":[{day:"2017-11-11",count:"5"},{day:"2017-11-11",count:"5"},{day:"2017-11-11",count:"5"}]};                                    
            callback(null, currenMonthSaleObject);           
        },
        saleIn24Hours: function(callback) {
            var graphArray = [];
            var saleIn24HoursObject = {"saleIn24HoursMain":"562,90","pageVisit" :"26,900","newVisit":"46.11%","lastWeek":"432.021","graph":[{day:"2017-11-11",count:"5"},{day:"2017-11-11",count:"5"},{day:"2017-11-11",count:"5"}]};                                   
            callback(null, saleIn24HoursObject);
        },
        incomeLastMonth: function(callback) {
            var graphArray = [];
            var lastMonthIncomeObject = {"project" :"142","comments":"61","companies":"432.021","companies":"152","incomeLastMonth":"160,000"};               
            callback(null, lastMonthIncomeObject);
        },
        currentYearSale: function(callback) {
            var graphArray = [];
            var currentYearSaleObject = {"Messages" :"22","articles":"54","clients":"32","companies":"152","saleCurrentYear":"42,120"};                                   
            callback(null, currentYearSaleObject);
        }        
    }, function done(err, results) {       
        var response = {
            resCode: 200,
            currentSale: results.currentSale,                    
            saleIn24Hours: results.saleIn24Hours,                    
            incomeLastMonth: results.incomeLastMonth,                    
            currentYearSale: results.currentYearSale                    
        }
        res.status(200).send(response);        
    });
});
module.exports = router;
