var config = require('../config/setting.js');
var Authmodel = require('../routes/api/authapi/user/model/user');
var Customermodel    = require('../routes/api/authapi/customer/model/customer');
var datetime = require('node-datetime');
var ERROR = {
    UNAUTH: {
        success: false,
        resCode: 401,
        resMessage: "Unauthorized Access"
    },
    TOKENEXP: {
        success: false,
        resCode: 401,
        resMessage: "Accesstoken Expire"
    },
    UNAUTHENTICATE: {
        success: false,
        resCode: 401,
        resMessage: "Failed to authenticate token."
    }
};
function isAuthenticated(req, res, next) {
    if (req.headers['userlogintype'] != 'customer') {   
        //Client registration from customer portal
        // CHECK THE USER STORED IN SESSION FOR A CUSTOM VARIABLE
        // you can do this however you want with whatever variables you set up
        //  var token  = req.headers['token'];
        var token = (!req.headers['token']) ? req.body['token'] : req.headers['token'];
        var ret = new Date();
        var currentime = ret.getTime();
        /* Set the expire time of logout to 24 hours */
        //var exp_time = ret.setTime(ret.getTime() + 60 * 24 * 60000);
        var exp_time = ret.setTime(ret.getTime() +  60 * 60000);
        var expiredate = datetime.create(new Date(exp_time));        
        var expire = expiredate.format("Y-m-d H:M:S");
        var apikey = (!req.headers['apikey']) ? req.body['apikey'] : req.headers['apikey'];
        var cond = {
            token: token
        }
        if (apikey == config.definedapikey) {
            if (token) {
                Authmodel.getAccessTokenByCondiction(cond, function(err) {                    
                }, function(token) {
                    if (token.length > 0) {
                        var expire_time = new Date(token[0].expire_time).getTime();                        
                        req.userId      = token[0].user_id;
                        req.userType    = 'ADMIN'
                        //need to be less than
                        if (currentime < expire_time) {
                            var data = {
                                expire_time: expire
                            }
                            Authmodel.updateAccessTokenByCondiction(data, cond, function(err) {                                
                            }, function(success) {                                
                                if(req.session.permissions == undefined || req.session.permissions == 'undefined'){
                                    var condiction="ts.token='"+token[0].token+"' ";
                                    var select  = "u.user_id as userId,ifnull(u.user,'') as userName, ifnull(u.email,'') as userEmail, ifnull(u.phone,'') as userPhone,ifnull(u.mobilePhone,'') as userMobile,ifnull(u.role,'') as userRoleId ,ifnull(r.name,'') as userRole,(SELECT GROUP_CONCAT(p.name) from role_permissions as rp LEFT JOIN permission as p ON rp.permission_id=p.permission_id where rp.role_id=u.role) as permissionName, (SELECT GROUP_CONCAT(name) from permission where 1) as fullpermission";
                                    var join    = "LEFT JOIN role as r ON r.role_id=u.role LEFT JOIN token_secret as ts ON ts.user_id=u.user_id";
                                    var param     = {
                                    condiction: condiction,
                                    join      : join,
                                    select    : select
                                    }
                                    Authmodel.getUserFullInfoByCondiction(param,function(err){},function(userdata){
                                        if(userdata.length>0){
                                            req.session.admin = (userdata[0].userRoleId==1 || userdata[0].userRoleId=='1' )?true:false;
                                            req.session.permissions = userdata[0].permissionName.split(',');                                            
                                            next();
                                        }else{
                                            res.status(200).send(ERROR.TOKENEXP);
                                        }
                                    });
                                }else{
                                    next();
                                }
                            })
                        } else {
                            req.session.destroy(function(err) {
                                //code
                                res.status(200).send(ERROR.TOKENEXP);
                            })

                        }
                    } else {
                        res.status(200).send(ERROR.UNAUTHENTICATE);
                    }
                })
            } else {
                res.status(200).json(ERROR.UNAUTH);
            }
        } else {
            res.status(200).send(ERROR.UNAUTH);
        }
       
    } else {   
        console.log("req.headers ::",req.headers);     
        if(req.headers['clientregistration'] == 'Y')
        {
            var apikey = (!req.headers['apikey'])?req.body['apikey']:req.headers['apikey'];
            if (apikey==config.definedapikey) {
                console.log("working");
              next();
            }else{
                console.log("not working");
              res.status(401).json(ERROR.UNAUTH);
            }
        }else{
            console.log("not working 2");
            // CHECK THE USER STORED IN SESSION FOR A CUSTOM VARIABLE
            // you can do this however you want with whatever variables you set up
            //  var token  = req.headers['token'];
            var token = (!req.headers['token']) ? req.body['token'] : req.headers['token'];
            var ret = new Date();
            var currentime = ret.getTime();
            /* Set the expire time of logout to 24 hours */
            var exp_time = ret.setTime(ret.getTime() +  60 * 60000);
            var expiredate = datetime.create(new Date(exp_time));
            var expire = expiredate.format("Y-m-d H:M:S");
            var apikey = (!req.headers['apikey']) ? req.body['apikey'] : req.headers['apikey'];
            var cond = {
                token: token
            }
            if (apikey == config.definedapikey) {
                if (token) {
                    Customermodel.getCustomerPortalAccessTokenByCondiction(cond, function(err) {                    
                    }, function(token) {
                        if (token.length > 0) {
                            var expire_time = new Date(token[0].expire_time).getTime();
                            console.log("Customer Token details ***************");
                            console.log(token[0].client_id);
                            console.log("Customer Token details end ***************");
                            req.client_id    = token[0].client_id;
                            console.log(req.client_id);
                            req.userId      = token[0].customer_user_id;
                            req.userType    = 'USER'
                            if (currentime < expire_time) {
                                var data = {
                                    expire_time: expire
                                }
                                Customermodel.updateCustomerPortalAccessTokenByCondiction(data, cond, function(err) {                                
                                }, function(success) {
                                    next();
                                })
                            } else {
                                req.session.destroy(function(err) {
                                    //res.status(200).send(ERROR.TOKENEXP);
                                    res.status(200).send(ERROR.TOKENEXP);
                                })

                            }
                        } else {
                            //res.status(200).send(ERROR.UNAUTHENTICATE);
                            res.status(200).send(ERROR.UNAUTHENTICATE);
                        }
                    })
                } else {
                    //res.status(200).json(ERROR.UNAUTH);
                    res.status(200).send(ERROR.UNAUTH);
                }
            } else {
                //res.status(200).send(ERROR.UNAUTH);
                res.status(200).send(ERROR.UNAUTH);
            }
        }
    }
}
module.exports = isAuthenticated;
