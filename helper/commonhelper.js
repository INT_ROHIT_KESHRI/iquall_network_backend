module.exports = {
  	"winstonContext":function(req,res)
  	{
  		var WinstonContext = require('winston-context');
		var logger = require('winston');
	    // Create a per-request child
	    var requestCtx = new WinstonContext(logger, '', {
	        requestId: req.apiRequestId
	    });
	    return requestCtx;   
  	}
}
