var ERROR = {

    USER: {
        REQUIREDFIELD: {
            "resCode": 201,
            "response": "Please give all required fields"
        },
        EMAILREQUIRED: {
            "resCode": 201,
            "response": "Email address required"
        },
        INVALIDEMAIL: {
            "resCode": 201,
            "response": "Invalid email address"
        },
        INVALIDLOGINCREDENTIAL: {
            "resCode": 201,
            "response": "Please provide valid credentials !"
        },
        INVALIDGROUPTYPE: {
            "resCode": 201,
            "response": "Invalid group type"
        },
        INVALIDGROUPDOB: {
            "resCode": 201,
            "response": "Invalid group type as per date of birth"
        },
        INVALIDUSERTYPE: {
            "resCode": 201,
            "response": "Invalid user type"
        },
        USERALREADYEXIST: {
            "resCode": 201,
            "response": "This email already exist. Please check your email for account validation or do login if already verified."
        },
        PARENTALREADYEXIST: {
            "resCode": 201,
            "response": "Parent email already exist"
        },
        DUPLICATEENTRY: {
            "resCode": 201,
            "response": "Parent email and mobile should be diffrent from user email and mobile."
        },
        INVALIDCREDENTIAL: {
            "resCode": 201,
            "response": "Invalid credential."
        },
        INVALIDUSERPASSWORD: {
            "resCode": 201,
            "response": "Old password is wrong.Please try again with the correct one."
        }
    },

    ROLE: {
        ROLEEXIST: {
            "resCode": 201,
            "response": "Role already exist.Please try with diffrent role name."
        },
        ROLEASSIGNED: {
            "resCode": 201,
            "response": "A user is already assigned to this Role.Please unassign to continue"
        },
        ROLENOTDELETED: {
            "resCode": 201,
            "response": "Unable to delete Role.Please try again."
        }
    },

    COMMON: {
        SOMETHINGWRONG: {
            "resCode": 201,
            "response": "Something went wrong.Pleas try again"
        },
        PERMISSIONDENIED: {
            "resCode": 201,
            "response": "Prmission denied"
        },
        REQUIREDFIELD: {
            "resCode": 201,
            "response": "Please give all required fields"
        },
        REQUESTERROR: {
            "resCode": 201,
            "response": "Request has expired"
        },
        RESPONSEERROR: {
            "resCode": 201,
            "response": "Response has expired"
        }
    },
    DIAMENTE: {
        DIAMENTESERVEREERROR: {
            "resCode": 201,
            "response": "Diamente server error.Please try again"
        }       
    }
}
module.exports = ERROR;
