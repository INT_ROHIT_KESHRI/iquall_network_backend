var homeModule = angular.module('homeModule',[])
homeModule
.config(function($stateProvider, $urlRouterProvider){
    $stateProvider
    .state('app.home', {
        url: '/home',
        cache: false,
        views: {
            'dasView': {
                templateUrl: 'templates/home/home.html',
                //controller: 'HomeCtrl'
            }
        }
    })
})
