var SETTING = require('../config/setting.js');
var node_module = require('../routes/export');
var ERROR = require('../config/statuscode');

var apimodel = {
    saveGlobalApi: function(apiurl, args, res, errorcallback, successcallback) {        
        //send the request parameter through api call
        var client = new node_module.restClient();
        var req = client.post(apiurl, args, function(data, response) {
            successcallback(data, response);
        });
        req.on('requestTimeout', function(req) {            
            res.status(201).send(ERROR.COMMON.REQUESTERROR);
        });
        req.on('responseTimeout', function(res) {            
            res.status(201).send(ERROR.COMMON.RESPONSEERROR);
        });
        req.on('error', function(err) {
            errorcallback("Uh oh! Some error occured while save data.");
        });
    },
    editGlobalApi: function(apiurl, args, errorcallback, successcallback) {        
        //send the request parameter through api call
        var client = new node_module.restClient();        
        var req = client.put(apiurl, args, function(data, response) {
            successcallback(data, response);
        });
        req.on('requestTimeout', function(req) {
            res.status(201).send(ERROR.COMMON.REQUESTERROR);
        });
        req.on('responseTimeout', function(res) {
            res.status(201).send(ERROR.COMMON.RESPONSEERROR);
        });
        req.on('error', function(err) {
            errorcallback("Uh oh! Some error occured while edit data.");
        });
    },
    listGlobalApi: function(apiurl, errorcallback, successcallback) {
        //send the request parameter through api call        
        var client = new node_module.restClient();
        var req = client.get(apiurl, function(data, response) {
            successcallback(data, response);
        });
        req.on('requestTimeout', function(req) {
            res.status(201).send(ERROR.COMMON.REQUESTERROR);
        });
        req.on('responseTimeout', function(res) {
            res.status(201).send(ERROR.COMMON.RESPONSEERROR);
        });
        req.on('error', function(err) {
            errorcallback("Uh oh! Some error occured while fetching data.");
        });
    },
    deleteGlobalApi: function(apiurl, args, errorcallback, successcallback) {
        //send the request parameter through api call        
        var client = new node_module.restClient();
        var req = client.delete(apiurl, function(data, response) {
            successcallback(data, response);
        });
        req.on('requestTimeout', function(req) {
            res.status(201).send(ERROR.COMMON.REQUESTERROR);
        });
        req.on('responseTimeout', function(res) {
            res.status(201).send(ERROR.COMMON.RESPONSEERROR);
        });
        req.on('error', function(err) {
            errorcallback("Uh oh! Some error occured while delete data.");
        });
    }
}
module.exports = apimodel;
