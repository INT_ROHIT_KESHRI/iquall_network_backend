var express = require('express');
var ws = require('ws.js')
     , Http = ws.Http
     , Security = ws.Security
     , UsernameToken = ws.UsernameToken
// var cors = require('cors');
var path = require('path');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var node_module = require('./routes/export');
var database = require('./config/database');
var setting = require('./config/setting');
var qb = require('node-querybuilder').QueryBuilder(database, 'mysql', 'single');
var datetime = require('node-datetime');
var cron = require('node-cron');
var request = require('request');
var fs = require('fs')
//Morgan Start
var morgan = node_module.morgan;
var rfs = node_module.rotatingFileStream;
var addRequestId = node_module.expressRequestId;
var logDirectory = path.join(__dirname, 'iquallApiLogs');
//Checks log directory exist
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);
//Create a rotating write stream
var accessLogApiCallStream = rfs('iquallApiCallLog.log', {
  interval: '1d', // rotate daily
  path: logDirectory
})
app.use(addRequestId({"attributeName":"apiRequestId","setHeader":true}));
// Logs created before API call
app.use(morgan(function (tokens, req, res) {
    return [
                "id: "+req.apiRequestId+";",
                "method: "+req.method+";",
                "url: "+req.protocol + "://" + req.get('host') + req.originalUrl+";",
                "time: "+ new Date()+";",
                "host: "+ req.headers.host+";",
                "origin: "+ req.headers.origin+";",
                "content-type: "+ req.headers['content-type']+";",
                "                                                       ",
                "                                                       "
            ].join(' ')
},{immediate:true,stream: accessLogApiCallStream}));
// Logs created after API call
app.use(morgan(function (tokens, req, res) {
    return [
                "id: "+req.apiRequestId+";",
                "url: "+req.protocol + "://" + req.get('host') + req.originalUrl+";",
                "status: "+tokens.status(req, res)+";",
                "time: "+tokens['response-time'](req, res)+"ms ;",
                "                                                       ",
                "                                                       "
            ].join(' ')
},{immediate:false,stream: accessLogApiCallStream}));
//End Morgan
io.on('connection', function(socket) {
    var socketId = socket.id;
    var blankResponse = '';
    var data = {
        socket_id: socketId
    }
    io.to(socketId).emit('socketJoinMessage', data);
    socket.on('join', function(dataParam) {
        if (dataParam.userParamType == 'customer') {
            //io.sockets.emit('message', data);
            var data = {
                socket_id: dataParam.socketId
            }
            var condiction = {
                token: dataParam.tokenParam
            }
            qb.update('customer_user_token_secret', data, condiction, function(err, res) {
                if (err) {
                } else {

                }
            });
        } else {
            //io.sockets.emit('message', data);
            var data = {
                socket_id: socketId
            }
            var condiction = {
                token: dataParam.tokenParam
            }
            qb.update('token_secret', data, condiction, function(err, res) {
                if (err) {
                } else {
                }
            });
        }
    });
    //cron schedule for customer
    cron.schedule('*/40 * * * * *', function() {
        var ret = new Date();
        var currentime = ret.getTime();
        var currentDate = datetime.create(new Date(currentime));
        var currentDateVal = currentDate.format("Y-m-d H:M:S");
        var getExpireSocketSql = "Select * FROM  customer_user_token_secret WHERE customer_secret_token_id != 0  AND expire_time < '" + currentDateVal + "' AND socket_id != '' ORDER BY customer_secret_token_id DESC ";
        var socketCustomerArr = [];
        qb.query(getExpireSocketSql, function(err, expireSocketResponse) {
            if (expireSocketResponse != undefined) {
                var totSocketArrLen = expireSocketResponse.length;
                if (totSocketArrLen > 0) {
                    node_module.asyncForEach(expireSocketResponse, function(expireSocketItem, expireSocketIndex, expireSocketArr) {
                        var selectedSocketId = expireSocketArr[expireSocketIndex].socket_id;
                        var socketMessageObj = {};
                        socketCustomerArr.push({
                            "socketid": selectedSocketId
                        });
                        socketMessageObj.previousSessionExpiretime = expireSocketArr[expireSocketIndex].expire_time;
                        socketMessageObj.sessionExpireMessage = 'Your session has been expired';
                        var removeSocketId = selectedSocketId;
                        var data = {
                            socket_id: '',
                            is_expired: 'Y'
                        }
                        var condiction = {
                            socket_id: removeSocketId
                        }
                        qb.delete('customer_user_token_secret', condiction, function(err, res) {
                            if (err) {} else {
                                io.to(selectedSocketId).emit('sessionOutMessage', socketMessageObj);
                                if (totSocketArrLen == socketCustomerArr.length) {

                                }
                            }
                        });
                    });
                }
            }
        });
    });

    //cron schedule for admin
    cron.schedule('*/40 * * * * *', function() {
        var ret = new Date();
        var currentime = ret.getTime();
        var currentDate = datetime.create(new Date(currentime));
        var currentDateVal = currentDate.format("Y-m-d H:M:S");

        //***** START ADMIN SECTION ****//
        var getExpireSocketSql = "Select * FROM  token_secret WHERE token_secret_id != 0  AND expire_time < '" + currentDateVal + "' AND socket_id != '' ORDER BY token_secret_id DESC ";
        console.log(getExpireSocketSql);
        var socketAdminArr = [];
        qb.query(getExpireSocketSql, function(err, expireSocketResponse) {
            if (expireSocketResponse != undefined) {
                var totSocketArrLen = expireSocketResponse.length;
                node_module.asyncForEach(expireSocketResponse, function(expireSocketItem, expireSocketIndex, expireSocketArr) {
                    var selectedSocketId = expireSocketArr[expireSocketIndex].socket_id;
                    var socketMessageObj = {};
                    socketAdminArr.push({
                        "socketid": selectedSocketId
                    });
                    socketMessageObj.previousSessionExpiretime = expireSocketArr[expireSocketIndex].expire_time;
                    socketMessageObj.sessionExpireMessage = 'Your session has been expired';
                    var removeSocketId = selectedSocketId;
                    var data = {
                        socket_id: '',
                        is_expired: 'Y'
                    }
                    var condiction = {
                        socket_id: removeSocketId
                    }
                    qb.delete('token_secret', condiction, function(err, res) {
                        if (err) {} else {
                            io.to(selectedSocketId).emit('sessionOutMessage', socketMessageObj);
                            if (totSocketArrLen == socketAdminArr.length) {}
                        }
                    });
                });
            }
        });
        //***** END ADMIN SECTION ****//
    });

    //**** forcefully delete token through socket **********
    socket.on('deleteTokenParam', function(dataParam) {
        var tokenParam = dataParam.tokenParam;
        if (dataParam.userParamType == 'customer') {
            var condiction = {
                token: dataParam.tokenParam
            }
            qb.delete('customer_user_token_secret', condiction, function(err, res) {
                if (err) {} else {}
            });
        } else {
            var condiction = {
                token: dataParam.tokenParam
            }
            qb.delete('token_secret', condiction, function(err, res) {
                if (err) {} else {}
            });
        }
    });

    //**** forcefully update token time through socket **********
    socket.on('updateTokenParamSession', function(dataParam) {
        var tokenParam = dataParam.tokenParam;
        var ret = new Date();
        var currentime = ret.getTime();
        /* Set the expire time of logout to 24 hours */
        //var exp_time = ret.setTime(ret.getTime() + 60 * 24 * 60000);
        var exp_time = ret.setTime(ret.getTime() +  60 * 60000);
        var expiredate = datetime.create(new Date(exp_time));
        var expire = expiredate.format("Y-m-d H:M:S");
        if (dataParam.userParamType == 'customer') {
            var data = {
                expire_time: expire
            }
            var condiction = {
                token: dataParam.tokenParam
            }
            qb.update('customer_user_token_secret', data, condiction, function(err, res) {
                if (err) {
                } else {
                }
            });
        } else {
            var data = {
                expire_time: expire
            }
            var condiction = {
                token: dataParam.tokenParam
            }
            qb.update('token_secret', data, condiction, function(err, res) {
                if (err) {
                } else {
                }
            });
        }
    });
});
var port = 5000;
var bodyParser = require('body-parser');
var header = require('./middleware/header');
var userSocket = require('./middleware/userSocket');
var session = require('express-session');
//THIS IS USED FOR GRAFANA WITH EJS
app.set('view engine', 'ejs');

/** Api Controllers **/
var UnauthUser = require('./routes/api/unauthapi/user/controller/user');
var AuthUser = require('./routes/api/authapi/user/controller/user');
var AuthCustomer = require('./routes/api/authapi/customer/controller/customer');
var diamanteDesk = require('./routes/api/authapi/diamantedesk/controller/diamantedesk');
/** End **/

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}));
app.use(express.static(path.join(__dirname, 'customerportal')));
app.use('/admin', express.static(path.join(__dirname, 'app')));
//app.use(express.static(path.join(__dirname, '/views')));
app.use(session({
    secret: 'mysecretkey'
}));
//app.use(session({secret: 'mysecretkey'},cookie:{maxAge:86400000}));
app.use(header);
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Credentials", true);
    next();
});
app.get('/public/*', function(req, res) {
    res.sendFile(path.join(__dirname, req.originalUrl));
});
/* Set the path for all controllers */
// app.use('/api/unauthUser', UnauthUser);
// app.use('/api/authUser', AuthUser);
// app.use('/api/authCustomer', AuthCustomer);
// app.use('/api/diamanteDesk', diamanteDesk);
app.use('/unauthUser', UnauthUser);
app.use('/authUser', AuthUser);
app.use('/authCustomer', AuthCustomer);
app.use('/diamanteDesk', diamanteDesk);
/** End **/
server.listen(port, function() {
    console.log(setting.grafanaurl);
    node_module.winston.log("Server connected on port",port);
});

app.get(/^\/grafana*/,function(req,res)
{
    var request = require('request');
    delete(req.headers["content-length"]);
    delete(req.headers["origin"]);
    var url = setting.grafanaurl;
    var options = {
        method: req.method,
        url: url+ req.originalUrl.substring(8)
    };
    options.headers = req.headers;
    if(req.method.toLowerCase() == "get")
        options.qsStringifyOptions = {
            arrayFormat: 'repeat'
        };
    //if (req.session && req.session.user && req.session.user.grafana_id) {
    if (req.query.user) {
        var user  = req.query.user;
        //module.exports.pipeRequest(user.email, req, options, res,function(){
        pipeRequest(user, req, options, res,function(){
            res.send(500);
        });
    } else {
        pipeRequest(false, req, options, res,function(){
           res.send(500);
        });
    }
});
function pipeRequest(user, req, options,res,callback)
{
    var apiKey      = setting.grafanaAPIKe;
    if (req.method == "GET") {
        options.qs = req.query;
    }
    if (req.method == "POST" || req.method == "PUT"  || req.method == "PATCH") {
        options.body = req.body;
    }
    var roleGrafana = "viewer";

    var keyUser = roleGrafana;
        options.headers["X-WEBAUTH-USER"] = "admin";
        options.headers["content-type"] = "application/json";
        options.headers["Accept"]= "application/json";
        options.json = true;
        options.timeout = 1000*10;
    var sendRequest  = request(options,function(error, response, body){
            if (error  && error.code){
                callback(error);
            }else{
                //sendRequest.pipe(res);
            }
    }).pipe(res);

    sendRequest.on('error', function(err) {
    });
}
