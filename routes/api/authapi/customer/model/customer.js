var database = require('../../../../../config/database');
var remoteDatabase = require('../../../../../config/remoteDatabase');
var pool = require('node-querybuilder').QueryBuilder(database, 'mysql', 'pool');
var queryDb = require('node-querybuilder').QueryBuilder(database, 'mysql', 'single');
var qbRemote = require('node-querybuilder').QueryBuilder(remoteDatabase, 'mysql', 'single');
var globalAPI = require('../../../../../middleware/globalapi');
var SETTING = require('../../../../../config/setting');
var node_module = require('../../../../export');

var model = {
    /* Role Management Section */
    createRole: function(data, errorcallback, successcallback) {
        queryDb.select('*')
            .where(data)
            .get('customer_role', function(err, response) {
                if (err) {
                    errorcallback("Uh oh! Couldn't get results: " + err.msg);
                } else {
                    if (response.length > 0) {

                        errorcallback("Role already exist.Please try with diffrent role name");
                    } else {
                        queryDb.insert('customer_role', data, function(err, res) {
                            if (err) errorcallback(err);
                            successcallback(res.insertId);
                        });
                    }
                }
            });
    },
    updateRole: function(cond, data, errorcallback, successcallback) {
        queryDb.select('*')
            .where({
                name: data.name,
                'c_role_id !=': cond.role_id
            })
            .get('customer_role', function(err, response) {
                if (err) {
                    errorcallback("Error in updating role.Please try again");
                } else {
                    if (response.length > 0) {
                        errorcallback("Role already exist.Please try with diffrent role name");
                    } else {
                        queryDb.update('customer_role', data, cond, function(err, res) {
                            if (err) errorcallback("Uh oh! Couldn't update results: " + err.msg);
                            successcallback(true);
                        });
                    }
                }
            });

    },
    deleteCustomerRole: function(cond, errorcallback, successcallback) {
        queryDb.delete('customer_role', cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't delete results: " + err);
            } else {
                successcallback(true);
            }
        });
    },
    getRoles: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from customer_role as r " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        });
    },
    /* Permission Management Section */
    createPermission: function(data, errorcallback, successcallback) {
        queryDb.select('*')
            .where({
                name: data.name
            })
            .get('permission', function(err, response) {
                if (err) {
                    errorcallback("Uh oh! Couldn't get results: " + err.msg);
                } else {
                    if (response.length > 0) {
                        errorcallback("Permission already exist.Please try with diffrent permission name");
                    } else {
                        queryDb.insert('permission', data, function(err, res) {
                            if (err) errorcallback(err);
                            successcallback(res.insertId);
                        });
                    }
                }
            });
    },
    updatePermission: function(cond, data, errorcallback, successcallback) {
        queryDb.select('*')
            .where({
                name: data.name
            })
            .get('permission', function(err, response) {
                if (err) {
                    errorcallback("Uh oh! Couldn't get results: " + err.msg);
                } else {
                    if (response.length > 0) {
                        errorcallback("Permission already exist.Please try with diffrent permission name");
                    } else {
                        queryDb.update('permission', data, cond, function(err, res) {
                            if (err) {
                                errorcallback("Uh oh! Couldn't update results: " + err.msg);
                            } else {
                                successcallback(true);
                            }
                        });
                    }
                }
            });
    },
    deletePermission: function(cond, errorcallback, successcallback) {
        queryDb.delete('permission', cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't delete results: " + err);
            } else {
                successcallback(true);
            }
        });
    },
    getPermissions: function(errorcallback, successcallback) {
        queryDb.select('*')
            .where({
                'c_permission_id !=': ''
            })
            .get('customer_permission', function(err, response) {
                if (err) {
                    errorcallback(err);
                } else {
                    successcallback(response);
                }
            })
    },
    getCustomerPermissions: function(customerpermissionIds, errorcallback, successcallback) {
        return queryDb.query("Select name from customer_permission where c_permission_id IN (" + customerpermissionIds + ")", function(err, response) {
            successcallback(response);
        });
    },
    getVServiceTemplate: function(errorcallback, successcallback) {
        return queryDb.query("Select vservice_template_id,name from vservice_template", function(err, response) {
            successcallback(response);
        })
    },
    /* Role-Permission Management Section */
    createRolePermission: function(data, errorcallback, successcallback) {
        queryDb.insert('customer_role_permissions', data, function(err, res) {
            if (err) errorcallback(err);
            successcallback(res.insertId);
        });
    },
    updateRolePermission: function(cond, data, errorcallback, successcallback) {
        queryDb.update('customer_role_permissions', data, cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't update results: " + err.msg);
            } else {
                successcallback(true);
            }
        });
    },
    deleteRolePermission: function(cond, errorcallback, successcallback) {
        queryDb.delete('customer_role_permissions', cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't delete results: " + err);
            } else {
                successcallback(true);
            }
        });
    },
    /* User section */
    getClientByCondiction: function(condiction, errorcallback, successcallback) {
        return queryDb.select('*')
            .where(condiction)
            .get('customer_user', function(err, response) {
                if (err) {
                    errorcallback("Uh oh! Couldn't get results: " + err.msg);
                } else {
                    successcallback(response);
                }
            });
    },
    createUser: function(data, errorcallback, successcallback) {
        queryDb.select('*')
        .where({
            email: data.email
        })
        .get('user', function(err, response) {
            if (err) {
                errorcallback("Uh oh! Couldn't get results: " + err.msg);
            } else {
                if (response.length > 0) {
                    errorcallback("User already exist with this email.Please try with diffrent email");
                } else {
                    queryDb.insert('user', data, function(err, res) {
                        if (err) {
                            errorcallback(err);
                        } else {
                            successcallback(res.insertId);
                        }
                    });
                }
            }
        });
    },
    updateUserByCondiction: function(data, cond, errorcallback, successcallback) {
        queryDb.select('*')
        .where({
            user_id: cond.user_id
        })
        .get('user', function(err, response) {
            if (err) {
                errorcallback("Uh oh! Couldn't get results: " + err.msg);
            } else {
                if (response.length == 0) {
                    errorcallback("User doesnot exist.");
                } else {
                    queryDb.update('user', data, cond, function(err, res) {
                        if (err) {
                            errorcallback("Uh oh! Couldn't update results: " + err);
                        } else {
                            successcallback(true);
                        }
                    });
                }
            }
        });
    },
    getUserFullInfoByCondiction: function(param, errorcallback, successcallback) {
        var response = "Select " + param.select + " from client as u " + param.join + " where " + param.condiction
        return queryDb.query("Select " + param.select + " from client as u " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    deleteUser: function(cond, errorcallback, successcallback) {
        queryDb.delete('user', cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't delete results: " + err);
            } else {
                successcallback(true);
            }
        });
    },
    deleteClient: function(cond, errorcallback, successcallback) {
        queryDb.delete('client', cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't delete results: " + err);
            } else {
                successcallback(true);
            }
        });
    },
    deletePlan: function(cond, errorcallback, successcallback) {
        queryDb.delete('plan', cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't delete results: " + err);
            } else {
                successcallback(true);
            }
        });
    },
    /* Token section */
    getAccessTokenByCondiction: function(condiction, errorcallback, successcallback) {
        return queryDb.select('*')
            .where(condiction)
            .get('token_secret', function(err, response) {
                if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
                successcallback(response);
            });
    },
    removeAccessToken: function(cond, errorcallback, successcallback) {
        queryDb.delete('token_secret', cond, function(err, res) {
            if (err) errorcallback("Uh oh! Couldn't delete results: " + err);
            successcallback(true);
        });

    },
    createAccessToken: function(data, errorcallback, successcallback) {
        return queryDb.insert('token_secret', data, function(err, res) {
            if (err) errorcallback(err);
            successcallback(res.insertId);
        });
    },
    createCustomer: function(data, errorcallback, successcallback) {
        queryDb.insert('client', data, function(err, res) {
            if (err) {
                errorcallback(err);
            } else {
                successcallback(res.insertId);
            }
        });
    },
    createPlan: function(data, errorcallback, successcallback) {
        queryDb.insert('plan', data, function(err, res) {
            if (err) {
                errorcallback(err);
            } else {
                successcallback(res.insertId);
            }
        });
    },
    getCustomerFullInfoByCondiction: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from client as u " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    /* User section */
    getCustomerByCondiction: function(condiction, errorcallback, successcallback) {
        return queryDb.select('*')
            .where(condiction)
            .get('customer_user', function(err, response) {
                if (err) {
                    errorcallback("Uh oh! Couldn't get results: " + err.msg);
                } else {
                    successcallback(response);
                }
            });
    },
    updateClientByCondiction: function(data, cond, errorcallback, successcallback) {
        queryDb.select('*')
            .where({
                client_id: cond.client_id
            })
            .get('client', function(err, response) {
                if (err) {
                    errorcallback("Uh oh! Couldn't get results: " + err.msg);
                } else {
                    if (response.length == 0) {
                        errorcallback("User doesnot exist.");
                    } else {
                        queryDb.update('client', data, cond, function(err, res) {
                            if (err) {
                                errorcallback("Uh oh! Couldn't update results: " + err);
                            } else {
                                successcallback(true);
                            }
                        });
                    }
                }
            });
    },
    updatePlanByCondiction: function(data, cond, errorcallback, successcallback) {
        queryDb.update('plan', data, cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't update results: " + err);
            } else {
                successcallback(true);
            }
        })
    },
    getPlanWithServiceTemplate: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from plan as p " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    getServiceTemplateDetails: function(param, errorcallback, successcallback) {
        queryDb.select('name')
            .where({
                vservice_template_id: param
            })
            .get('vservice_template', function(err, response) {
                successcallback(response);
            });
    },
    getPlanByConditionWithServiceTemplate: function(param, errorcallback, successcallback) {
        var planquery = "Select " + param.select + " from plan as p " + param.join + " where " + param.condiction;
        return queryDb.query("Select " + param.select + " from plan as p " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    getPlanByCondition: function(planIds, errorcallback, successcallback) {
        return queryDb.query("Select image from plan where plan_id IN (" + planIds + ")", function(err, response) {
            successcallback(response);
        });
    },
    createCustomerUser: function(data, errorcallback, successcallback) {
        queryDb.insert('customer_user', data, function(err, res) {
            if (err) {
                errorcallback(err);
            } else {
                successcallback(res.insertId);
            }
        });
    },
    getCustomerUserFullInfoByCondiction: function(param, errorcallback, successcallback) {
        var responsequery = "Select " + param.select + " from customer_user as u " + param.join + " where " + param.condiction;
        return queryDb.query(responsequery, function(err, response) {
            successcallback(response);
        })
    },
    deleteCustomerUser: function(cond, errorcallback, successcallback) {
        queryDb.delete('customer_user', cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't delete results: " + err);
            } else {
                successcallback(true);
            }
        });
    },
    updateCustomerUserByCondiction: function(data, cond, errorcallback, successcallback) {
        queryDb.select('*')
            .where({
                c_user_id: cond.c_user_id
            })
            .get('customer_user', function(err, response) {
                if (err) {
                    errorcallback("Uh oh! Couldn't get results: " + err.msg);
                } else {
                    if (response.length == 0) {
                        errorcallback("User doesnot exist.");
                    } else {
                        queryDb.update('customer_user', data, cond, function(err, res) {
                            if (err) {
                                errorcallback("Uh oh! Couldn't update results: " + err);
                            } else {
                                successcallback(true);
                            }
                        });
                    }
                }
            });
    },
    updateAccessTokenByCondiction: function(data, condiction, errorcallback, successcallback) {
        return queryDb.update('token_secret', data, condiction, function(err, res) {
            if (err) errorcallback("Uh oh! Couldn't update results: " + err.msg);
            successcallback(true);
        });
    },
    createTag: function(data, errorcallback, successcallback) {
        queryDb.insert('tag', data, function(err, res) {
            if (err) {
                errorcallback(err);
            } else {
                successcallback(res.insertId);
            }
        });
    },
    getTagFullInfoByCondiction: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from tag as t " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    deleteTagUser: function(cond, errorcallback, successcallback) {
        queryDb.delete('tag', cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't delete results: " + err);
            } else {
                successcallback(true);
            }
        });
    },
    updateTagByCondiction: function(data, cond, errorcallback, successcallback) {
        queryDb.select('*')
        .where({
            tag_id: cond.tag_id
        })
        .get('tag', function(err, response) {
            if (err) {
                errorcallback("Uh oh! Couldn't get results: " + err.msg);
            } else {
                if (response.length == 0) {
                    errorcallback("User doesnot exist.");
                } else {
                    queryDb.update('tag', data, cond, function(err, res) {
                        if (err) {
                            errorcallback("Uh oh! Couldn't update results: " + err);
                        } else {
                            successcallback(true);
                        }
                    });
                }
            }
        });

    },
    createTagPlan: function(data, errorcallback, successcallback) {
        queryDb.insert('plan_tag', data, function(err, res) {
            if (err) {
                errorcallback(err);
            } else {
                successcallback(res.insertId);
            }
        });
    },
    deleteTagPlan: function(cond, errorcallback, successcallback) {
        queryDb.delete('plan_tag', cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't delete results: " + err);
            } else {
                successcallback(true);
            }
        });
    },
    getTagPlanFullInfoByCondiction: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from tag as t " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    updateAccessTokenByCondiction: function(data, condiction, errorcallback, successcallback) {
        return queryDb.update('token_secret', data, condiction, function(err, res) {
            if (err) errorcallback("Uh oh! Couldn't update results: " + err.msg);
            successcallback(true);
        });
    },
    /* Token section */
    getCustomerPortalAccessTokenByCondiction: function(condiction, errorcallback, successcallback) {
        return queryDb.select('*')
            .where(condiction)
            .get('customer_user_token_secret', function(err, response) {
                if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
                successcallback(response);
            });
    },
    removeCustomerPortalAccessToken: function(cond, errorcallback, successcallback) {
        queryDb.delete('customer_user_token_secret', cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't delete results: " + err);
            } else {
                successcallback(true);
            }
        });
    },
    createCustomerPortalAccessToken: function(data, errorcallback, successcallback) {
        return queryDb.insert('customer_user_token_secret', data, function(err, res) {
            //queryDb.release();
            if (err) errorcallback(err);
            successcallback(res.insertId);
        });
    },
    updateCustomerPortalAccessTokenByCondiction: function(data, condiction, errorcallback, successcallback) {
        return queryDb.update('customer_user_token_secret', data, condiction, function(err, res) {
            if (err) errorcallback("Uh oh! Couldn't update results: " + err.msg);
            successcallback(true);
        });
    },
    createConnectionType: function(data, errorcallback, successcallback) {
        queryDb.insert('connection_type', data, function(err, res) {
            if (err) {
                errorcallback(err);
            } else {
                successcallback(res.insertId);
            }
        });
    },
    deleteConnectionType: function(cond, errorcallback, successcallback) {
        queryDb.delete('connection_type', cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't delete results: " + err);
            } else {
                successcallback(true);
            }
        });
    },
    getConnectionTypeFullInfoByCondiction: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from connection_type as ct " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    getSiteFullInfoByCondiction: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from connection_type as ct " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    getConnectionTypeParameterFullInfoByCondiction: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from connection_type_parameter as ctp " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    getPlanDetails: function(condition, errorcallback, successcallback) {
        var planDetailQuery = "SELECT p.plan_id as plan_id,p.plan_name,p.vservice_template_id,ifnull(p.display_name,'') as display_name,ifnull(p.description,'') as description,ifnull(p.detail,'') as detail,p.image,p.color1,p.color2,p.featured,(SELECT GROUP_CONCAT(tg.name) as tag_name from tag as tg LEFT JOIN plan_tag as ptg ON tg.tag_id=ptg.tag_id WHERE ptg.plan_id = p.plan_id) as plan_tag,(SELECT GROUP_CONCAT(tg.description) as tag_description from tag as tg LEFT JOIN plan_tag as ptg ON ";
        planDetailQuery += " tg.tag_id=ptg.tag_id WHERE ptg.plan_id = p.plan_id) as plan_tag_desc,(SELECT GROUP_CONCAT(tg.tag_id) as tag_name from tag as tg LEFT JOIN plan_tag as ptg ON tg.tag_id=ptg.tag_id WHERE ptg.plan_id = p.plan_id) as plan_tag_ids,ifnull(p.plan_name,'') as plan_name,CASE WHEN p.featured=1 THEN 'true' ELSE 'false' END as featured,CASE WHEN p.allow_subscription=1 THEN 'true' ELSE 'false' END as allow_subscription,ifnull(p.display_name,'') as display_name,";
        planDetailQuery += " ifnull(p.description,'') as description,ifnull(p.detail,'') as detail,ifnull(p.image,'') as image,ifnull(p.color1,'') as color1, ifnull(p.color2,'') as color2,ifnull(p.price,'0') as price,ifnull(v.name,'') as vservice_template_name,v.vservice_template_id as vservice_template_id from plan as p  LEFT JOIN vservice_template as v ON v.vservice_template_id=p.vservice_template_id where  " + condition;
        return queryDb.query(planDetailQuery, function(err, response) {
            successcallback(response);
        })
    },
    getProductDetails: function(condition, errorcallback, successcallback) {
        return queryDb.query("SELECT vservice_template_id,name,description,ifnull(PRODUCT.description,'') AS description,ifnull(PRODUCT.detail,'') AS detail,PRODUCT.min_sites,PRODUCT.max_sites from vservice_template AS PRODUCT  where " + condition, function(err, response) {
            successcallback(response);
        })
    },
    getServiceTemplateParamDetails: function(condition, errorcallback, successcallback) {
        return queryDb.query("SELECT required,default_value,name,parameter_type,enum_values from vservice_template_parameter AS VTP  where " + condition, function(err, response) {
            successcallback(response);
        })
    },
    getBillingFullInfoByCondiction: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from billing_info as billing " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    addBillingInfoModel: function(data, errorcallback, successcallback) {
        queryDb.insert('billing_info', data, function(err, res) {
            if (err) {
                errorcallback(err);
            } else {
                successcallback(res.insertId);
            }
        });
    },
    updateBillingInfoModel: function(cond, data, errorcallback, successcallback) {
        queryDb.update('billing_info', data, cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't update results: " + err.msg);
            } else {
                successcallback(true);
            }
        });
    },
    deleteBillingInfoModel: function(cond, errorcallback, successcallback) {
        queryDb.delete('billing_info', cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't delete results: " + err);
            } else {
                successcallback(true);
            }
        });
    },
    getServiceFullInfoByCondition: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from vservice_template as vt " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    updateServiceInfo: function(cond, data, errorcallback, successcallback) {
        queryDb.update('vservice_template', data, cond, function(err, res) {
            if (err) {
                errorcallback("Uh oh! Couldn't update results: " + err.msg);
            } else {
                successcallback(true);
            }
        });
    },
    getPlanServiceInfoByCondition: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from vservice as vs " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    getSubscriptionDetailInfoByCondition: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from plan_subscription as ps " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    getPlanSubscriptionDetailInfoByCondition: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from plan_subscription_argument as psa " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    getVmsDetailInfoByCondition: function(query, errorcallback, successcallback) {
        return queryDb.query(query, function(err, response) {
            successcallback(response);
        })
    },
    getPlanServiceConfugirationByCondition: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from vservice_argument as vsa " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    getPlanFullInfoByCondiction: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from vservice_template as vt " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    getSelectedSiteFullInfoByCondiction: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from service_site as ss " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    getHostFullInfoByCondition: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from host as h " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    getMprPlanByCondition: function(param, errorcallback, successcallback) {
        var getMprQuery = "select * from v_report_mrr_daily INNER JOIN tenants ON tenants.record_id = v_report_mrr_daily.tenant_record_id WHERE tenants.api_key = 'iqvcpe3'";
        return qbRemote.query(getMprQuery, function(err, response) {
            successcallback(response);
        })
    },
    getRevenuePlanByCondition: function(param, errorcallback, successcallback) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        dd = '01';
        if (mm < 10) {
            mm = '0' + mm
        }
        todayDate = yyyy + '-' + mm + '-' + dd;
        var getRevenueQuery = "select * from v_report_mrr_daily where day < '" + todayDate + "' order by count desc limit 0,5";
        return qbRemote.query(getRevenueQuery, function(err, response) {
            successcallback(response);
        })
    },
    getBusinessAccountsSummarySubscriptionByCondition: function(param, errorcallback, successcallback) {
        var getReportAcctSummaryQuery = "select * from v_report_accounts_summary INNER JOIN tenants ON tenants.record_id = v_report_accounts_summary.tenant_record_id WHERE tenant.api_key = 'iqvcpe3'";
        return qbRemote.query(getReportAcctSummaryQuery, function(err, response) {
            successcallback(response);
        })
    },
    getBusinessReportCancellationsSubscriptionByCondition: function(param, errorcallback, successcallback) {
        var getReportCancellationQuery = "select * from v_report_cancellations_count_daily INNER JOIN tenants ON tenants.record_id = v_report_cancellations_count_daily.tenant_record_id WHERE tenants.api_key = 'iqvcpe3'";
        return qbRemote.query(getReportCancellationQuery, function(err, response) {
            successcallback(response);
        })
    },
    saveSiteDetailsInfo: function(req, planSubscriptionId, errorcallback, successcallback) {
        var precessedSiteObj = {};
        serviceDetailPath = SETTING.serviceSavePath + '/' + planSubscriptionId;
        globalAPI.listGlobalApi(serviceDetailPath, function(error) {
            var response = {
                    resCode: 201,
                    response: error
                }
            successcallback(response);
        }, function(data, response) {
            data.resCode = response.statusCode;
            if (data.hasOwnProperty('service_id') && data.service_id != 0 && data.service_id != null) {
                var serviceId = data.service_id;
                //now check wheather any site detail provided or not
                if (req.body.hasOwnProperty('siteDetail') && req.body.siteDetail.length > 0) {
                    var siteDetails = req.body.siteDetail;
                    var siteLen = req.body.siteDetail.length;
                    node_module.asyncForEach(siteDetails, function(item, index, arr) {
                        //now check wheather the particular site and slot id is present or not
                        var siteId = item.siteId;
                        var slotId = item.slotId;
			requestParam = {};
                        requestParam.site_id = siteId;
                        var args = {
                            data: requestParam,
                            headers: {
                                "Content-Type": "application/json"
                            }
                        };
                        serviceDetailPath = SETTING.serviceSiteSavePath + '/' + serviceId + '/' + 'site' + '/' + slotId;
                        globalAPI.saveGlobalApi(serviceDetailPath, args, function(error) {}, function(data, response) {
                            if (data.status == 'ASSIGNED') {
                                precessedSiteObj[index] = siteId;
                                var objlen = Object.keys(precessedSiteObj).length;
                                if (objlen == siteLen) {
                                    var response = {
                                            resCode: SETTING.succResCode,
                                            response: 'A new service has been added successfully.'
                                        }
                                    successcallback(response);
                                }
                            } else {
                                var response = {
                                    resCode: 201,
                                    response: 'Error occured while assigne site id.'
                                }
                                errorcallback(response);
                            }
                        });
                    });
                } else {
                    var response = {
                            resCode: 200,
                            response: data
                        }
                    successcallback(response);
                }
            } else {
                var response = {
                        resCode: 201,
                        response: 'Unable to get service id!!'
                    }
                successcallback(response);
            }
        });
    },
    /* save data in user provision  */
    createUsersProvision: function(data, errorcallback, successcallback) {
        queryDb.insert('customer_user_provisions', data, function(err, res) {
            if (err) errorcallback(err);
            successcallback(res.insertId);
        });
    },
    /* save data in user provision  */
    updateUsersProvision: function(data, cond, errorcallback, successcallback) {
        queryDb.update('customer_user_provisions', data, cond, function(err, res) {
            if (err) errorcallback("Uh oh! Couldn't update results: " + err.msg);
            successcallback(true);
        });
    },
    getCustomerUsersProvisionFullInfoByCondiction: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from customer_user_provisions as cup " + param.join + " where " + param.condiction, function(err, response) {
            if (typeof response != 'undefined' && response != null && response != '') {
                successcallback(response[0]);
            } else {
                successcallback({});
            }
        })
    },
    /* save data in customer option  */
    createCustomerOption: function(data, errorcallback, successcallback) {
        queryDb.insert('customer_portal_option', data, function(err, res) {
            if (err) errorcallback(err);
            successcallback(res.insertId);
        });
    },
    /* update data in customer option  */
    updateCustomerOption: function(data, cond, errorcallback, successcallback) {
        queryDb.update('customer_portal_option', data, cond, function(err, res) {
            if (err) errorcallback("Uh oh! Couldn't update results: " + err.msg);
            successcallback(true);
        });
    },
    getCustomerOptionFullInfoByCondiction: function(param, errorcallback, successcallback) {
        var getCustomerPortalSql = "Select " + param.select + " from customer_portal_option as cpo " + param.join + " where " + param.condiction
        return queryDb.query(getCustomerPortalSql, function(err, response) {
            successcallback(response);
        })
    },
    getSubscriptionDetailsFullInfoByCondiction: function(param, errorcallback, successcallback) {
        var queryCheck = "Select " + param.select + " from plan_subscription as ps " + param.join + " where " + param.condiction;
        return queryDb.query("Select " + param.select + " from plan_subscription as ps " + param.join + " where " + param.condiction, function(err, response) {
            if (response != undefined) {
                successcallback(response[0]);
            } else {
                successcallback(response);
            }
        })
    },
    getSubscriptionDetailsFullInfoByCondictionDetails: function(param, errorcallback, successcallback) {
        return queryDb.query("Select " + param.select + " from plan_subscription as ps " + param.join + " where " + param.condiction, function(err, response) {
            if (response != undefined) {
                successcallback(response);
            } else {
                successcallback(response);
            }
        })
    },
    getCommonInfoByCondition: function(commonQuery, errorcallback, successcallback) {
        return queryDb.query(commonQuery, function(err, response) {
            successcallback(response);
        })
    },
    getPlanWithServiceTemplateList: function(planQuery, errorcallback, successcallback) {
        return queryDb.query(planQuery, function(err, response) {
            successcallback(response);
        })
    },
    getActiveAndDeactiveClient: function(param, errorcallback, successcallback) {
        var queryCheck = "Select count(name) as clientCount from client where " + param.condiction;
        return queryDb.query(queryCheck, function(err, response) {
            if (response != undefined) {
                successcallback(response);
            } else {
                successcallback(response);
            }
        })
    },
    getCustomerSubscriptionCondition: function(param, errorcallback, successcallback) {
        var queryCheck = "Select count(name) as clientCount from client where " + param.condiction;
        return queryDb.query(queryCheck, function(err, response) {
            if (response != undefined) {
                successcallback(response);
            } else {
                successcallback(response);
            }
        })
    },
    getCustomerSubscriptionCount: function(param, errorcallback, successcallback) {
        var queryCheck = "Select client_id,count(name) as customerSubscriptionCount from plan_subscription where " + param.condiction + " GROUP BY client_id";
        return queryDb.query(queryCheck, function(err, response) {
            if (response != undefined) {
                successcallback(response);
            } else {
                successcallback(response);
            }
        })
    },
    getCustomerCount: function(param, errorcallback, successcallback) {
        var queryCheck = "Select client_id,count(user) as clientCustomerCount from customer_user where " + param.condiction + " GROUP BY client_id";
        return queryDb.query(queryCheck, function(err, response) {
            if (response != undefined) {
                successcallback(response);
            } else {
                successcallback(response);
            }
        })
    },
    getSubscribingCustomerCondition: function(param, errorcallback, successcallback) {
        var getMprQuery = "select * from v_report_accounts_summary WHERE tenant_record_id = 4 AND label = 'Subscriber'";
        return qbRemote.query(getMprQuery, function(err, response) {
            successcallback(response);
        })
    },
    getCustomerByDayCondition: function(param, errorcallback, successcallback) {
        var getCustomerByDayQuery = "select TIMESTAMP(day) as timestamps,day,tenant_record_id as tenant_record_id,count from v_report_new_accounts_daily WHERE tenant_record_id = 4 AND DATEDIFF(NOW(), day) <= 7";
        return qbRemote.query(getCustomerByDayQuery, function(err, response) {
            successcallback(response);
        })
    },
    getConversionsByDayCondition: function(param, errorcallback, successcallback) {
        var getConversionsByDayQuery = "select TIMESTAMP(day) as timestamps,day,tenant_record_id as tenant_record_id,count from v_report_conversions_daily WHERE tenant_record_id = 4 AND DATEDIFF(NOW(), day) <= 7";
        return qbRemote.query(getConversionsByDayQuery, function(err, response) {
            successcallback(response);
        })
    },
    getTrialStartedByDayCondition: function(param, errorcallback, successcallback) {
        var getTrialStartedByDayQuery = "SELECT TIMESTAMP(day) as timestamps,day,tenant_record_id as tenant_record_id,count FROM `v_report_trial_starts_count_daily` WHERE tenant_record_id = 4 AND DATEDIFF(NOW(), day) <= 7";
        return qbRemote.query(getTrialStartedByDayQuery, function(err, response) {
            successcallback(response);
        })
    },
    getCancellationByDayCondition: function(param, errorcallback, successcallback) {
        var getCancellationByDayQuery = "SELECT TIMESTAMP(day) as timestamps,day,tenant_record_id as tenant_record_id,count FROM `v_report_cancellations_count_daily` WHERE tenant_record_id = 1 AND DATEDIFF(NOW(), day) <= 7";
        return qbRemote.query(getCancellationByDayQuery, function(err, response) {
            successcallback(response);
        })
    },
    getTotalCustomerCondition: function(param, errorcallback, successcallback) {
        var getCustomerQuery = "select count(name) as totalCustomer from client "+param.condiction;
        return qbRemote.query(getCustomerQuery, function(err, response) {
            successcallback(response);
        })
    },
    getActiveCustomerCondition: function(param, errorcallback, successcallback) {
        var getCustomerQuery = "select count(name) as activeCustomer from client "+param.condiction;
        return qbRemote.query(getCustomerQuery, function(err, response) {
            successcallback(response);
        })
    },
    getCustomerSubscriptionAverageCondition: function(param, errorcallback, successcallback) {
        var getCustomerQuery = "select avg(subscriptionCustomerCount) as averageCustomerSubscription from (select count(name) as subscriptionCustomerCount from plan_subscription "+param.condiction+" group by client_id) as count";
        return qbRemote.query(getCustomerQuery, function(err, response) {
            successcallback(response);
        })
    },
    //busniess report plans
    getBusinessReportPlansCondition: function(param, errorcallback, successcallback) {
        var getCustomerQuery = "select * from plan where 1";
        return queryDb.query(getCustomerQuery, function(err, response) {
            successcallback(response);
        })
    },
    getClientCountCondition: function(param, errorcallback, successcallback) {
        var getCustomerQuery = "select count(*) as customerCount from client where 1";
        return qbRemote.query(getCustomerQuery, function(err, response) {
            successcallback(response);
        })
    },
    getEachCustomerSubscriptionCondition: function(param, errorcallback, successcallback) {
        var getCustomerQuery = "SELECT GROUP_CONCAT( DISTINCT(plan_id)) as subscriptions ,client_id from plan_subscription GROUP BY client_id ORDER BY GROUP_CONCAT( DISTINCT(plan_id)) DESC";
        return queryDb.query(getCustomerQuery, function(err, response) {
            successcallback(response);
        })
    },
    //pie chart for business report clients subscription and non subscription
    getCustomerSubscriptionNonsubscriptionCondition: function(param, errorcallback, successcallback) {
        var getMprQuery = "select * from v_report_accounts_summary WHERE tenant_record_id = "+SETTING.tenant_record_id+" ";
        return qbRemote.query(getMprQuery, function(err, response) {
            successcallback(response);
        })
    },
    //pie chart for business report clients Overduestatus
    getCustomerOverdueStatusCondition: function(param, errorcallback, successcallback) {
        var getMprQuery = "select * from v_report_overdue_states_count_daily WHERE tenant_record_id = "+SETTING.tenant_record_id+' AND DATEDIFF(NOW(), day) = 0 ORDER BY day ASC';
        return qbRemote.query(getMprQuery, function(err, response) {
            successcallback(response);
        })
    },
    //multiple chart for business report clients
    getCustomerMultipleLineChartCondition: function(param, errorcallback, successcallback) {
        var getMprQuery = "select * from v_report_new_accounts_daily WHERE tenant_record_id = "+SETTING.tenant_record_id+" AND DATEDIFF(NOW(), day) <= 7 ORDER BY day ASC";
        return qbRemote.query(getMprQuery, function(err, response) {
            successcallback(response);
        })
    }
}
module.exports = model;
