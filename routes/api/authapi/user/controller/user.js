var express             = require('express');
var router              = express.Router();
var app                 = express();
var customodule         = require('../../../../customexport');
var node_module         = require('../../../../export');
var ERROR               = require('../../../../../config/statuscode');
var PERMISSION          = require('../../../../../config/permission');
var Usermodel           = require('../model/user');
/* Role Management Api Start */

/* This api is to create role and only admin can access this api
   Created  : 29-03-2017
   Modified : 05-04-2017
*/
router.post('/createRole',customodule.authenticate,function(req,res,next){
  var name            = req.body.name;
  var permission_ids  = req.body.permissionIds;
  var permissions = req.session.permissions;
  var manage_role_index = permissions.indexOf(PERMISSION.role_permission_manage);
  if(manage_role_index != -1){
    if(!name || !permission_ids){
      res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    }else{
      var data = {
        name : name
      }
      Usermodel.createRole(data,function(error){
        var response = {
          resCode   : 201,
          response  : error
        }
        res.status(200).send(response);
      },function(insertId){
        if(insertId){
          var permissions = permission_ids.split(',');
          var i;
          for(i=0;i<permissions.length;i++){
            var data = {
              permission_id : permissions[i],
              role_id       : insertId
            }
            Usermodel.createRolePermission(data,function(error){},function(successcallback){});
            if(i  ==  permissions.length-1){
            var response = {
              resCode   : 200,
              response  : "Role created successfully",
            }
            res.status(200).send(response);
            }
          }
        }else{
          res.status(200).send(ERROR.ROLE.ROLEEXIST);
        }
      })
    }
   }else{
    res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
   }   
})
/* This api is to edit role and only admin can access this api
   Created  : 29-03-2017
   Modified : 04-04-2017
 */
router.post('/editRole',customodule.authenticate,function(req,res,next){
  var name           = req.body.name;
  var role_id        = req.body.roleId;
  var permission_ids = req.body.permissionIds;
  if(!name || !role_id || !permission_ids){
    res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
  }else{
      var permissions = req.session.permissions;
      var manage_role_index = permissions.indexOf(PERMISSION.role_permission_manage);
    if(manage_role_index != -1){
      var permissions = permission_ids.split(',');
      var data = {
          name    : name
      }
      var cond = {
          role_id : role_id
      }
      Usermodel.updateRole(cond,data,function(error){
        var response = {
          resCode   : 201,
          response  : error
        }
        res.status(200).send(response);
      },function(success){
        Usermodel.deleteRolePermission(cond,function(err){
          customodule.helper.winstonContext(req,res).info("Getting error on deleting role permission");             
        },function(success){
          var i;
          for(i=0;i<permissions.length;i++){
            var data = {
              permission_id : permissions[i],
              role_id       : role_id
            }
            Usermodel.createRolePermission(data,function(error){},function(successcallback){});
            if(i==permissions.length-1){
              var response = {
                resCode   : 200,
                response  : "Role updated successfully"
              }
              res.status(200).send(response);
            }
          }
        })
      })
    }else{
      res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
  }
})

/* This api is to delete role and only admin can access this api
   Created  : 29-03-2017
   Modified : 04-04-2017
*/
router.get('/deleteRole',customodule.authenticate,function(req,res,next){
  var role_id  = req.query.roleId;
  if(!role_id){
    res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
  }else{
      var permissions = req.session.permissions;
      var manage_role_index = permissions.indexOf(PERMISSION.role_permission_manage);
    if(manage_role_index != -1){
      var cond = {
        role_id  : role_id
      }
      Usermodel.getUserByCondiction({role:role_id},function(err){
        res.status(200).send(ERROR.ROLE.ROLENOTDELETED);
      },function(users){
        if(users.length>0){
          res.status(200).send(ERROR.ROLE.ROLEASSIGNED);
        }else{
          Usermodel.deleteRolePermission(cond,function(error){
            var response = {
              resCode   : 201,
              response  : error
            }
            res.status(200).send(response);
          },function(success){
            if(success){
              Usermodel.deleteRole(cond,function(error){
                customodule.helper.winstonContext(req,res).info("Getting error on deleting role");
              },function(response){
                var response = {
                  resCode : 200,
                  response: "Role deleted successfully"
                }
                res.status(200).send(response);
              })
            }
          })
        }
      })
     }else{
       res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
     }
  }
})
/* This api is to get roles and only admin can access this api
   Created  : 04-04-2017
   Modified : 04-04-2017
*/
router.get('/getRoles',customodule.authenticate,function(req,res,next){
    var permissions = req.session.permissions;
    var manage_role_index = permissions.indexOf(PERMISSION.role_permission_manage);
    var role_list_index   = permissions.indexOf(PERMISSION.role_permission_list);
    if(manage_role_index != -1 || role_list_index!= -1){
      if(req.query.roleId){
        var condiction="r.role_id ="+req.query.roleId;
      }else{
        var condiction="r.role_id !=''";
      }
      var select  = "r.role_id as roleId,ifnull(r.name,'') as roleName, (SELECT GROUP_CONCAT(permission_id) from role_permissions as rp where rp.role_id=r.role_id) as permissionIds , (SELECT GROUP_CONCAT(p.name) from  role_permissions as rp LEFT JOIN permission as p ON rp.permission_id=p.permission_id where rp.role_id=r.role_id) as permissionNames ";
      var join    = "";
      var param     = {
        condiction: condiction,
        join      : join,
        select    : select
      }
      Usermodel.getRoles(param,function(error){
        var response = {
          resCode : 201,
          response: error
        }
        res.status(200).send(response);
      },function(success){
        if(success){
          var response = {
            resCode : 200,
            response: success
          }
          res.status(200).send(response);
        }
      })
   }else{
     res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
   }
})
/* Role Management Api End */

/* Permission Management Api Start */

/* This api is to create permission and only admin can access this api
   Created  : 29-03-2017
*/
router.post('/createPermission_UNUSED',customodule.authenticate,function(req,res,next){
  var name             = req.body.name;
  var permission_type  = (req.body.permissionType=='read')?1:0;
  var description      = req.body.description;
  if((req.session.admin) && (req.session.admin==true)){
    if(!name || !permission_type || !description){
      res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    }else{
      var data = {
        name:name,
        permission_type:permission_type,
        description:description
      }
      Usermodel.createPermission(data,function(error){
        var response = {
          resCode : 201,
          response: error
        }
        res.status(200).send(response);
      },function(success){
        var response = {
          resCode : 200,
          response: "Permission created successfully"
        }
        res.status(200).send(response);
      })
    }
  }else{
    res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
  } 
})
/* This api is to edit permission and only admin can access this api
   Created  : 29-03-2017
*/
router.post('/editPermission_UNUSED',customodule.authenticate,function(req,res,next){
  var name             = req.body.name;
  var permission_type  = (req.body.permissionType=='read')?1:0;
  var description      = req.body.description;
  var permission_id    = req.body.permission_id;
  if(!name || !permission_id || !name || !permission_type || !description){
    res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
  }else{
    if((req.session.admin) && (req.session.admin==true)){
      var data = {
        name:name,
        permission_type:permission_type,
        description:description
      }
      var cond = {
        permission_id:permission_id
      }
      Usermodel.updatePermission(cond,data,function(error){
        var response = {
          resCode : 201,
          response: error
        }
        res.status(200).send(response);
      },function(success){
        var response = {
          resCode : 200,
          response: "Permission updated successfully"
        }
        res.status(200).send(response);
      })
    }else{
      res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
  }

})

/* This api is to delete permission and only admin can access this api
   Created  : 29-03-2017
*/
router.get('/deletePermission_UNUSED',customodule.authenticate,function(req,res,next){
  var permission_id  = req.query.permissionId;
  if(!permission_id){
    res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
  }else{
    if((req.session.admin) && (req.session.admin==true)){
      var cond = {
        permission_id:permission_id
      }
      Usermodel.deletePermission(cond,function(error){
        var response = {
          resCode : 201,
          response: error
        }
        res.status(200).send(response);
      },function(success){
        if(success){
          var response = {
            resCode : 200,
            response: "Permission deleted successfully"
          }
          res.status(200).send(response);
        }
      })
    }else{
      res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
  }
})
/* This api is to get permissions and only admin can access this api
   Created  : 30-03-2017
*/
router.get('/getPermissions',function(req,res,next){
    var permissions = req.session.permissions;    
    var manage_role_index = 1;
    var role_list_index   = 1;
    if(manage_role_index != -1 || role_list_index != -1){
      Usermodel.getPermissions(function(error){
        var response = {
          resCode : 201,
          response: error
        }
        res.status(200).send(response);
      },function(success){
        if(success){
          var response = {
            resCode : 200,
            response: success
          }
          res.status(200).send(response);
        }
      })
   }else{
     res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
   }
})
/* Permission Management Api End */

/* Role-Permission Management Api Start */

/* This api is to create role-permission and only admin can access this api
   Created  : 30-03-2017
*/
router.post('/createRolePermission',customodule.authenticate,function(req,res,next){
  var permission_id             = req.body.permissionId;
  var role_id                   = req.body.roleId;
  var permissions = req.session.permissions;
  var manage_role_index = permissions.indexOf(PERMISSION.role_permission_manage);
  if(manage_role_index != -1){
    if(!permission_id || !role_id){
      res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    }else{
      var data = {
        permission_id : permission_id,
        role_id       : role_id,
      }
      Usermodel.createRolePermission(data,function(error){
        var response = {
          resCode : 201,
          response: error
        }
        res.status(200).send(response);
      },function(success){
        var response = {
          resCode : 200,
          response: "Role-Permission created successfully"
        }
        res.status(200).send(response);
      })
    }
  }else{
    res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
  }
})
/* This api is to edit role-permission and only admin can access this api
   Created  : 30-03-2017
   Modified : 04-04-2017
*/
router.post('/editRolePermission',customodule.authenticate,function(req,res,next){
  var permission_id             = req.body.permissionId;
  var role_id                   = req.body.roleId;
  var role_permissions_id       = req.body.rolePermissionId;
  if(!role_id || !permission_id || !role_permissions_id ){
    res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
  }else{
    var permissions = req.session.permissions;
    var manage_role_index = permissions.indexOf(PERMISSION.role_permission_manage);
    if(manage_role_index != -1){
      var data = {
        permission_id           : permission_id,
        role_id                 : role_id,
        role_permissions_id     : role_permissions_id
      }
      var cond = {
        role_permissions_id : role_permissions_id
      }
      Usermodel.updateRolePermission(cond,data,function(error){
        var response = {
          resCode : 201,
          response: error
        }
        res.status(200).send(response);
      },function(success){
        var response = {
          resCode : 200,
          response: "Role-Permission updated successfully"
        }
        res.status(200).send(response);
      })
    }else{
      res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
  }
})
/* This api is to delete role-permission and only admin can access this api
   Created  : 30-03-2017
   Modified : 04-04-2017
*/
router.get('/deleteRolePermission',customodule.authenticate,function(req,res,next){
  var role_permissions_id  = req.query.rolePermissionId;
  if(!role_permissions_id){
    res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
  }else{
    var permissions = req.session.permissions;
    var manage_role_index = permissions.indexOf(PERMISSION.role_permission_manage);
    if(manage_role_index != -1){
      var cond = {
        role_permissions_id:role_permissions_id
      }
      Usermodel.deleteRolePermission(cond,function(error){
        var response = {
          resCode : 201,
          response: error
        }
        res.status(200).send(response);
      },function(success){
        if(success){
          var response = {
            resCode : 200,
            response: "Role-Permission deleted successfully"
          }
          res.status(200).send(response);
        }
      })
    }else{
      res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
  }
})
/* Role-Permission Management Api End */

/* User Management Section */

/* This api is to create user and only admin can access this api
   Created  : 30-03-2017
*/
router.post('/createUser',customodule.authenticate,function(req,res,next){
  var email             = req.body.userEmail;
  var password          = req.body.userPassword;
  var user              = req.body.userName;
  var phone             = req.body.userPhone;
  var mobilePhone       = req.body.userMobile;
  var role              = req.body.userRole;
  var notes             = req.body.userNotes;
  var permissions = req.session.permissions;
  var manage_user_index = permissions.indexOf(PERMISSION.user_manage);
  if(manage_user_index != -1){    
    if(!email || !password || !user || !role){
      res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
    }else{
      var data = {
        email          : email,
        password       : node_module.crypto.createHash('md5').update(customodule.setting.passprefix+password).digest("hex"),
        user           : user,
        phone          : phone,
        mobilePhone    : mobilePhone,
        role           : role,
        notes          : notes
      }
      Usermodel.createUser(data,function(error){
        var response = {
          resCode : 201,
          response: error
        }
        res.status(200).send(response);
      },function(success){
        var mail_data = {
          password : password,
          notes    : notes
        }
        customodule.mail(email,'welcome',mail_data);
        var condiction  ="u.user_id ="+success;
        var select      = "u.user_id as userId,ifnull(u.user,'') as userName, ifnull(u.email,'') as userEmail, ifnull(u.phone,'') as userPhone,ifnull(u.mobilePhone,'') as userMobile, u.status as userStatus,u.notes as userNotes,ifnull(r.role_id,'') as userRoleId, ifnull(r.name,'') as userRole,ifnull(rp.permission_id,'') as userRolePermissionId,ifnull(p.name,'') as userPermission,ifnull(p.permission_type,'') as userPermissiontype";
        var join        = "LEFT JOIN role as r ON r.role_id=u.role LEFT JOIN role_permissions as rp ON rp.role_id=r.role_id LEFT JOIN permission as p ON p.permission_id=rp.permission_id";
        var param       = {
          condiction: condiction,
          join      : join,
          select    : select
        }
        Usermodel.getUserFullInfoByCondiction(param,function(err){
          customodule.helper.winstonContext(req,res).info("Getting error on fetching user information");             
        },function(userdata){
          var response = {
            resCode   : 200,
            response  : "User created successfully",
            userdata  : userdata[0]
          }
          res.status(200).send(response);
        })
      })
    }
   }else{
     res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
   }  
})
/* This api is to get all users and only admin can access this api
   Created  : 31-03-2017
*/
router.get('/getUsers',customodule.authenticate,function(req,res,next){
    var permissions = req.session.permissions;    
    var manage_user_index = permissions.indexOf(PERMISSION.user_manage);
    var user_list_index = permissions.indexOf(PERMISSION.user_list);
    if(manage_user_index != -1 || user_list_index != -1){
      if(req.query.userId){
        var condiction="u.user_id ="+req.query.userId;
      }else{
        var condiction="u.user_id !=1";
      }
      var select  = "u.user_id as userId,ifnull(u.user,'') as userName, ifnull(u.email,'') as userEmail, ifnull(u.phone,'') as userPhone,ifnull(u.mobilePhone,'') as userMobile, u.status as userStatus,ifnull(u.notes,'') as userNotes,ifnull(r.role_id,'') as userRoleId, ifnull(r.name,'') as userRole";
      var join    = "LEFT JOIN role as r ON r.role_id=u.role";
      var param     = {
        condiction: condiction,
        join      : join,
        select    : select
      }
      Usermodel.getUserFullInfoByCondiction(param,function(err){
        customodule.helper.winstonContext(req,res).info("Getting error on fetching user details");             
      },function(userdata){
        var response = {
            resCode   : 200,
            response  : userdata
        }
        res.status(200).send(response)
      })
   }else{
     res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
   }
})
/* User Management Section end */

/* This api is to Update user information and only admin can access this api
   Created  : 30-03-2017
*/
router.post('/editUser',customodule.authenticate,function(req,res,next){
  var user              = req.body.userName;
  var phone             = req.body.userPhone;
  var mobilePhone       = req.body.userMobile;
  var role              = req.body.userRole;
  var notes             = req.body.userNotes;
  var user_id           = req.body.userId;
  var permissions = req.session.permissions;
  var manage_user_index = permissions.indexOf(PERMISSION.user_manage);
  if(manage_user_index != -1){
      if(!user || !role || !user_id){
        res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
      }else{
        var data = {
          user           : user,
          phone          : phone,
          mobilePhone    : mobilePhone,
          role           : role,
          notes          : notes
        }
        var cond = {
          user_id : user_id
        }
        Usermodel.updateUserByCondiction(data,cond,function(error){
          var response = {
            resCode : 201,
            response: error
          }
          res.status(200).send(response);
        },function(success){
          var response = {
            resCode : 200,
            response: "User information updated successfully"
          }
          res.status(200).send(response);
        })
      }
  }else{
      res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
  }  
})

/* This api is to delete user and only admin can access this api
   Created  : 30-03-2017
   Modified : 04-04-2017
*/
router.get('/deleteUser',customodule.authenticate,function(req,res,next){
  var user_id  = req.query.userId;
  if(!user_id){
    res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
  }else{
    var permissions = req.session.permissions;
    var manage_user_index = permissions.indexOf(PERMISSION.user_manage);
    if(manage_user_index != -1){
      var cond = {
        user_id:user_id
      }
      Usermodel.removeAccessToken(cond,function(error){
        var response = {
          resCode : 201,
          response: error
        }
        res.status(200).send(response);
      },function(success){
        Usermodel.deleteUser(cond,function(error){
          var response = {
            resCode : 201,
            response: error
          }
          res.status(200).send(response);
        },function(success){
          if(success){
            var response = {
              resCode : 200,
              response: "User deleted successfully"
            }
            res.status(200).send(response);
          }
        })
      })
    }else{
      res.status(200).send(ERROR.COMMON.PERMISSIONDENIED);
    }
  }
})
/* This api is to change password
   Created  : 30-03-2017
*/
router.post('/changePassword',customodule.authenticate,function(req,res,next){
  var old_password  = req.body.oldPassword;
  var new_password  = req.body.newPassword;
  var userId        = req.body.userId;
  if(!old_password || !new_password || !userId ){
    res.status(200).send(ERROR.COMMON.REQUIREDFIELD);
  }else{
    var data  = {
      password    : node_module.crypto.createHash('md5').update(customodule.setting.passprefix+new_password).digest("hex"),
    }
    var condiction    = {
        password  : node_module.crypto.createHash('md5').update(customodule.setting.passprefix+old_password).digest("hex"),
        user_id   : userId,
        status    : '1'
    }
    Usermodel.getUserByCondiction(condiction,function(err){
      customodule.helper.winstonContext(req,res).info("Getting error on fetching user details");
    },function(users){
      if(users.length>0){
        Usermodel.updateUserByCondiction(data,condiction,function(err){
          customodule.helper.winstonContext(req,res).info("Getting error on updating user");
        },function(success){
          if(success){
            var response  = {
                resCode   : 200,
                response  : "You have successfully change your password"
            }
            res.status(200).send(response);
          }
        })
      }else{
        res.status(200).send(ERROR.USER.INVALIDUSERPASSWORD);
      }
    })
  }
})
/* Api to logout
  Created  : 30-03-2017
*/
router.post('/userLogout',customodule.authenticate,function(req,res){
  var user_id = req.body.userId;
  var cond = {
    user_id:user_id,
    token  :req.headers['token']
  }
  Usermodel.removeAccessToken(cond,function(error){
  },function(success){
    if(success){
      req.session.destroy(function(err) {
        if(err) {
          res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
        } else {
          var response = {
            resCode  : 200,
            response : "You are logout successfully"
          }
          res.status(200).send(response);
        }
      })
    }
  })
})
module.exports = router;
